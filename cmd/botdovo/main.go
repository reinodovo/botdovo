package main

import (
	"context"
	"log"
	"math/rand"
	"os"
	"regexp"
	"strings"
	"time"

	"github.com/patrickmn/go-cache"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracegrpc"
	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/sdk/resource"
	sdktrace "go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.4.0"
	"google.golang.org/grpc/credentials"

	"gitlab.com/reinodovo/botdovo/internal/actions"
	"gitlab.com/reinodovo/botdovo/internal/actions/builder_base"
	"gitlab.com/reinodovo/botdovo/internal/actions/clan_capital"
	"gitlab.com/reinodovo/botdovo/internal/actions/clan_games"
	"gitlab.com/reinodovo/botdovo/internal/actions/cwl"
	"gitlab.com/reinodovo/botdovo/internal/actions/home_village"
	"gitlab.com/reinodovo/botdovo/internal/bot"
	"gitlab.com/reinodovo/botdovo/internal/clash"
	"gitlab.com/reinodovo/botdovo/internal/configuration_manager"
	"gitlab.com/reinodovo/botdovo/internal/cwl_planning/cwl_sheets"
	"gitlab.com/reinodovo/botdovo/internal/database"
	"gitlab.com/reinodovo/botdovo/internal/device"
	"gitlab.com/reinodovo/botdovo/internal/information_manager"
	"gitlab.com/reinodovo/botdovo/internal/scheduler"
	"gitlab.com/reinodovo/botdovo/internal/server"
)

func risada(message string) []string {
	re := regexp.MustCompile(`(?:k|[ahus]|rs){5,}`)
	matches := re.FindAllIndex([]byte(strings.ToLower(message)), -1)
	laughSize := 0
	for _, match := range matches {
		laughSize += match[1] - match[0]
	}
	if laughSize > int(0.5*float32(len(message))) && rand.Float32() < 0.5 {
		return []string{strings.Repeat("k", 10+rand.Intn(20))}
	}
	return nil
}

func newExporter(ctx context.Context) (*otlptrace.Exporter, error) {
	opts := []otlptracegrpc.Option{
		otlptracegrpc.WithEndpoint("api.honeycomb.io:443"),
		otlptracegrpc.WithHeaders(map[string]string{
			"x-honeycomb-team":    os.Getenv("HONEYCOMB_API_KEY"),
			"x-honeycomb-dataset": os.Getenv("HONEYCOMB_DATASET"),
		}),
		otlptracegrpc.WithTLSCredentials(credentials.NewClientTLSFromCert(nil, "")),
	}

	client := otlptracegrpc.NewClient(opts...)
	return otlptrace.New(ctx, client)
}

func newTraceProvider(exp *otlptrace.Exporter) *sdktrace.TracerProvider {
	resource :=
		resource.NewWithAttributes(
			semconv.SchemaURL,
			semconv.ServiceNameKey.String("botdovo"),
		)

	return sdktrace.NewTracerProvider(
		sdktrace.WithBatcher(exp),
		sdktrace.WithResource(resource),
	)
}

func main() {
	rand.Seed(time.Now().UTC().UnixNano())

	var err error
	logFile, err := os.OpenFile("logs.txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		log.Fatal(err)
	}
	logger := log.New(logFile, "", log.Ldate|log.Ltime|log.Lshortfile)

	ctx := context.Background()

	exp, err := newExporter(ctx)
	if err != nil {
		log.Fatalf("failed to initialize exporter: %v", err)
	}

	tp := newTraceProvider(exp)

	defer func() { _ = tp.Shutdown(ctx) }()

	otel.SetTracerProvider(tp)

	otel.SetTextMapPropagator(
		propagation.NewCompositeTextMapPropagator(
			propagation.TraceContext{},
			propagation.Baggage{},
		),
	)

	cache := cache.New(1*time.Hour, 1*time.Hour)

	var cron scheduler.Scheduler = scheduler.NewGoCronScheduler()

	var db database.Database = database.NewMongoDatabase(logger)
	defer db.Close()

	var configurationManager configuration_manager.ConfigurationManager = configuration_manager.NewDBConfigurationManager(db, cache, logger)

	var clashAPI clash.ClashAPI = clash.NewClashAPIClient(cache, configurationManager)

	var mgr information_manager.InformationManager = information_manager.NewLocalDBInformationManager(db, configurationManager, cron, clashAPI, logger)

	behaviours := []bot.Behaviour{risada}
	var chatBot bot.Bot = bot.NewTelegramBot(behaviours)

	var cwlSheets *cwl_sheets.CWLSheets = cwl_sheets.NewCWLSheets(clashAPI, cache, configurationManager, db, logger)

	var device device.Device = device.NewDevice()

	home_village.SetupHomeVillage(chatBot, clashAPI, mgr, configurationManager)
	builder_base.SetupBuilderBase(chatBot, clashAPI, mgr)
	clan_games.SetupClanGames(chatBot, configurationManager, mgr)
	clan_capital.SetupClanCapital(chatBot, configurationManager, mgr)
	cwl.SetupCWL(chatBot, configurationManager, cwlSheets, cron, clashAPI, db, device, logger)
	actions.NewSheetsManagement(chatBot, cwlSheets, cron, logger)

	server := server.NewServer(cwlSheets, clashAPI)
	go server.Serve()

	chatBot.Start()
}
