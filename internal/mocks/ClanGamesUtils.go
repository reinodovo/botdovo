// Code generated by mockery v2.16.0. DO NOT EDIT.

package mocks

import (
	time "time"

	mock "github.com/stretchr/testify/mock"
)

// ClanGamesUtils is an autogenerated mock type for the ClanGamesUtils type
type ClanGamesUtils struct {
	mock.Mock
}

// Ended provides a mock function with given fields:
func (_m *ClanGamesUtils) Ended() bool {
	ret := _m.Called()

	var r0 bool
	if rf, ok := ret.Get(0).(func() bool); ok {
		r0 = rf()
	} else {
		r0 = ret.Get(0).(bool)
	}

	return r0
}

// RemainingTime provides a mock function with given fields:
func (_m *ClanGamesUtils) RemainingTime() time.Duration {
	ret := _m.Called()

	var r0 time.Duration
	if rf, ok := ret.Get(0).(func() time.Duration); ok {
		r0 = rf()
	} else {
		r0 = ret.Get(0).(time.Duration)
	}

	return r0
}

// Started provides a mock function with given fields:
func (_m *ClanGamesUtils) Started() bool {
	ret := _m.Called()

	var r0 bool
	if rf, ok := ret.Get(0).(func() bool); ok {
		r0 = rf()
	} else {
		r0 = ret.Get(0).(bool)
	}

	return r0
}

type mockConstructorTestingTNewClanGamesUtils interface {
	mock.TestingT
	Cleanup(func())
}

// NewClanGamesUtils creates a new instance of ClanGamesUtils. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
func NewClanGamesUtils(t mockConstructorTestingTNewClanGamesUtils) *ClanGamesUtils {
	mock := &ClanGamesUtils{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
