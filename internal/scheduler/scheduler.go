package scheduler

import (
	"context"
	"reflect"
	"runtime"
	"strings"
	"time"

	"github.com/avast/retry-go"
	"github.com/go-co-op/gocron"
	"gitlab.com/reinodovo/botdovo/internal/utils"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
)

const (
	tracerName = "scheduler"
)

type Interval = string

const (
	Year     Interval = "0 0 1 1 *"
	Month             = "0 0 1 * *"
	CWLStart          = "0 19 2 * *"
	Monday            = "0 0 * * 2"
	Saturday          = "0 0 * * 0"
	Day               = "0 0 * * *"
	Hour              = "0 * * * *"
	Minute            = "* * * * *"
)

type SchedulerCallback = func(ctx context.Context) error

type Scheduler interface {
	Schedule(key string, callback SchedulerCallback) (*gocron.Job, error)
}

type GoCronScheduler struct {
	scheduler *gocron.Scheduler
}

func NewGoCronScheduler() *GoCronScheduler {
	scheduler := gocron.NewScheduler(time.UTC)
	scheduler.StartAsync()
	return &GoCronScheduler{
		scheduler: scheduler,
	}
}

func getFunctionName(temp interface{}) string {
	strs := strings.Split((runtime.FuncForPC(reflect.ValueOf(temp).Pointer()).Name()), ".")
	return strs[len(strs)-1]
}

func (scheduler *GoCronScheduler) Schedule(key string, callback SchedulerCallback) (*gocron.Job, error) {
	callbackWithTrace := func(callback SchedulerCallback) error {
		ctx, span := otel.Tracer(tracerName).Start(context.Background(), "scheduler_callback")
		ctx = context.WithValue(ctx, "interval", key)
		defer span.End()
		span.SetAttributes(attribute.String("interval", key))
		span.SetAttributes(attribute.String("function_name", getFunctionName(callback)))

		err := retry.Do(func() error {
			return callback(ctx)
		}, retry.Attempts(5))
		utils.RecordErrorIfNotNil(span, err)
		return err
	}
	return scheduler.scheduler.Cron(key).Do(callbackWithTrace, callback)
}
