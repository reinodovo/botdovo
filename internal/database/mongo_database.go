package database

import (
	"context"
	"log"
	"os"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
)

type MongoDatabase struct {
	client *mongo.Client
	logger *log.Logger
}

const (
	tracerName = "database"
)

const (
	databaseName = "botdovo"
)

func NewMongoDatabase(logger *log.Logger) *MongoDatabase {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(os.Getenv("MONGODB_URI")))
	if err != nil {
		logger.Printf("could not connect to mongodb: %v\n", err)
	}
	return &MongoDatabase{client: client, logger: logger}
}

func (db *MongoDatabase) Get(ctx context.Context, collectionName, key string, object any) error {
	_, span := otel.Tracer(tracerName).Start(ctx, "get")
	defer span.End()

	span.SetAttributes(attribute.String("collection", collectionName))
	span.SetAttributes(attribute.String("key", key))

	collection := db.client.Database(databaseName).Collection(collectionName)

	var result bson.M

	err := collection.FindOne(context.Background(), bson.D{{Key: "_id", Value: key}}).Decode(&result)
	if err == mongo.ErrNoDocuments {
		span.SetAttributes(attribute.Bool("not_found", true))
		return nil
	} else if err != nil {
		span.RecordError(err)
		span.SetStatus(codes.Error, err.Error())
		return err
	}
	if value, ok := result["value"]; ok {
		bsonType, data, err := bson.MarshalValue(value)
		if err != nil {
			span.RecordError(err)
			span.SetStatus(codes.Error, err.Error())
			return err
		}
		rawData := bson.RawValue{Type: bsonType, Value: data}
		err = rawData.Unmarshal(object)
		if err != nil {
			span.RecordError(err)
			span.SetStatus(codes.Error, err.Error())
			return err
		}
	}
	return nil
}

func (db *MongoDatabase) SaveObject(ctx context.Context, collectionName, key string, object any) error {
	_, span := otel.Tracer(tracerName).Start(ctx, "save")
	defer span.End()

	span.SetAttributes(attribute.String("collection", collectionName))
	span.SetAttributes(attribute.String("key", key))

	collection := db.client.Database(databaseName).Collection(collectionName)

	result := struct {
		Value interface{}
	}{
		Value: object,
	}

	_, err := collection.ReplaceOne(ctx, bson.D{{Key: "_id", Value: key}}, result, options.Replace().SetUpsert(true))
	if err != nil {
		span.RecordError(err)
		span.SetStatus(codes.Error, err.Error())
	}
	return err
}

func (db *MongoDatabase) Close() error {
	return db.client.Disconnect(context.Background())
}
