package database

import (
	"context"
)

type Database interface {
	SaveObject(ctx context.Context, bucket string, key string, object any) error
	Get(ctx context.Context, bucket string, key string, object any) error
	Close() error
}
