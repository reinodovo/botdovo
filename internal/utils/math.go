package utils

import (
	"math/rand"
)

func RandomString(messages []string) string {
	return messages[rand.Int31n(int32(len(messages)))]
}

func Min(a, b int) int {
	if a < b {
		return a
	}
	return b
}
