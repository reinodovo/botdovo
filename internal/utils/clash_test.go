package utils

import (
	"reflect"
	"testing"

	"gitlab.com/reinodovo/botdovo/internal/clash"
)

func TestPlayersProgress(t *testing.T) {
	// setup
	previous := []clash.Player{
		{Tag: "tag1", VersusBattleWins: 10, VersusTrophies: 100},
		{Tag: "tag3", VersusBattleWins: 50, VersusTrophies: 150},
		{Tag: "tag4", VersusBattleWins: 60, VersusTrophies: 1000},
	}
	current := []clash.Player{
		{Tag: "tag1", VersusBattleWins: 10, VersusTrophies: 90},
		{Tag: "tag2", VersusBattleWins: 100, VersusTrophies: 4000},
		{Tag: "tag3", VersusBattleWins: 55, VersusTrophies: 200},
	}
	tests := []struct {
		extractor               func(previous, current clash.Player) int64
		expectedPlayersProgress []PlayerProgress[int64]
	}{
		{
			extractor: func(previous, current clash.Player) int64 {
				return current.VersusBattleWins - previous.VersusBattleWins
			},
			expectedPlayersProgress: []PlayerProgress[int64]{
				{Previous: previous[0], Current: current[0], Data: 0},
				{Previous: clash.Player{}, Current: current[1], Data: 100},
				{Previous: previous[1], Current: current[2], Data: 5},
			},
		},
		{
			extractor: func(previous, current clash.Player) int64 {
				return current.VersusTrophies - previous.VersusTrophies
			},
			expectedPlayersProgress: []PlayerProgress[int64]{
				{Previous: previous[0], Current: current[0], Data: -10},
				{Previous: clash.Player{}, Current: current[1], Data: 4000},
				{Previous: previous[1], Current: current[2], Data: 50},
			},
		},
	}
	for idx, test := range tests {
		// act
		playersProgress := PlayersProgress(previous, current, test.extractor)
		// test
		if !reflect.DeepEqual(playersProgress, test.expectedPlayersProgress) {
			t.Errorf("expected %v, got %v on test %v", test.expectedPlayersProgress, playersProgress, idx)
		}
	}
}
