package utils

import "context"

func BoolFromContext(ctx context.Context, key string) bool {
	return ctx.Value(key) != nil && ctx.Value(key).(bool)
}
