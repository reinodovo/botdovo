package utils

import (
	"fmt"
)

var (
	InvalidArgumentsError error = fmt.Errorf("Invalid Arguments")
)
