package utils

import (
	"time"

	"gitlab.com/reinodovo/botdovo/internal/clash"
	"gitlab.com/reinodovo/botdovo/internal/configuration_manager"
)

var cwlWarTimeFormat string = "20060102T150405.000Z"

func ParseTime(timeString string) time.Time {
	time, _ := time.Parse(cwlWarTimeFormat, timeString)
	return time
}

func PlayersByTag(players []clash.Player) map[string]clash.Player {
	playersByTag := make(map[string]clash.Player)
	for _, player := range players {
		playersByTag[player.Tag] = player
	}
	return playersByTag
}

type PlayerProgress[T any] struct {
	Previous clash.Player
	Current  clash.Player
	Data     T
}

type OwnerProgress[T any] struct {
	Owner   string
	Players []PlayerProgress[T]
}

func PlayersProgress[T any](previous, current []clash.Player, extractor func(previous, current clash.Player) T) []PlayerProgress[T] {
	var playersProgress []PlayerProgress[T]
	previousByTag := PlayersByTag(previous)
	for _, player := range current {
		var (
			previousPlayer clash.Player
			ok             bool
		)
		previousPlayer, ok = previousByTag[player.Tag]
		if !ok {
			previousPlayer = clash.Player{}
		}
		playersProgress = append(playersProgress, PlayerProgress[T]{
			Previous: previousPlayer,
			Current:  player,
			Data:     extractor(previousPlayer, player),
		})
	}
	return playersProgress
}

func PlayersProgressByOwner[T any](previous, current []clash.Player, extractor func(previous, current clash.Player) T, config configuration_manager.ConfigurationManager) []OwnerProgress[T] {
	ownersProgressMap := make(map[string]OwnerProgress[T])
	playersProgress := PlayersProgress(previous, current, extractor)
	for _, playerProgress := range playersProgress {
		owner, err := config.AccountOwner(playerProgress.Current.Tag)
		if err != nil {
			owner = ""
		}
		ownerProgress, ok := ownersProgressMap[owner]
		if !ok {
			ownerProgress = OwnerProgress[T]{Owner: owner, Players: []PlayerProgress[T]{}}
		}
		ownerProgress.Players = append(ownerProgress.Players, playerProgress)
		ownersProgressMap[owner] = ownerProgress
	}
	var ownersProgress []OwnerProgress[T]
	for _, ownerProgress := range ownersProgressMap {
		ownersProgress = append(ownersProgress, ownerProgress)
	}
	return ownersProgress
}
