package utils

func StatusCodeIsSuccess(statusCode int) bool {
	return statusCode >= 200 && statusCode < 300
}
