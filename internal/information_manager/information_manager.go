package information_manager

import (
	"context"
	"fmt"
	"log"
	"reflect"
	"runtime"
	"strings"
	"sync"

	"github.com/avast/retry-go"
	"gitlab.com/reinodovo/botdovo/internal/clash"
	"gitlab.com/reinodovo/botdovo/internal/configuration_manager"
	"gitlab.com/reinodovo/botdovo/internal/database"
	"gitlab.com/reinodovo/botdovo/internal/scheduler"
	"gitlab.com/reinodovo/botdovo/internal/utils"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
)

const (
	tracerName = "information_manager"
)

const schedulerIntervalContextKey string = "interval"

type Interval = string

const (
	Day    = scheduler.Day
	Minute = scheduler.Minute
	Monday = scheduler.Monday
	Month  = scheduler.Month
	Year   = scheduler.Year
)

type Data = int

const (
	Player Data = iota
	CapitalRaids
	CWLs
	HonoraryMembers
)

const (
	playerBucket          = "ClanMembers"
	capitalRaidsBucket    = "CapitalRaids"
	cwlsBucket            = "CWLs"
	honoraryMembersBucket = "HonoraryMembers"
)

type Trigger = func(context.Context) error

type entry struct {
	data     Data
	bucket   string
	key      string
	interval Interval
}

type observer struct {
	entry    entry
	callback interface{}
}

type InformationManager interface {
	GenerateTrigger(data Data, interval Interval, callback interface{}) (func(ctx context.Context) error, error)
	Subscribe(data Data, interval Interval, callback interface{}) (func(ctx context.Context) error, error)
}

type LocalDBInformationManager struct {
	db        database.Database
	config    configuration_manager.ConfigurationManager
	scheduler scheduler.Scheduler
	clashAPI  clash.ClashAPI
	logger    *log.Logger
	observers []observer
	entries   []entry
}

var lock = &sync.Mutex{}
var mgrSingleton *LocalDBInformationManager = nil

func getFunctionName(temp interface{}) string {
	strs := strings.Split((runtime.FuncForPC(reflect.ValueOf(temp).Pointer()).Name()), ".")
	return strs[len(strs)-1]
}

func NewLocalDBInformationManager(db database.Database, config configuration_manager.ConfigurationManager, scheduler scheduler.Scheduler, clashAPI clash.ClashAPI, logger *log.Logger) *LocalDBInformationManager {
	if mgrSingleton == nil {
		lock.Lock()
		defer lock.Unlock()
		if mgrSingleton == nil {
			mgrSingleton = &LocalDBInformationManager{
				db:        db,
				config:    config,
				clashAPI:  clashAPI,
				scheduler: scheduler,
				logger:    logger,
			}
			mgrSingleton.entries = []entry{
				{
					data:     Player,
					bucket:   playerBucket,
					key:      "minute",
					interval: Minute,
				},
				{
					data:     HonoraryMembers,
					bucket:   honoraryMembersBucket,
					key:      "minute",
					interval: Minute,
				},
				{
					data:     Player,
					bucket:   playerBucket,
					key:      "day",
					interval: Day,
				},
				{
					data:     Player,
					bucket:   playerBucket,
					key:      "week-monday",
					interval: Monday,
				},
				{
					data:     Player,
					bucket:   playerBucket,
					key:      "month",
					interval: Month,
				},
				{
					data:     Player,
					bucket:   playerBucket,
					key:      "year",
					interval: Year,
				},
				{
					data:     CapitalRaids,
					bucket:   capitalRaidsBucket,
					key:      "minute",
					interval: Minute,
				},
			}
			mgrSingleton.createSchedulings()
			mgrSingleton.createMissingDatabaseEntries()
		}
	}
	return mgrSingleton
}

func (mgr *LocalDBInformationManager) getData(ctx context.Context, data Data) (interface{}, error) {
	switch data {
	case Player:
		return mgr.clashAPI.MainClanMembers(ctx)
	case CapitalRaids:
		return mgr.clashAPI.MainClanCapitalRaids(ctx)
	case CWLs:
		return mgr.clashAPI.MainClanCWL(ctx)
	case HonoraryMembers:
		tags, err := mgr.config.HonoraryMembers()
		if err != nil {
			return nil, err
		}
		return mgr.clashAPI.Players(ctx, tags)
	default:
		return nil, fmt.Errorf("unavailable data %v", data)
	}
}

func (mgr *LocalDBInformationManager) createMissingDatabaseEntries() {
	for _, entry := range mgr.entries {
		var object any
		err := mgr.db.Get(context.Background(), entry.bucket, entry.key, &object)
		if err != nil {
			mgr.logger.Printf("could not find database entry for bucket %v, key %v, creating", entry.bucket, entry.key)
			object, err := mgr.getData(context.Background(), entry.data)
			if err != nil {
				log.Fatalf("could not get data when creating database entry: %v", err)
			}
			err = mgr.db.SaveObject(context.Background(), entry.bucket, entry.key, object)
			if err != nil {
				log.Fatalf("could not save members entry: %v", err)
			}
		}
	}
	mgr.saveCWL(context.Background())
}

func (mgr *LocalDBInformationManager) createEntryCallback(entry entry) scheduler.SchedulerCallback {
	return func(ctx context.Context) error {
		return mgr.notifyObservers(ctx, entry, true, mgr.observers)
	}
}

func (mgr *LocalDBInformationManager) createSchedulings() {
	for _, entry := range mgr.entries {
		_, err := mgr.scheduler.Schedule(entry.interval, mgr.createEntryCallback(entry))
		if err != nil {
			log.Fatalf("could not create schedulings for entry %v: %v", entry, err)
		}
	}
	_, err := mgr.scheduler.Schedule(scheduler.Day, mgr.saveCWL)
	if err != nil {
		log.Fatalf("could not create scheduling to save cwl")
	}
}

func (mgr *LocalDBInformationManager) saveCWL(ctx context.Context) error {
	ctx, span := otel.Tracer(tracerName).Start(ctx, "save_cwl")
	defer span.End()

	data, err := mgr.getData(ctx, CWLs)
	if err != nil {
		utils.RecordErrorIfNotNil(span, err)
		return err
	}

	if cwl, ok := data.(clash.CWL); ok {
		span.SetAttributes(attribute.String("season", cwl.Group.Season))
		span.SetAttributes(attribute.String("state", cwl.Group.State))

		if cwl.Group.State == clash.CWL_ENDED {
			var object any = nil
			err := mgr.db.Get(ctx, cwlsBucket, cwl.Group.Season, &object)
			utils.RecordErrorIfNotNil(span, err)

			if err != nil || object == nil {
				err = mgr.db.SaveObject(ctx, cwlsBucket, cwl.Group.Season, cwl)
				utils.RecordErrorIfNotNil(span, err)
			}
		}
	}
	return nil
}

func (mgr *LocalDBInformationManager) getEntryData(ctx context.Context, entry entry) (interface{}, interface{}, error) {
	current, err := mgr.getData(ctx, entry.data)
	if err != nil {
		return nil, nil, err
	}
	switch current.(type) {
	case []clash.Player:
		var previous []clash.Player
		err = mgr.db.Get(ctx, entry.bucket, entry.key, &previous)
		return previous, current, err
	case []clash.CapitalRaid:
		var previous []clash.CapitalRaid
		err = mgr.db.Get(ctx, entry.bucket, entry.key, &previous)
		return previous, current, err
	default:
		return nil, nil, fmt.Errorf("could not recognize data type")
	}
}

func callObserver(ctx context.Context, previous, current interface{}, obs observer) error {
	return retry.Do(func() error {
		switch callback := obs.callback.(type) {
		case func(ctx context.Context, previous, current []clash.Player) error:
			return callback(ctx, previous.([]clash.Player), current.([]clash.Player))
		case func(ctx context.Context, previous, current []clash.CapitalRaid) error:
			return callback(ctx, previous.([]clash.CapitalRaid), current.([]clash.CapitalRaid))
		default:
			return fmt.Errorf("callback %v does not implement a recognized type", getFunctionName(callback))
		}
	}, retry.Attempts(5))
}

func callObserversAboutEntry(ctx context.Context, entry entry, previous, current interface{}, observers []observer) error {
	ctx, span := otel.Tracer(tracerName).Start(ctx, "call_observers")
	defer span.End()

	span.SetAttributes(attribute.Int("data", entry.data))
	span.SetAttributes(attribute.String("key", entry.key))
	span.SetAttributes(attribute.Int("number_of_observers", len(observers)))

	var wg sync.WaitGroup
	for _, obs := range observers {
		if obs.entry.data == entry.data && obs.entry.key == entry.key {
			wg.Add(1)
			go func(obs observer) {
				defer wg.Done()

				ctx, span := otel.Tracer(tracerName).Start(ctx, "callback")
				defer span.End()

				span.SetAttributes(attribute.String("function_name", getFunctionName(obs.callback)))

				err := callObserver(ctx, previous, current, obs)

				utils.RecordErrorIfNotNil(span, err)
			}(obs)
		}
	}
	wg.Wait()
	return nil
}

func (mgr *LocalDBInformationManager) notifyObservers(ctx context.Context, entry entry, shouldSave bool, observers []observer) error {
	ctx, span := otel.Tracer(tracerName).Start(ctx, "notify_observers")
	defer span.End()

	span.SetAttributes(attribute.Bool("should_save", shouldSave))

	previous, current, err := mgr.getEntryData(ctx, entry)
	if err != nil {
		utils.RecordErrorIfNotNil(span, err)
		return err
	}

	err = callObserversAboutEntry(ctx, entry, previous, current, observers)
	if err != nil {
		utils.RecordErrorIfNotNil(span, err)
	}

	if shouldSave {
		return mgr.db.SaveObject(ctx, entry.bucket, entry.key, current)
	}
	return nil
}

func (mgr *LocalDBInformationManager) findEntry(data Data, interval Interval) (entry, error) {
	var targetEntry *entry = nil
	for _, entry := range mgr.entries {
		if entry.data == data && entry.interval == interval {
			targetEntry = &entry
			break
		}
	}
	if targetEntry == nil {
		return entry{}, fmt.Errorf("could not find entry")
	}
	return *targetEntry, nil
}

func validateCallback(data Data, callback interface{}) error {
	var ok bool = false
	switch data {
	case Player, HonoraryMembers:
		_, ok = callback.(func(ctx context.Context, previous, current []clash.Player) error)
	case CapitalRaids:
		_, ok = callback.(func(ctx context.Context, previous, current []clash.CapitalRaid) error)
	}
	if ok {
		return nil
	}
	return fmt.Errorf("callback %v is not compatible with data type %v", getFunctionName(callback), data)
}

func (mgr *LocalDBInformationManager) generateTrigger(obs observer) Trigger {
	return func(ctx context.Context) error {
		ctx, span := otel.Tracer(tracerName).Start(ctx, "trigger")
		defer span.End()

		span.SetAttributes(attribute.Int("data_type", obs.entry.data))
		span.SetAttributes(attribute.String("interval", obs.entry.interval))

		return mgr.notifyObservers(ctx, obs.entry, false, []observer{obs})
	}
}

func (mgr *LocalDBInformationManager) saveObserver(data Data, interval Interval, callback interface{}, shouldSchedule bool) (Trigger, error) {
	_, span := otel.Tracer(tracerName).Start(context.Background(), "save_observer")
	defer span.End()
	span.SetAttributes(attribute.Bool("should_schedule", shouldSchedule))

	targetEntry, err := mgr.findEntry(data, interval)
	if err != nil {
		utils.RecordErrorIfNotNil(span, err)
		return nil, err
	}
	err = validateCallback(data, callback)
	if err != nil {
		utils.RecordErrorIfNotNil(span, err)
		return nil, err
	}
	obs := observer{
		entry:    targetEntry,
		callback: callback,
	}
	if shouldSchedule {
		mgr.observers = append(mgr.observers, obs)
	}
	trigger := mgr.generateTrigger(obs)
	return trigger, nil
}

func (mgr *LocalDBInformationManager) GenerateTrigger(data Data, interval Interval, callback interface{}) (Trigger, error) {
	shouldSchedule := false
	return mgr.saveObserver(data, interval, callback, shouldSchedule)
}

func (mgr *LocalDBInformationManager) Subscribe(data Data, interval Interval, callback interface{}) (Trigger, error) {
	shouldSchedule := true
	return mgr.saveObserver(data, interval, callback, shouldSchedule)
}
