package device

import (
	"bytes"
	"context"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"time"

	"github.com/avast/retry-go"
	"gitlab.com/reinodovo/botdovo/internal/utils"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
)

const tracerName = "device"

const (
	trainPath  = "train"
	donatePath = "donate"
)

type Device interface {
	TrainTroops(ctx context.Context, troopIndex int, quantity int) (int, error)
	DonateTroops(ctx context.Context, day int, donations []int) error
	StartCWL(ctx context.Context) error
}

type DeviceClient struct{}

func NewDevice() Device {
	return &DeviceClient{}
}

func makePostRequest(ctx context.Context, path string, body []byte, response interface{}) error {
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{
		Timeout:   2 * time.Second,
		Transport: tr,
	}

	span := trace.SpanFromContext(ctx)
	span.SetAttributes(attribute.String("path", path))

	baseURL := os.Getenv("DEVICE_SERVER_URL")
	base, err := url.Parse(baseURL)
	if err != nil {
		utils.RecordErrorIfNotNil(span, err)
		return err
	}
	url := base.ResolveReference(&url.URL{Path: path})

	var res *http.Response
	err = retry.Do(func() error {
		req, err := http.NewRequest("POST", url.String(), bytes.NewBuffer(body))
		if err != nil {
			return err
		}
		req.Header.Set("Content-Type", "application/json")
		res, err = client.Do(req)
		if err != nil {
			return err
		}
		if !utils.StatusCodeIsSuccess(res.StatusCode) {
			return fmt.Errorf("expected success status code, got %v", res.StatusCode)
		}
		if response == nil {
			return nil
		}
		return json.NewDecoder(res.Body).Decode(response)
	}, retry.Attempts(5))
	utils.RecordErrorIfNotNil(span, err)

	return err
}

type TrainTroopsRequest struct {
	Index    int `json:"index"`
	Quantity int `json:"quantity"`
}

type TrainTroopsResponse struct {
	TrainingTime int `json:"training_time"`
}

func (d *DeviceClient) TrainTroops(ctx context.Context, troopIndex int, quantity int) (int, error) {
	ctx, span := otel.Tracer(tracerName).Start(ctx, "train_troops")
	defer span.End()

	body, err := json.Marshal(TrainTroopsRequest{Index: troopIndex, Quantity: quantity})
	if err != nil {
		utils.RecordErrorIfNotNil(span, err)
		return 0, err
	}

	var response TrainTroopsResponse
	err = makePostRequest(ctx, "train", body, &response)
	return response.TrainingTime, err
}

type DonateTroopsRequest struct {
	Day       int   `json:"day"`
	Donations []int `json:"donations"`
}

func (d *DeviceClient) DonateTroops(ctx context.Context, day int, donations []int) error {
	ctx, span := otel.Tracer(tracerName).Start(ctx, "donate_troops")
	defer span.End()

	body, err := json.Marshal(DonateTroopsRequest{Day: day, Donations: donations})
	if err != nil {
		utils.RecordErrorIfNotNil(span, err)
		return err
	}

	return makePostRequest(ctx, "donate", body, nil)
}

func (d *DeviceClient) StartCWL(ctx context.Context) error {
	ctx, span := otel.Tracer(tracerName).Start(ctx, "start_cwl")
	defer span.End()

	body := ([]byte)("{}")
	return makePostRequest(ctx, "start_cwl", body, nil)
}
