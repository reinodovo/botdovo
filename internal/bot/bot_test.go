package bot

import (
	"context"
	"reflect"
	"strings"
	"testing"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/stretchr/testify/mock"
	tracing_mocks "gitlab.com/reinodovo/botdovo/internal/mocks/tracing"
	api "gitlab.com/reinodovo/telegram-bot-api"
)

type BotAPIMock struct {
	sentMessages []string
}

func (api *BotAPIMock) Send(c tgbotapi.Chattable) (tgbotapi.Message, error) {
	if messageConfig, ok := c.(tgbotapi.MessageConfig); ok {
		api.sentMessages = append(api.sentMessages, strings.TrimSpace(messageConfig.Text))
	}
	return tgbotapi.Message{}, nil
}

func (api *BotAPIMock) SendToGroupOnly(message tgbotapi.Chattable) (tgbotapi.Message, error) {
	return api.Send(message)
}

func (api *BotAPIMock) SetBotUpdatesHandler(handler api.UpdateHandler) error {
	return nil
}

func (api *BotAPIMock) SetGroupUpdatesHandler(handler api.UpdateHandler) error {
	return nil
}

func NewBotAPIMock() BotAPIMock {
	return BotAPIMock{}
}

func TestHelpCommand(t *testing.T) {
	// setup
	mockSpan := &tracing_mocks.Span{}
	mockSpan.On("End")
	mockTracer := &tracing_mocks.Tracer{}
	mockTracer.On("Start", mock.Anything, mock.Anything).Return(nil, mockSpan)

	dummyCommand := func(ctx context.Context, args ...string) error { return nil }

	bot := NewTelegramBot([]Behaviour{})
	bot.tracing = mockTracer

	mockBotAPI := NewBotAPIMock()
	bot.api = &mockBotAPI

	bot.SetCommand(context.Background(), "command1", dummyCommand, "explanation1")
	bot.SetCommand(context.Background(), "command2", dummyCommand, "explanation2")
	bot.SetCommand(context.Background(), "command3", dummyCommand, "explanation3")

	expectedSentMessages := []string{`/help - mostra essa mensagem
/command1 - explanation1
/command2 - explanation2
/command3 - explanation3`}

	// act
	bot.helpCommand(context.Background())

	// test
	if !reflect.DeepEqual(mockBotAPI.sentMessages, expectedSentMessages) {
		t.Errorf("expected %v, got %v", expectedSentMessages, mockBotAPI.sentMessages)
	}
}
