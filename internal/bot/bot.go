package bot

import (
	"bytes"
	"context"
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"gitlab.com/reinodovo/botdovo/internal/utils"
	api "gitlab.com/reinodovo/telegram-bot-api"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
)

var (
	PinMessageError = fmt.Errorf("could not pin message")
)

type Behaviour func(message string) []string
type Callback func(ctx context.Context, text string, messageId int) error
type Command func(ctx context.Context, arguments ...string) error

type CommandInformation struct {
	command     Command
	explanation string
}

type Bot interface {
	SendMessage(ctx context.Context, text string) (int, error)
	SendMessageWithInlineKeyboard(ctx context.Context, text string, keyboard *tgbotapi.InlineKeyboardMarkup) (int, error)
	SendMessageAndPin(ctx context.Context, text string) (int, error)
	Reply(ctx context.Context, text string, messageId int) (int, error)
	EditMessage(ctx context.Context, text string, messageId int) error
	SetReplyCallback(ctx context.Context, messageId int, callback Callback) error
	SetCommand(ctx context.Context, name string, command Command, explanation string) error
	Start() error
}

type BotAPI interface {
	GetUpdatesChan(config tgbotapi.UpdateConfig) (tgbotapi.UpdatesChannel, error)
	Send(c tgbotapi.Chattable) (tgbotapi.Message, error)
	PinChatMessage(config tgbotapi.PinChatMessageConfig) (tgbotapi.APIResponse, error)
}

type TelegramBot struct {
	api            api.TelegramBotAPI
	chatID         int64
	ID             int64
	replyCallbacks map[int]Callback
	commands       map[string]CommandInformation
	commandOrder   []string
	behaviours     []Behaviour
	tracing        trace.Tracer
}

func NewTelegramBot(behaviours []Behaviour) *TelegramBot {
	ID, _ := strconv.ParseInt(os.Getenv("BOT_USER_ID"), 10, 64)
	chatID, _ := strconv.ParseInt(os.Getenv("CHAT_ID"), 10, 64)
	rabbitMQPort, _ := strconv.ParseInt(os.Getenv("RABBITMQ_PORT"), 10, 64)
	api, err := api.NewTelegramBot(api.TelegramConfiguration{
		Token:   os.Getenv("BOT_TOKEN"),
		Timeout: 60,
		ChatID:  chatID,
	}, api.RabbitMQStreamConfiguration{
		Host:       os.Getenv("RABBITMQ_HOST"),
		Port:       int(rabbitMQPort),
		User:       os.Getenv("RABBITMQ_USER"),
		Password:   os.Getenv("RABBITMQ_PASS"),
		StreamName: os.Getenv("RABBITMQ_STREAM_NAME"),
	})
	if err != nil && flag.Lookup("test.v") == nil {
		log.Panicf("error while starting telegram bot: %v", err)
	}
	bot := &TelegramBot{
		api:            api,
		chatID:         chatID,
		ID:             ID,
		commands:       make(map[string]CommandInformation),
		replyCallbacks: make(map[int]Callback),
		behaviours:     behaviours,
		tracing:        otel.Tracer("telegram_bot"),
	}
	bot.SetCommand(context.Background(), "help", bot.helpCommand, "mostra essa mensagem")
	return bot
}

func (bot *TelegramBot) SendMessage(ctx context.Context, text string) (int, error) {
	ctx, span := bot.tracing.Start(ctx, "send_message")
	defer span.End()

	msg := tgbotapi.NewMessage(bot.chatID, text)
	msg.ParseMode = "html"
	sentMsg, err := bot.api.Send(msg)
	if err != nil {
		return 0, err
	}
	return sentMsg.MessageID, nil
}

func (bot *TelegramBot) SendMessageWithInlineKeyboard(ctx context.Context, text string, keyboard *tgbotapi.InlineKeyboardMarkup) (int, error) {
	ctx, span := bot.tracing.Start(ctx, "send_message_with_inline_keyboard")
	defer span.End()

	msg := tgbotapi.NewMessage(bot.chatID, text)
	msg.ParseMode = "html"
	msg.ReplyMarkup = keyboard
	sentMsg, err := bot.api.Send(msg)
	if err != nil {
		return 0, err
	}
	return sentMsg.MessageID, nil
}

func (bot *TelegramBot) Reply(ctx context.Context, text string, messageId int) (int, error) {
	ctx, span := bot.tracing.Start(ctx, "reply")
	defer span.End()

	msg := tgbotapi.NewMessage(bot.chatID, text)
	msg.ParseMode = "html"
	msg.ReplyToMessageID = messageId
	sentMsg, err := bot.api.Send(msg)
	if err != nil {
		return 0, err
	}
	return sentMsg.MessageID, nil
}

func (bot *TelegramBot) EditMessage(ctx context.Context, text string, messageId int) error {
	ctx, span := bot.tracing.Start(ctx, "edit_message")
	defer span.End()

	msg := tgbotapi.NewEditMessageText(bot.chatID, messageId, text)
	msg.ParseMode = "html"
	_, err := bot.api.Send(msg)
	return err
}

func (bot *TelegramBot) SendMessageAndPin(ctx context.Context, text string) (int, error) {
	ctx, span := bot.tracing.Start(ctx, "send_message_and_pin")
	defer span.End()

	msgId, err := bot.SendMessage(ctx, text)
	utils.RecordErrorIfNotNil(span, err)
	span.SetAttributes(attribute.Int("message_id", msgId))
	if err != nil {
		return msgId, err
	}
	pinConfig := tgbotapi.PinChatMessageConfig{
		ChatID:              bot.chatID,
		MessageID:           msgId,
		DisableNotification: false,
	}
	_, err = bot.api.Send(pinConfig)
	utils.RecordErrorIfNotNil(span, err)
	if err != nil {
		return msgId, PinMessageError
	}
	return msgId, nil
}

func (bot *TelegramBot) SetReplyCallback(ctx context.Context, messageId int, callback Callback) error {
	ctx, span := bot.tracing.Start(ctx, "set_reply_callback")
	defer span.End()

	if _, ok := bot.replyCallbacks[messageId]; ok {
		return fmt.Errorf("there is already a callback for message %v", messageId)
	}
	bot.replyCallbacks[messageId] = callback
	return nil
}

func (bot *TelegramBot) SetCommand(ctx context.Context, name string, command Command, explanation string) error {
	ctx, span := bot.tracing.Start(ctx, "set_command")
	defer span.End()

	if _, ok := bot.commands[name]; ok {
		err := fmt.Errorf("command %v already exists", name)
		utils.RecordErrorIfNotNil(span, err)
		return err
	}
	bot.commandOrder = append(bot.commandOrder, name)
	bot.commands[name] = CommandInformation{
		command:     command,
		explanation: explanation,
	}
	return nil
}

func (bot *TelegramBot) helpCommandText() string {
	var buffer bytes.Buffer
	for _, command := range bot.commandOrder {
		info := bot.commands[command]
		buffer.WriteString(fmt.Sprintf("/%v - %v\n", command, strings.TrimSpace(info.explanation)))
	}
	return buffer.String()
}

func (bot *TelegramBot) helpCommand(ctx context.Context, args ...string) error {
	_, err := bot.SendMessage(ctx, bot.helpCommandText())
	return err
}

func fillTraceWithTelegramUpdateInformation(span trace.Span, update tgbotapi.Update) {
	if update.Message != nil && update.Message.Chat != nil {
		span.SetAttributes(attribute.String("user_name", update.Message.Chat.UserName))
		span.SetAttributes(attribute.Int64("chat_id", update.Message.Chat.ID))
	}
}

func (bot *TelegramBot) Start() error {
	err := bot.api.SetGroupUpdatesHandler(bot.handleUpdates)
	if err != nil {
		return err
	}
	err = bot.api.SetBotUpdatesHandler(bot.handleUpdates)
	if err != nil {
		return err
	}

	quitChannel := make(chan os.Signal, 1)
	signal.Notify(quitChannel, syscall.SIGINT, syscall.SIGTERM)
	<-quitChannel

	log.Printf("closing telegram bot...")
	return nil
}

func (bot *TelegramBot) handleCallbackQuery(ctx context.Context, update tgbotapi.Update) error {
	callbackQueryId := update.CallbackQuery.Message.MessageID
	callback, ok := bot.replyCallbacks[callbackQueryId]
	if ok {
		err := callback(ctx, update.CallbackQuery.Data, 0)
		return err
	}
	return nil
}

func (bot *TelegramBot) handleReplyCallback(ctx context.Context, update tgbotapi.Update) error {
	repliedMessageId := update.Message.ReplyToMessage.MessageID
	callback, ok := bot.replyCallbacks[repliedMessageId]
	if ok {
		err := callback(ctx, update.Message.Text, update.Message.MessageID)
		return err
	}
	return nil
}

func (bot *TelegramBot) handleCommand(ctx context.Context, update tgbotapi.Update) error {
	arguments := update.Message.CommandArguments()
	commandText := update.Message.Command()
	if info, ok := bot.commands[commandText]; ok {
		go func(ctx context.Context) error {
			ctx, span := otel.Tracer("bot").Start(ctx, commandText)
			defer span.End()
			var err error
			if len(arguments) == 0 {
				err = info.command(ctx)
			} else {
				err = info.command(ctx, strings.Fields(arguments)...)
			}
			utils.RecordErrorIfNotNil(span, err)
			return err
		}(context.Background())
	}
	return nil
}

func (bot *TelegramBot) handleBehaviours(ctx context.Context, update tgbotapi.Update) {
	for _, behaviour := range bot.behaviours {
		messages := behaviour(update.Message.Text)
		for _, message := range messages {
			bot.SendMessage(ctx, message)
		}
	}
}

func (bot *TelegramBot) handleUpdate(update tgbotapi.Update) error {
	ctx, span := bot.tracing.Start(context.Background(), "update_received")
	defer span.End()

	if update.Message != nil && (update.Message.Chat.ID != bot.chatID || update.Message.From.ID == bot.ID) {
		return nil
	}
	if update.CallbackQuery != nil {
		err := bot.handleCallbackQuery(ctx, update)
		utils.RecordErrorIfNotNil(span, err)
	}
	if update.Message != nil {
		if update.Message.ReplyToMessage != nil {
			err := bot.handleReplyCallback(ctx, update)
			utils.RecordErrorIfNotNil(span, err)
		}
		if update.Message.IsCommand() {
			err := bot.handleCommand(ctx, update)
			utils.RecordErrorIfNotNil(span, err)
		} else {
			bot.handleBehaviours(ctx, update)
		}
	}
	return nil
}

func (bot *TelegramBot) handleUpdates(updates tgbotapi.UpdatesChannel) {
	for update := range updates {
		bot.handleUpdate(update)
	}
}
