package cwl_sheets

import (
	"context"
	"fmt"
	"log"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/patrickmn/go-cache"
	"gitlab.com/reinodovo/botdovo/internal/clash"
	"gitlab.com/reinodovo/botdovo/internal/configuration_manager"
	"gitlab.com/reinodovo/botdovo/internal/cwl_planning"
	"gitlab.com/reinodovo/botdovo/internal/database"
	"gitlab.com/reinodovo/botdovo/internal/utils"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"

	"google.golang.org/api/sheets/v4"
)

var updateSheets = &sync.Mutex{}

const (
	tracerName = "cwl_sheets"
)

const (
	roundsCacheKey string = "rounds"
)

type CWLSheets struct {
	srv           *sheets.Service
	clashAPI      clash.ClashAPI
	cache         *cache.Cache
	config        configuration_manager.ConfigurationManager
	db            database.Database
	logger        *log.Logger
}

func NewCWLSheets(clashAPI clash.ClashAPI, cache *cache.Cache, config configuration_manager.ConfigurationManager, db database.Database, logger *log.Logger) *CWLSheets {
	cwlSheets := &CWLSheets{
		logger:   logger,
		cache:    cache,
		clashAPI: clashAPI,
		db:       db,
		config:   config,
	}
	var err error
	cwlSheets.srv, err = cwlSheets.GetSheetsService()
	if err != nil {
		log.Fatalf("an error occurred while getting spreadsheets service: %v", err)
	}
	return cwlSheets
}

func (cwlSheets *CWLSheets) validateSheetsId(id string) error {
	var err error
	_, err = cwlSheets.srv.Spreadsheets.Values.Get(id, cwlSheets.rosterRange()).Do()
	if err != nil {
		return err
	}
	_, err = cwlSheets.srv.Spreadsheets.Values.Get(id, cwlSheets.actualAttacksRange(0)).Do()
	if err != nil {
		return err
	}
	return nil
}

func (cwlSheets *CWLSheets) UnsetSheetsId(ctx context.Context) error {
	return cwlSheets.config.SetSheetsId("")
}

func (cwlSheets *CWLSheets) OverviewSheetsURL() (string, error) {
	id, err := cwlSheets.config.OverviewSheetsId()
	if err != nil {
		return "", nil
	}
	return fmt.Sprintf("https://docs.google.com/spreadsheets/d/%v", id), nil
}

func (cwlSheets *CWLSheets) SheetsURL() (string, error) {
	id, err := cwlSheets.config.PlanningSheetsId()
	if err != nil {
		return "", nil
	}
	return fmt.Sprintf("https://docs.google.com/spreadsheets/d/%v", id), nil
}

func (cwlSheets *CWLSheets) SetSheetsId(id string) error {
	err := cwlSheets.validateSheetsId(id)
	if err != nil {
		return err
	}
	err = cwlSheets.config.SetSheetsId(id)
	if err != nil {
		return err
	}
	err = cwlSheets.config.SetPlanningSheetsId(id)
	return err
}

func (cwlSheets *CWLSheets) UpdateToken() error {
	var err error
	cwlSheets.srv, err = cwlSheets.GetSheetsService()
	return err
}

func (cwlSheets *CWLSheets) donationsRange() string {
	return "DONATIONS"
}

func (cwlSheets *CWLSheets) rosterRange() string {
	return "ROSTER"
}

func (cwlSheets *CWLSheets) roundsOverviewRange() string {
	return "ROUNDS_OVERVIEW"
}

func (cwlSheets *CWLSheets) actualAttacksRange(round int) string {
	return fmt.Sprintf("ACTUAL_ATTACKS_ROUND%v", round+1)
}

func (cwlSheets *CWLSheets) roundRange(round int) string {
	return fmt.Sprintf("ROUND%v", round+1)
}

func (cwlSheets *CWLSheets) Rounds(ctx context.Context) int {
	ctx, span := otel.Tracer(tracerName).Start(ctx, "rounds")
	defer span.End()

	if rounds, found := cwlSheets.cache.Get(roundsCacheKey); found {
		span.SetAttributes(attribute.Bool("cache_hit", true))
		return rounds.(int)
	} else {
		span.SetAttributes(attribute.Bool("cache_hit", false))
		cwl, err := cwlSheets.clashAPI.MainClanCWL(ctx)
		if err != nil {
			cwlSheets.cache.Set(roundsCacheKey, 0, 1*time.Minute)
			return 0
		}
		rounds := len(cwl.Group.Rounds)
		cwlSheets.cache.Set(roundsCacheKey, rounds, cache.DefaultExpiration)
		return rounds
	}
}

func (cwlSheets *CWLSheets) WarSize(ctx context.Context) int {
	return 15
}

func (cwlSheets *CWLSheets) parseRow(row []interface{}) (*cwl_planning.IdealAttack, *cwl_planning.ActualAttack) {
	var (
		idealAttack  *cwl_planning.IdealAttack  = nil
		actualAttack *cwl_planning.ActualAttack = nil
		emptyString  string                     = ""
	)
	var stringRow []string
	for idx := 0; idx < 4; idx++ {
		if idx < len(row) {
			if field, ok := row[idx].(string); ok {
				stringRow = append(stringRow, field)
				continue
			}
		}
		stringRow = append(stringRow, emptyString)
	}
	if stringRow[0] != emptyString {
		stars, err := strconv.ParseFloat(stringRow[1], 32)
		if err == nil {
			idealAttack = &cwl_planning.IdealAttack{
				Player: stringRow[0],
				Stars:  float32(stars),
			}
		}
	}
	if stringRow[2] != emptyString {
		actualAttack = &cwl_planning.ActualAttack{
			Player:   stringRow[2],
			Stars:    0,
			Finished: false,
		}
		stars, err := strconv.ParseInt(stringRow[3], 10, 32)
		if err == nil {
			actualAttack.Stars = int(stars)
			actualAttack.Finished = true
		}
	}
	return idealAttack, actualAttack
}

func (cwlSheets *CWLSheets) AllAttacks(ctx context.Context, round int) ([]cwl_planning.IdealAttack, []cwl_planning.ActualAttack, error) {
	ctx, span := otel.Tracer(tracerName).Start(ctx, "all_attacks")
	defer span.End()

	if round < 0 || round > 6 {
		err := fmt.Errorf("%v is not a valid round (should be in range [0, 6]", round)
		utils.RecordErrorIfNotNil(span, err)
		return nil, nil, err
	}
	readRange := cwlSheets.roundRange(round)
	id, err := cwlSheets.config.SheetsId()
	if err != nil || len(id) == 0 {
		err = fmt.Errorf("invalid sheets id: %v", err)
		utils.RecordErrorIfNotNil(span, err)
		return nil, nil, err
	}
	resp, err := cwlSheets.srv.Spreadsheets.Values.Get(id, readRange).Do()
	if err != nil {
		err = fmt.Errorf("an error occurred while trying to read round %v at range %v: %v", round, readRange, err)
		utils.RecordErrorIfNotNil(span, err)
		return nil, nil, err
	}

	var (
		idealAttacks  []cwl_planning.IdealAttack
		actualAttacks []cwl_planning.ActualAttack
	)
	for position := 0; position < cwlSheets.WarSize(ctx); position++ {
		if len(resp.Values) > position {
			idealAttack, actualAttack := cwlSheets.parseRow(resp.Values[position])
			if idealAttack != nil {
				idealAttack.Position = position
				idealAttacks = append(idealAttacks, *idealAttack)
			}
			if actualAttack != nil {
				actualAttack.Position = position
				actualAttacks = append(actualAttacks, *actualAttack)
			}
		}
	}
	return idealAttacks, actualAttacks, nil
}

func (cwlSheets *CWLSheets) IdealAttacks(ctx context.Context, round int) ([]cwl_planning.IdealAttack, error) {
	ctx, span := otel.Tracer(tracerName).Start(ctx, "ideal_attacks")
	defer span.End()

	idealAttacks, _, err := cwlSheets.AllAttacks(ctx, round)
	utils.RecordErrorIfNotNil(span, err)
	return idealAttacks, err
}

func (cwlSheets *CWLSheets) ActualAttacks(ctx context.Context, round int) ([]cwl_planning.ActualAttack, error) {
	ctx, span := otel.Tracer(tracerName).Start(ctx, "actual_attacks")
	defer span.End()

	_, actualAttacks, err := cwlSheets.AllAttacks(ctx, round)
	utils.RecordErrorIfNotNil(span, err)
	return actualAttacks, err
}

func (cwlSheets *CWLSheets) UpdateActualAttacks(ctx context.Context, round int, attacks []cwl_planning.ActualAttack) error {
	ctx, span := otel.Tracer(tracerName).Start(ctx, "update_actual_attacks")
	defer span.End()

	if !cwlSheets.clashAPI.CWLIsHappening(ctx) {
		return fmt.Errorf("can't update sheets when there is no CWL happening")
	}
	if round < 0 || round > 6 {
		err := fmt.Errorf("%v is not a valid round (should be in range [0, 6]", round)
		utils.RecordErrorIfNotNil(span, err)
		return err
	}

	updateSheets.Lock()
	defer updateSheets.Unlock()

	_, actualAttacks, err := cwlSheets.AllAttacks(ctx, round)
	if err != nil {
		utils.RecordErrorIfNotNil(span, err)
		return err
	}

	var vr sheets.ValueRange
	vr.Values = make([][]interface{}, cwlSheets.WarSize(ctx))
	for idx := range vr.Values {
		vr.Values[idx] = make([]interface{}, 2)
	}
	actualAttacks = append(actualAttacks, attacks...)
	for _, attack := range actualAttacks {
		vr.Values[attack.Position][0] = attack.Player
		if attack.Finished {
			vr.Values[attack.Position][1] = attack.Stars
		}
	}

	id, err := cwlSheets.config.SheetsId()
	if err != nil || len(id) == 0 {
		err = fmt.Errorf("invalid sheets id: %v", err)
		utils.RecordErrorIfNotNil(span, err)
		return err
	}
	_, err = cwlSheets.srv.Spreadsheets.Values.Update(id, cwlSheets.actualAttacksRange(round), &vr).ValueInputOption("RAW").Do()
	utils.RecordErrorIfNotNil(span, err)
	return err
}

func (cwlSheets *CWLSheets) UpdateOverwrittenAttacks(ctx context.Context, round int, war clash.War) error {
	ctx, span := otel.Tracer(tracerName).Start(ctx, "update_overwritten_attacks")
	defer span.End()

	var overwrittenNames []string
	tags := cwlSheets.findOverwrittenAttackerTags(war)
	for _, tag := range tags {
		overwrittenNames = append(overwrittenNames, cwlSheets.findPlayerNameByTag(tag, war.Clan.Members))
	}

	id, err := cwlSheets.config.SheetsId()
	if err != nil || len(id) == 0 {
		err = fmt.Errorf("invalid sheets id: %v", err)
		utils.RecordErrorIfNotNil(span, err)
		return err
	}

	metadataId := cwlSheets.overwrittenAttacksMetadataId(round)
	metadata := &sheets.DeveloperMetadata{
		Location: &sheets.DeveloperMetadataLocation{
			Spreadsheet: true,
		},
		Visibility:    "DOCUMENT",
		MetadataId:    metadataId,
		MetadataKey:   "overwritten_attacks",
		MetadataValue: strings.Join(overwrittenNames, ";"),
	}
	span.SetAttributes(attribute.Int64("metadata_id", metadata.MetadataId))
	span.SetAttributes(attribute.String("metadata_value", metadata.MetadataValue))

	request := &sheets.Request{}

	requestType := ""
	response, err := cwlSheets.srv.Spreadsheets.DeveloperMetadata.Get(id, metadataId).Do()
	if response == nil {
		request.CreateDeveloperMetadata = &sheets.CreateDeveloperMetadataRequest{
			DeveloperMetadata: metadata,
		}
		requestType = "CREATE"
	} else {
		request.UpdateDeveloperMetadata = &sheets.UpdateDeveloperMetadataRequest{
			DataFilters: []*sheets.DataFilter{
				{
					DeveloperMetadataLookup: &sheets.DeveloperMetadataLookup{
						MetadataId: metadataId,
					},
				},
			},
			DeveloperMetadata: metadata,
			Fields:            "*",
		}
		requestType = "UPDATE"
	}
	span.SetAttributes(attribute.String("request_type", requestType))

	batchUpdateRequest := sheets.BatchUpdateSpreadsheetRequest{
		Requests: []*sheets.Request{request},
	}
	_, err = cwlSheets.srv.Spreadsheets.BatchUpdate(id, &batchUpdateRequest).Do()
	if err != nil {
		utils.RecordErrorIfNotNil(span, err)
		return err
	}
	return nil
}

func (cwlSheets *CWLSheets) overwrittenAttacksMetadataId(round int) int64 {
	return int64(round + 1)
}

func (cwlSheets *CWLSheets) findPlayerNameByTag(tag string, members []clash.WarPlayer) string {
	for _, member := range members {
		if member.Tag == tag {
			return member.Name
		}
	}
	return ""
}

func (cwlSheets *CWLSheets) findOverwrittenAttackerTags(war clash.War) []string {
	bestAttackerTags := cwlSheets.bestAttackerTags(war.Opponent.Members)
	allAttackerTags := cwlSheets.allAttackerTags(war.Clan.Members)
	var overwrittenAttackerTags []string
	for _, attackerTag := range allAttackerTags {
		wasOverwritten := true
		for _, bestAttackerTag := range bestAttackerTags {
			if attackerTag == bestAttackerTag {
				wasOverwritten = false
				break
			}
		}
		if wasOverwritten {
			overwrittenAttackerTags = append(overwrittenAttackerTags, attackerTag)
		}
	}
	return overwrittenAttackerTags
}

func (cwlSheets *CWLSheets) bestAttackerTags(opponents []clash.WarPlayer) []string {
	var bestAttackerTags []string
	for _, opponent := range opponents {
		if len(opponent.BestOpponentAttack.AttackerTag) == 0 {
			continue
		}
		bestAttackerTags = append(bestAttackerTags, opponent.BestOpponentAttack.AttackerTag)
	}
	return bestAttackerTags
}

func (cwlSheets *CWLSheets) allAttackerTags(members []clash.WarPlayer) []string {
	var attackerTags []string
	for _, member := range members {
		if len(member.Attacks) == 0 {
			continue
		}
		for _, attack := range member.Attacks {
			attackerTags = append(attackerTags, attack.AttackerTag)
		}
	}
	return attackerTags
}

func donationsFromRow(row []interface{}) (string, []int, error) {
	name := row[0].(string)
	quantities := make([]int, 0)
	for idx := 2; idx < len(row); idx++ {
		if quantityStr, ok := row[idx].(string); ok {
			quantity, err := strconv.ParseInt(quantityStr, 10, 64)
			if err != nil {
				quantities = append(quantities, 0)
			} else {
				quantities = append(quantities, int(quantity))
			}
		} else {
			return "", []int{}, fmt.Errorf("could not parse %v as string", row[idx])
		}
	}
	return name, quantities, nil
}

func playerRosterFromRow(row []interface{}, rounds int) cwl_planning.PlayerRoster {
	var playerRoster cwl_planning.PlayerRoster
	for idx := 0; idx < rounds; idx++ {
		var rosterAttack cwl_planning.RosterAttack
		if code, ok := row[idx].(string); ok {
			if len(code) > 0 {
				rosterAttack.AttackerCode = rune(code[0])
			}
			if len(code) > 1 {
				rosterAttack.MissedAttack = true
			}
		}
		playerRoster.Attacks = append(playerRoster.Attacks, rosterAttack)
	}
	if playerName, ok := row[rounds].(string); ok {
		playerRoster.Player = playerName
	}
	return playerRoster
}

func (cwlSheets *CWLSheets) GetRoster(ctx context.Context) ([]cwl_planning.PlayerRoster, error) {
	ctx, span := otel.Tracer(tracerName).Start(ctx, "get_roster")
	defer span.End()

	readRange := cwlSheets.rosterRange()
	id, err := cwlSheets.config.SheetsId()
	if err != nil || len(id) == 0 {
		err = fmt.Errorf("invalid sheets id: %v", err)
		utils.RecordErrorIfNotNil(span, err)
		return nil, err
	}
	resp, err := cwlSheets.srv.Spreadsheets.Values.Get(id, readRange).Do()
	if err != nil {
		utils.RecordErrorIfNotNil(span, err)
		return nil, fmt.Errorf("an error occurred while trying to read roster at range %v: %v", readRange, err)
	}

	var roster []cwl_planning.PlayerRoster
	rounds := cwlSheets.Rounds(ctx)
	for _, row := range resp.Values {
		if len(row) < rounds+1 {
			roster = append(roster, cwl_planning.PlayerRoster{})
			continue
		}
		playerRoster := playerRosterFromRow(row, rounds)
		roster = append(roster, playerRoster)
	}
	return roster, nil
}

func (cwlSheets *CWLSheets) UpdateRoster(ctx context.Context, roster []cwl_planning.PlayerRoster) error {
	ctx, span := otel.Tracer(tracerName).Start(ctx, "update_roster")
	defer span.End()

	if !cwlSheets.clashAPI.CWLIsHappening(ctx) {
		return fmt.Errorf("can't update sheets when there is no CWL happening")
	}
	updateSheets.Lock()
	defer updateSheets.Unlock()

	var vr sheets.ValueRange
	for _, playerRoster := range roster {
		var row []interface{}
		for _, attack := range playerRoster.Attacks {
			code := ""
			if attack.AttackerCode != 0 {
				code += string(attack.AttackerCode)
			}
			if attack.MissedAttack {
				code += "x"
			}
			row = append(row, code)
		}
		row = append(row, playerRoster.Player)
		vr.Values = append(vr.Values, row)
	}

	id, err := cwlSheets.config.SheetsId()
	if err != nil || len(id) == 0 {
		err = fmt.Errorf("invalid sheets id: %v", err)
		utils.RecordErrorIfNotNil(span, err)
		return err
	}
	_, err = cwlSheets.srv.Spreadsheets.Values.Update(id, cwlSheets.rosterRange(), &vr).ValueInputOption("RAW").Do()
	utils.RecordErrorIfNotNil(span, err)
	return err
}

func getRoundOverviewFromRow(row []interface{}) (cwl_planning.RoundOverview, error) {
	overview := cwl_planning.RoundOverview{}
	if starsStr, ok := row[0].(string); ok {
		stars, err := strconv.ParseInt(starsStr, 10, 64)
		if err != nil {
			return cwl_planning.RoundOverview{}, fmt.Errorf("could not parse %v as number of stars in round overview: %v", starsStr, err)
		}
		overview.Stars = int(stars)
	}
	if enemy, ok := row[1].(string); ok {
		overview.Enemy = enemy
	}
	if enemyStarsStr, ok := row[2].(string); ok {
		enemyStars, err := strconv.ParseInt(enemyStarsStr, 10, 64)
		if err != nil {
			return cwl_planning.RoundOverview{}, fmt.Errorf("could not parse %v as number of enemy stars in round overview: %v", enemyStarsStr, err)
		}
		overview.EnemyStars = int(enemyStars)
	}
	if wonStr, ok := row[3].(string); ok {
		won, err := strconv.ParseBool(wonStr)
		if err != nil {
			return cwl_planning.RoundOverview{}, fmt.Errorf("could not parse %v as a boolean in round overview: %v", wonStr, err)
		}
		overview.Won = won
	}
	return overview, nil
}

func (cwlSheets *CWLSheets) GetRoundsOverview(ctx context.Context) ([]cwl_planning.RoundOverview, error) {
	ctx, span := otel.Tracer(tracerName).Start(ctx, "get_rounds_overview")
	defer span.End()

	readRange := cwlSheets.roundsOverviewRange()
	id, err := cwlSheets.config.SheetsId()
	if err != nil || len(id) == 0 {
		err = fmt.Errorf("invalid sheets id: %v", err)
		utils.RecordErrorIfNotNil(span, err)
		return nil, err
	}
	resp, err := cwlSheets.srv.Spreadsheets.Values.Get(id, readRange).Do()
	if err != nil {
		utils.RecordErrorIfNotNil(span, err)
		return nil, fmt.Errorf("an error occurred while trying to read roster at range %v: %v", readRange, err)
	}

	var fieldCount = 4
	var overview []cwl_planning.RoundOverview
	for _, row := range resp.Values {
		if len(row) != fieldCount {
			continue
		}

		round, err := getRoundOverviewFromRow(row)
		if err != nil {
			utils.RecordErrorIfNotNil(span, err)
			return nil, fmt.Errorf("could not get round overview information from row: %v", err)
		}

		overview = append(overview, round)
	}
	return overview, nil
}

func (cwlSheets *CWLSheets) UpdateRoundsOverview(ctx context.Context, overview []cwl_planning.RoundOverview) error {
	ctx, span := otel.Tracer(tracerName).Start(ctx, "update_rounds_overview")
	defer span.End()

	if !cwlSheets.clashAPI.CWLIsHappening(ctx) {
		return fmt.Errorf("can't update sheets when there is no CWL happening")
	}
	if len(overview) > cwlSheets.Rounds(ctx) {
		err := fmt.Errorf("inserting %v elements into round overview would overflow the range", len(overview))
		utils.RecordErrorIfNotNil(span, err)
		return err
	}

	updateSheets.Lock()
	defer updateSheets.Unlock()

	var vr sheets.ValueRange
	for _, round := range overview {
		var row []interface{}
		row = append(row, round.Stars)
		row = append(row, round.Enemy)
		row = append(row, round.EnemyStars)
		row = append(row, round.Won)
		vr.Values = append(vr.Values, row)
	}

	id, err := cwlSheets.config.SheetsId()
	if err != nil || len(id) == 0 {
		err = fmt.Errorf("invalid sheets id: %v", err)
		utils.RecordErrorIfNotNil(span, err)
		return err
	}
	_, err = cwlSheets.srv.Spreadsheets.Values.Update(id, cwlSheets.roundsOverviewRange(), &vr).ValueInputOption("RAW").Do()
	utils.RecordErrorIfNotNil(span, err)
	return err
}

func (cwlSheets *CWLSheets) Donations(ctx context.Context) (cwl_planning.Donations, error) {
	ctx, span := otel.Tracer(tracerName).Start(ctx, "donations")
	defer span.End()

	readRange := cwlSheets.donationsRange()
	id, err := cwlSheets.config.SheetsId()
	if err != nil || len(id) == 0 {
		err = fmt.Errorf("invalid sheets id: %v", err)
		utils.RecordErrorIfNotNil(span, err)
		return nil, err
	}
	resp, err := cwlSheets.srv.Spreadsheets.Values.Get(id, readRange).Do()
	if err != nil {
		utils.RecordErrorIfNotNil(span, err)
		return nil, fmt.Errorf("an error occurred while trying to read donations at range %v: %v", readRange, err)
	}

	donations := make(cwl_planning.Donations)
	for _, row := range resp.Values {
		name, quantities, err := donationsFromRow(row)
		if err != nil {
			utils.RecordErrorIfNotNil(span, err)
			return donations, err
		}
		donations[name] = quantities
	}
	return donations, nil
}
