package cwl_sheets

import (
	"context"
	"net/http"
	"os"
	"time"

	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/option"
	"google.golang.org/api/sheets/v4"
)

const (
	authCollection = "auth"
	tokenKey       = "token"
)

const googleAPITimeout = 3 * time.Second

// Retrieve a token, saves the token, then returns the generated client.
func (cwlSheets *CWLSheets) getClient(config *oauth2.Config) (*http.Client, error) {
	// The file token.json stores the user's access and refresh tokens, and is
	// created automatically when the authorization flow completes for the first
	// time.
	tok, err := cwlSheets.tokenFromDatabase()
	if err != nil {
		return nil, err
	}
	return config.Client(context.Background(), tok), nil
}

func (cwlSheets *CWLSheets) GetAuthURL() (string, error) {
	config, err := getConfig()
	if err != nil {
		return "", err
	}
	url := config.AuthCodeURL("state-token", oauth2.AccessTypeOffline, oauth2.ApprovalForce)
	return url, nil
}

func (cwlSheets *CWLSheets) tokenFromDatabase() (*oauth2.Token, error) {
	var token oauth2.Token
	err := cwlSheets.db.Get(context.Background(), authCollection, tokenKey, &token)
	return &token, err
}

// Saves a token to a file path.
func (cwlSheets *CWLSheets) SaveToken(token string) error {
	config, err := getConfig()
	if err != nil {
		return err
	}
	tok, err := config.Exchange(context.TODO(), token)
	if err != nil {
		return err
	}
	err = cwlSheets.db.SaveObject(context.Background(), authCollection, tokenKey, *tok)
	if err != nil {
		return err
	}
	return nil
}

func getConfig() (*oauth2.Config, error) {
	b := []byte(os.Getenv("GOOGLE_CREDENTIALS"))
	return google.ConfigFromJSON(b, "https://www.googleapis.com/auth/spreadsheets")
}

func (cwlSheets *CWLSheets) GetSheetsService() (*sheets.Service, error) {
	config, err := getConfig()
	if err != nil {
		return nil, err
	}
	client, err := cwlSheets.getClient(config)
	if err != nil {
		return nil, err
	}
	client.Timeout = googleAPITimeout

	ctx := context.Background()
	srv, err := sheets.NewService(ctx, option.WithHTTPClient(client))
	return srv, err
}
