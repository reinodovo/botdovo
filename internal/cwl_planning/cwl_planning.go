package cwl_planning

import (
	"context"

	"gitlab.com/reinodovo/botdovo/internal/clash"
)

type IdealAttack struct {
	Player   string
	Stars    float32
	Position int
}

type ActualAttack struct {
	Player   string
	Stars    int
	Position int
	Finished bool
}

type RosterAttack struct {
	AttackerCode rune
	MissedAttack bool
}

type PlayerRoster struct {
	Player  string
	Attacks []RosterAttack
}

type RoundOverview struct {
	Stars      int
	Won        bool
	Enemy      string
	EnemyStars int
}

type Donations = map[string][]int

type CWLPlanning interface {
	Rounds(ctx context.Context) int
	WarSize(ctx context.Context) int
	AllAttacks(ctx context.Context, round int) ([]IdealAttack, []ActualAttack, error)
	IdealAttacks(ctx context.Context, round int) ([]IdealAttack, error)
	ActualAttacks(ctx context.Context, round int) ([]ActualAttack, error)
	UpdateActualAttacks(ctx context.Context, round int, attacks []ActualAttack) error
	GetRoster(ctx context.Context) ([]PlayerRoster, error)
	UpdateRoster(ctx context.Context, roster []PlayerRoster) error
	GetRoundsOverview(ctx context.Context) ([]RoundOverview, error)
	UpdateRoundsOverview(ctx context.Context, overview []RoundOverview) error
	UpdateOverwrittenAttacks(ctx context.Context, round int, war clash.War) error
	Donations(ctx context.Context) (Donations, error)
}
