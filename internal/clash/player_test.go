package clash

import (
	"reflect"
	"testing"
)

func TestFindAchieventment(t *testing.T) {
	// setup
	player := Player{
		Achievements: []Achievement{
			{Name: "achievement1", Value: 50},
			{Name: "achievement2", Value: 60},
			{Name: "achievement3", Value: 100},
		},
	}
	tests := []struct {
		achievementName     string
		expectedAchievement Achievement
		shouldFail          bool
	}{
		{
			achievementName:     "achievement1",
			expectedAchievement: Achievement{Name: "achievement1", Value: 50},
			shouldFail:          false,
		},
		{
			achievementName:     "achievement3",
			expectedAchievement: Achievement{Name: "achievement3", Value: 100},
			shouldFail:          false,
		},
		{
			achievementName:     "achievement2",
			expectedAchievement: Achievement{Name: "achievement2", Value: 60},
			shouldFail:          false,
		},
		{
			achievementName: "achievement5",
			shouldFail:      true,
		},
	}
	for idx, test := range tests {
		// act
		achievement, err := player.FindAchieventment(test.achievementName)
		// test
		if test.shouldFail != (err != nil) {
			t.Errorf("test %v has shouldFail %v, but err is %v", idx, test.shouldFail, err)
			continue
		}
		if !reflect.DeepEqual(achievement, test.expectedAchievement) {
			t.Errorf("expected %v to be %v in test %v", achievement, test.expectedAchievement, idx)
		}
	}
}
