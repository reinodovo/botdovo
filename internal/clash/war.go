package clash

type WarAttack struct {
	AttackerTag           string
	DefenderTag           string
	Stars                 int
	DestructionPercentage int
	Order                 int
	Duration              int
}

type WarPlayer struct {
	Tag                string
	Name               string
	MapPosition        int
	TownHallLevel      int
	OpponentAttacks    int
	Attacks            []WarAttack
	BestOpponentAttack WarAttack
}

type WarClan struct {
	Tag                   string
	Name                  string
	ClanLevel             int
	Attacks               int
	Stars                 int
	DestructionPercentage float64
	Members               []WarPlayer
}

type War struct {
	State                string
	TeamSize             int
	PreparationStartTime string
	StartTime            string
	EndTime              string
	Clan                 WarClan
	Opponent             WarClan
}
