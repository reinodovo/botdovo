package clash

type Achievement struct {
	Name           string
	Stars          int64
	Value          int64
	Target         int64
	Info           string
	CompletionInfo string
	Village        string
}
