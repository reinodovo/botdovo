package clash

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"sync"
	"time"

	"github.com/avast/retry-go"
	"github.com/patrickmn/go-cache"
	"gitlab.com/reinodovo/botdovo/internal/configuration_manager"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
)

const (
	tracerName = "clash"
)

const (
	CLAN_ROUTE                 = "https://cocproxy.royaleapi.dev/v1/clans/%v"
	PLAYER_ROUTE               = "https://cocproxy.royaleapi.dev/v1/players/%v"
	LEAGUE_GROUP_ROUTE         = "https://cocproxy.royaleapi.dev/v1/clans/%v/currentwar/leaguegroup"
	LEAGUE_WAR_ROUTE           = "https://cocproxy.royaleapi.dev/v1/clanwarleagues/wars/%v"
	CAPITAL_RAID_SEASONS_ROUTE = "https://cocproxy.royaleapi.dev/v1/clans/%v/capitalraidseasons"
)

const (
	WAR_ENDED = "warEnded"
	CWL_ENDED = "ended"
)

const (
	MAX_RETRIES = 5
)

type ClashAPI interface {
	Clan(ctx context.Context, tag string) (Clan, error)
	Player(ctx context.Context, tag string) (Player, error)
	Players(ctx context.Context, tags []string) ([]Player, error)
	Members(ctx context.Context, clan Clan) ([]Player, error)
	MainClanMembers(ctx context.Context) ([]Player, error)
	Wars(ctx context.Context, info WarDayInfo) ([]War, error)
	CurrentWar(ctx context.Context, clanTag string) (War, int, error)
	MainClanCurrentWar(ctx context.Context) (War, int, error)
	MainClanCurrentPreparation(ctx context.Context) (War, int, error)
	MainClanCWLWar(ctx context.Context, day int) (War, error)
	MainClanCWL(ctx context.Context) (CWL, error)
	CWLIsHappening(ctx context.Context) bool
	MainClanCapitalRaids(ctx context.Context) ([]CapitalRaid, error)
}

type Client struct {
	cache       *cache.Cache
	config      configuration_manager.ConfigurationManager
	token       string
	mainClanTag string
}

func NewClashAPIClient(cache *cache.Cache, config configuration_manager.ConfigurationManager) *Client {
	return &Client{
		cache:       cache,
		config:      config,
		token:       os.Getenv("API_TOKEN"),
		mainClanTag: os.Getenv("CLAN_TAG"),
	}
}

func (c *Client) Clan(ctx context.Context, tag string) (Clan, error) {
	ctx, span := otel.Tracer(tracerName).Start(ctx, "clan")
	defer span.End()

	var clan Clan
	err := unmarshal(ctx, fmt.Sprintf(CLAN_ROUTE, url.PathEscape(tag)), c.token, &clan)
	return clan, err
}

func playerCacheKey(tag string) string {
	return fmt.Sprintf("player_%v", tag)
}

func (c *Client) Player(ctx context.Context, tag string) (Player, error) {
	ctx, span := otel.Tracer(tracerName).Start(ctx, "player")
	defer span.End()

	var player Player
	cacheKey := playerCacheKey(tag)
	if cachedPlayer, found := c.cache.Get(cacheKey); found {
		span.SetAttributes(attribute.Bool("cache_hit", true))
		player = cachedPlayer.(Player)
	} else {
		span.SetAttributes(attribute.Bool("cache_hit", false))
		err := unmarshal(ctx, fmt.Sprintf(PLAYER_ROUTE, url.PathEscape(tag)), c.token, &player)
		if err != nil {
			return Player{}, err
		}
		c.cache.Set(cacheKey, player, 30*time.Second)
	}
	return player, nil
}

func (c *Client) Players(ctx context.Context, tags []string) ([]Player, error) {
	ctx, span := otel.Tracer(tracerName).Start(ctx, "players")
	defer span.End()

	var players []Player

	var wg sync.WaitGroup
	var playersLock sync.Mutex
	errors := make(chan error)
	done := make(chan bool)
	for _, tag := range tags {
		wg.Add(1)
		go func(tag string) {
			player, err := c.Player(ctx, tag)
			if err != nil {
				errors <- err
			} else {
				playersLock.Lock()
				players = append(players, player)
				playersLock.Unlock()
			}
			wg.Done()
		}(tag)
	}

	go func() {
		wg.Wait()
		close(done)
	}()

	select {
	case <-done:
		return players, nil
	case err := <-errors:
		return players, err
	}
}

func (c *Client) Members(ctx context.Context, clan Clan) ([]Player, error) {
	ctx, span := otel.Tracer(tracerName).Start(ctx, "members")
	defer span.End()

	tags := make([]string, len(clan.MemberList))
	for index, member := range clan.MemberList {
		tags[index] = member.Tag
	}

	return c.Players(ctx, tags)
}

func (c *Client) MainClanMembers(ctx context.Context) ([]Player, error) {
	ctx, span := otel.Tracer(tracerName).Start(ctx, "main_clan_members")
	defer span.End()

	tags, err := c.config.MemberTags()
	if err != nil {
		return nil, err
	}

	return c.Players(ctx, tags)
}

func (c *Client) Wars(ctx context.Context, info WarDayInfo) ([]War, error) {
	ctx, span := otel.Tracer(tracerName).Start(ctx, "wars")
	defer span.End()

	EMPTY_WAR := "#0"
	var wars []War
	for _, warTag := range info.WarTags {
		if warTag == EMPTY_WAR {
			continue
		}
		var war War
		warCacheKey := fmt.Sprintf("war_cache|%v", warTag)
		if cachedWar, found := c.cache.Get(warCacheKey); found {
			span.SetAttributes(attribute.Bool("cache_hit", true))
			war = cachedWar.(War)
		} else {
			span.SetAttributes(attribute.Bool("cache_hit", false))
			err := unmarshal(ctx, fmt.Sprintf(LEAGUE_WAR_ROUTE, url.PathEscape(warTag)), c.token, &war)
			if err != nil {
				return nil, err
			}
			if war.State == WAR_ENDED {
				c.cache.Set(warCacheKey, war, cache.DefaultExpiration)
			}
		}
		wars = append(wars, war)
	}
	return wars, nil
}

func (c *Client) currentWarInState(ctx context.Context, clanTag string, state string) (War, int, error) {
	ctx, span := otel.Tracer(tracerName).Start(ctx, "current_war_in_state")
	defer span.End()

	var leagueGroup LeagueGroup
	err := unmarshal(ctx, fmt.Sprintf(LEAGUE_GROUP_ROUTE, url.PathEscape(clanTag)), c.token, &leagueGroup)
	if err != nil {
		return War{}, 0, err
	}

	for round := len(leagueGroup.Rounds) - 1; round >= 0; round-- {
		dayWars, err := c.Wars(ctx, leagueGroup.Rounds[round])
		if err != nil {
			return War{}, 0, err
		}
		for _, war := range dayWars {
			if war.State != state {
				continue
			}
			if war.Clan.Tag == clanTag {
				return war, round, nil
			}
			if war.Opponent.Tag == clanTag {
				war.Clan, war.Opponent = war.Opponent, war.Clan
				return war, round, nil
			}
		}
	}
	return War{}, 0, fmt.Errorf("could not find any war in state %v for clan %v", state, clanTag)
}

func (c *Client) CurrentWar(ctx context.Context, clanTag string) (War, int, error) {
	ctx, span := otel.Tracer(tracerName).Start(ctx, "current_war")
	defer span.End()

	state := "inWar"
	return c.currentWarInState(ctx, clanTag, state)
}

func (c *Client) MainClanCurrentWar(ctx context.Context) (War, int, error) {
	ctx, span := otel.Tracer(tracerName).Start(ctx, "main_clan_current_war")
	defer span.End()

	return c.CurrentWar(ctx, c.mainClanTag)
}

func (c *Client) MainClanCurrentPreparation(ctx context.Context) (War, int, error) {
	ctx, span := otel.Tracer(tracerName).Start(ctx, "main_clan_current_preparation")
	defer span.End()

	state := "preparation"
	return c.currentWarInState(ctx, c.mainClanTag, state)
}

func (c *Client) MainClanCWLWar(ctx context.Context, round int) (War, error) {
	ctx, span := otel.Tracer(tracerName).Start(ctx, "main_clan_cwl_war")
	defer span.End()
	span.SetAttributes(attribute.Int("round", round))

	if round < 0 || round > 6 {
		return War{}, fmt.Errorf("%v is not a valid round for CWL war", round)
	}

	var leagueGroup LeagueGroup
	err := unmarshal(ctx, fmt.Sprintf(LEAGUE_GROUP_ROUTE, url.PathEscape(c.mainClanTag)), c.token, &leagueGroup)
	if err != nil {
		return War{}, err
	}

	wars, err := c.Wars(ctx, leagueGroup.Rounds[round])
	if err != nil {
		return War{}, err
	}

	for _, war := range wars {
		if war.Clan.Tag == c.mainClanTag {
			return war, nil
		}
		if war.Opponent.Tag == c.mainClanTag {
			war.Clan, war.Opponent = war.Opponent, war.Clan
			return war, nil
		}
	}
	return War{}, fmt.Errorf("could not find round %v war for clan %v", round, c.mainClanTag)
}

func (c *Client) MainClanCWL(ctx context.Context) (CWL, error) {
	ctx, span := otel.Tracer(tracerName).Start(ctx, "main_clan_cwl")
	defer span.End()

	var cwl CWL

	err := unmarshal(ctx, fmt.Sprintf(LEAGUE_GROUP_ROUTE, url.PathEscape(c.mainClanTag)), c.token, &cwl.Group)
	if err != nil {
		return CWL{}, err
	}

	cwl.Wars = make([][]War, len(cwl.Group.Rounds))
	for round := 0; round < len(cwl.Group.Rounds); round++ {
		cwl.Wars[round], err = c.Wars(ctx, cwl.Group.Rounds[round])
		if err != nil {
			return CWL{}, err
		}
	}
	return cwl, nil
}

func (c *Client) CWLIsHappening(ctx context.Context) bool {
	ctx, span := otel.Tracer(tracerName).Start(ctx, "cwl_is_happening")
	defer span.End()

	var leagueGroup LeagueGroup
	err := unmarshal(ctx, fmt.Sprintf(LEAGUE_GROUP_ROUTE, url.PathEscape(c.mainClanTag)), c.token, &leagueGroup)
	return err == nil
}

func (c *Client) MainClanCapitalRaids(ctx context.Context) ([]CapitalRaid, error) {
	ctx, span := otel.Tracer(tracerName).Start(ctx, "main_clan_capital_raid_seasons")
	defer span.End()

	var capitalRaidSeasons CapitalRaidSeasons
	err := unmarshal(ctx, fmt.Sprintf(CAPITAL_RAID_SEASONS_ROUTE, url.PathEscape(c.mainClanTag)), c.token, &capitalRaidSeasons)
	return capitalRaidSeasons.Items, err
}

func statusCodeIsSuccess(statusCode int) bool {
	return statusCode >= 200 && statusCode < 300
}

func unmarshal(ctx context.Context, url string, token string, object interface{}) error {
	client := &http.Client{
		Timeout: 10 * time.Second,
	}

	span := trace.SpanFromContext(ctx)
	span.SetAttributes(attribute.String("path", url))

	var res *http.Response
	err := retry.Do(func() error {
		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			return err
		}
		req.Header.Set("authorization", fmt.Sprintf("Bearer %s", token))
		res, err = client.Do(req)
		if err != nil {
			return err
		}
		if !statusCodeIsSuccess(res.StatusCode) {
			return fmt.Errorf("expected success status code, got %v", res.StatusCode)
		}
		return nil
	}, retry.Attempts(5))

	if err != nil {
		span.RecordError(err)
		span.SetStatus(codes.Error, err.Error())
		return err
	}

	span.SetAttributes(attribute.Int("status_code", res.StatusCode))

	return json.NewDecoder(res.Body).Decode(object)
}
