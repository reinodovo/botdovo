package clash

type Clan struct {
	Name       string
	Tag        string
	MemberList []Player
	Members    int64
}
