package clash

type CapitalRaidSeasons struct {
	Items []CapitalRaid
}

type CapitalRaid struct {
	State                   string
	StartTime               string
	EndTime                 string
	CapitalTotalLoot        int
	RaidsCompleted          int
	TotalAttacks            int
	EnemyDistrictsDestroyed int
	OffensiveReward         int
	DefensiveReward         int
	Members                 []CapitalRaidMember
	AttackLog               []CapitalRaidAttackLog
	DefenseLog              []CapitalRaidDefenseLog
}

type CapitalRaidMember struct {
	Tag                    string
	Name                   string
	Attacks                int
	AttackLimit            int
	BonusAttackLimit       int
	CapitalResourcesLooted int
}

type CapitalRaidAttackLog struct {
	Defender CapitalRaidClan
	CapitalRaidLog
}

type CapitalRaidDefenseLog struct {
	Attacker CapitalRaidClan
	CapitalRaidLog
}

type CapitalRaidLog struct {
	Defender           CapitalRaidClan
	AttackCount        int
	DistrictCount      int
	DistrictsDestroyed int
	Districts          []CapitalRaidDistrict
}

type CapitalRaidClan struct {
	Tag   string
	Name  string
	Level int
}

type CapitalRaidDistrict struct {
	Id                 int
	Name               string
	DistrictHallLevel  int
	DestructionPercent int
	Stars              int
	AttackCount        int
	TotalLooted        int
}
