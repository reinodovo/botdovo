package clash

import "fmt"

type Player struct {
	Name                string
	Tag                 string
	ExpLevel            int64
	TownHallLevel       int64
	TownHallWeaponLevel int64
	VersusTrophies      int64
	VersusBattleWins    int64
	Achievements        []Achievement
	Troops              []Army
	Spells              []Army
	Heroes              []Army
}

func (player *Player) FindAchieventment(name string) (Achievement, error) {
	for _, achievement := range player.Achievements {
		if achievement.Name == name {
			return achievement, nil
		}
	}
	return Achievement{}, fmt.Errorf("could not find achievement with name '%v'\n", name)
}
