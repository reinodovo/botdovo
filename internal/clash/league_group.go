package clash

type LeagueGroupMember struct {
	Tag           string
	Name          string
	TownHallLevel int
}

type LeagueGroupClan struct {
	Tag       string
	Name      string
	ClanLevel int
	Members   []LeagueGroupMember
}

type WarDayInfo struct {
	WarTags []string
}

type LeagueGroup struct {
	State  string
	Season string
	Clans  []LeagueGroupClan
	Rounds []WarDayInfo
}
