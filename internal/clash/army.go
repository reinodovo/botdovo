package clash

type Army struct {
	Name               string
	Level              int64
	MaxLevel           int64
	Village            string
	SuperTroopIsActive bool
}
