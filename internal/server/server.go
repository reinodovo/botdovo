package server

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"

	"go.opentelemetry.io/contrib/instrumentation/net/http/otelhttp"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"

	"gitlab.com/reinodovo/botdovo/internal/clash"
	"gitlab.com/reinodovo/botdovo/internal/cwl_planning"
	"gitlab.com/reinodovo/botdovo/internal/cwl_planning/cwl_sheets"
	"gitlab.com/reinodovo/botdovo/internal/utils"
)

type Server struct {
	cwlSheets *cwl_sheets.CWLSheets
	clashAPI  clash.ClashAPI
}

func NewServer(cwlSheets *cwl_sheets.CWLSheets, clashAPI clash.ClashAPI) Server {
	return Server{cwlSheets: cwlSheets, clashAPI: clashAPI}
}

func (server *Server) handleCurrentWar(w http.ResponseWriter, req *http.Request) {
	ctx := req.Context()
	span := trace.SpanFromContext(ctx)

	war, round, err := server.clashAPI.MainClanCurrentWar(ctx)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		utils.RecordErrorIfNotNil(span, err)
		return
	}

	type currentWar struct {
		War   clash.War
		Round int
	}

	jsonResponse, err := json.Marshal(currentWar{
		War:   war,
		Round: round,
	})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		utils.RecordErrorIfNotNil(span, err)
		return
	}
	w.Write(jsonResponse)
}

func (server *Server) handleRound(w http.ResponseWriter, req *http.Request) {
	var (
		rounds []string
		ok     bool
	)

	if rounds, ok = req.URL.Query()["round"]; !ok || len(rounds) != 1 {
		http.Error(w, "could not find 'round' querystring value", http.StatusBadRequest)
		return
	}

	ctx := req.Context()
	span := trace.SpanFromContext(ctx)

	round, err := strconv.ParseInt(rounds[0], 10, 32)
	if err != nil {
		http.Error(w, "'round' querystring value is not valid", http.StatusBadRequest)
		utils.RecordErrorIfNotNil(span, err)
		return
	}

	war, err := server.clashAPI.MainClanCWLWar(ctx, int(round))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		utils.RecordErrorIfNotNil(span, err)
		return
	}

	type warInfo struct {
		War   clash.War
		Round int
	}

	jsonResponse, err := json.Marshal(warInfo{
		War:   war,
		Round: int(round),
	})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		utils.RecordErrorIfNotNil(span, err)
		return
	}
	w.Write(jsonResponse)
}

func (server *Server) handlePlayerAttacks(w http.ResponseWriter, req *http.Request) {
	if player, ok := req.URL.Query()["player"]; ok && len(player) > 0 {
		ctx := req.Context()
		span := trace.SpanFromContext(ctx)

		playerCode := player[0]
		span.SetAttributes(attribute.String("player", playerCode))

		war, round, err := server.clashAPI.MainClanCurrentWar(ctx)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			utils.RecordErrorIfNotNil(span, err)
			return
		}

		roster, err := server.cwlSheets.GetRoster(ctx)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			utils.RecordErrorIfNotNil(span, err)
			return
		}

		allIdealAttacks, allActualAttacks, err := server.cwlSheets.AllAttacks(ctx, round)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			utils.RecordErrorIfNotNil(span, err)
			return
		}

		playerAccounts := make(map[string]bool)
		for _, playerRoster := range roster {
			if len(playerRoster.Attacks) > round && string(playerRoster.Attacks[round].AttackerCode) == playerCode {
				playerAccounts[playerRoster.Player] = true
			}
		}

		type allPlayerAttacks struct {
			War           clash.War
			Round         int
			IdealAttacks  []cwl_planning.IdealAttack
			ActualAttacks []cwl_planning.ActualAttack
		}
		playerAttacks := allPlayerAttacks{
			War:   war,
			Round: round,
		}

		for _, idealAttack := range allIdealAttacks {
			if result, ok := playerAccounts[idealAttack.Player]; result && ok {
				playerAttacks.IdealAttacks = append(playerAttacks.IdealAttacks, idealAttack)
			}
		}
		for _, actualAttack := range allActualAttacks {
			if result, ok := playerAccounts[actualAttack.Player]; result && ok {
				playerAttacks.ActualAttacks = append(playerAttacks.ActualAttacks, actualAttack)
			}
		}

		jsonResponse, err := json.Marshal(playerAttacks)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			utils.RecordErrorIfNotNil(span, err)
			return
		}
		w.Write(jsonResponse)
	} else {
		http.Error(w, "could not find 'player' querystring value", http.StatusBadRequest)
	}
}

func (server *Server) handleCWLSheetsRedirect(w http.ResponseWriter, req *http.Request) {
	ctx := req.Context()
	span := trace.SpanFromContext(ctx)

	url, err := server.cwlSheets.SheetsURL()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		utils.RecordErrorIfNotNil(span, err)
		return
	}
	http.Redirect(w, req, url, http.StatusTemporaryRedirect)
}

func (server *Server) handleCWLSheetsAuthToken(w http.ResponseWriter, req *http.Request) {
	ctx := req.Context()
	span := trace.SpanFromContext(ctx)

	code := req.FormValue("code")
	span.SetAttributes(attribute.String("code", code))
	saveErr := server.cwlSheets.SaveToken(code)
	updateErr := server.cwlSheets.UpdateToken()
	if saveErr != nil || updateErr != nil {
		http.Error(w, "", http.StatusInternalServerError)
		utils.RecordErrorIfNotNil(span, saveErr)
		utils.RecordErrorIfNotNil(span, updateErr)
	} else {
		w.WriteHeader(http.StatusOK)
	}
}

func (server *Server) Serve() {
	http.Handle("/api/pending_attacks", otelhttp.NewHandler(http.HandlerFunc(server.handlePlayerAttacks), "pending_attacks"))
	http.Handle("/api/current_war", otelhttp.NewHandler(http.HandlerFunc(server.handleCurrentWar), "current_war"))
	http.Handle("/api/round", otelhttp.NewHandler(http.HandlerFunc(server.handleRound), "round"))
	http.Handle("/planning", otelhttp.NewHandler(http.HandlerFunc(server.handleCWLSheetsRedirect), "planning"))
	http.Handle("/auth", otelhttp.NewHandler(http.HandlerFunc(server.handleCWLSheetsAuthToken), "oauth"))
	http.Handle("/token", otelhttp.NewHandler(http.HandlerFunc(server.handleCWLSheetsAuthToken), "oauth"))

	log.Fatal(http.ListenAndServe(":5000", nil))
}
