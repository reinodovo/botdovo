package cwl

import (
	"log"

	"gitlab.com/reinodovo/botdovo/internal/bot"
	"gitlab.com/reinodovo/botdovo/internal/clash"
	"gitlab.com/reinodovo/botdovo/internal/configuration_manager"
	"gitlab.com/reinodovo/botdovo/internal/cwl_planning/cwl_sheets"
	"gitlab.com/reinodovo/botdovo/internal/database"
	"gitlab.com/reinodovo/botdovo/internal/device"
	"gitlab.com/reinodovo/botdovo/internal/scheduler"
)

var tracerName = "cwl"

func SetupCWL(
	bot bot.Bot,
	config configuration_manager.ConfigurationManager,
	cwlSheets *cwl_sheets.CWLSheets,
	cron scheduler.Scheduler,
	clashAPI clash.ClashAPI,
	database database.Database,
	device device.Device,
	logger *log.Logger,
) error {
	if err := setupCWLAttacks(bot, cwlSheets, clashAPI); err != nil {
		return err
	}
	if err := setupCWLAttackLocks(bot, cwlSheets, clashAPI); err != nil {
		return err
	}
	if err := setupUpdateCWLPlanning(bot, cwlSheets, cron, clashAPI); err != nil {
		return err
	}
	if err := setupUpdateCWLRoundsOverview(bot, cwlSheets, cron, clashAPI); err != nil {
		return err
	}
	if err := setupCWLBonus(bot, config, clashAPI, cwlSheets, cron); err != nil {
		return err
	}
	if err := setupFixMislabeledAttacks(bot, cwlSheets, cron, clashAPI); err != nil {
		return err
	}
	if err := setupDonateCWLTroops(bot, cwlSheets, cron, clashAPI, database, device); err != nil {
		return err
	}
	if err := setupStartCWL(bot, cron, device); err != nil {
		return err
	}
	return nil
}
