package cwl

import (
	"context"
	"reflect"
	"strings"
	"testing"

	"github.com/stretchr/testify/mock"
	"gitlab.com/reinodovo/botdovo/internal/clash"
	"gitlab.com/reinodovo/botdovo/internal/cwl_planning"
	"gitlab.com/reinodovo/botdovo/internal/mocks"
)

func compareActualAttacks(a, b []cwl_planning.ActualAttack) bool {
	if len(a) != len(b) {
		return false
	}
	for idx, attackA := range a {
		attackB := b[idx]
		if attackA != attackB {
			return false
		}
	}
	return true
}

func compareMislabeledAttacks(a, b []mislabeledRosterAttack) bool {
	if len(a) != len(b) {
		return false
	}
	for idx, attackA := range a {
		attackB := b[idx]
		if attackA != attackB {
			return false
		}
	}
	return true
}

func compareRosters(a, b []cwl_planning.PlayerRoster) bool {
	compareRosterAttacks := func(a, b []cwl_planning.RosterAttack) bool {
		if len(a) != len(b) {
			return false
		}
		for idx, attackA := range a {
			attackB := b[idx]
			if attackA != attackB {
				return false
			}
		}
		return true
	}
	if len(a) != len(b) {
		return false
	}
	for idx, playerRosterA := range a {
		playerRosterB := b[idx]
		if !compareRosterAttacks(playerRosterA.Attacks, playerRosterB.Attacks) || playerRosterA.Player != playerRosterB.Player {
			return false
		}
	}
	return true
}

func compareRoundOverviews(a, b []cwl_planning.RoundOverview) bool {
	if len(a) != len(b) {
		return false
	}
	for idx, overviewA := range a {
		overviewB := b[idx]
		if overviewA != overviewB {
			return false
		}
	}
	return true
}

func TestGenerateNewAttacksMessage(t *testing.T) {
	// setup
	newAttacks := []cwl_planning.ActualAttack{
		{Player: "player1", Stars: 2, Position: 2, Finished: true},
		{Player: "player2", Stars: 0, Position: 4, Finished: true},
	}
	war := clash.War{
		Opponent: clash.WarClan{
			Members: []clash.WarPlayer{
				{MapPosition: 0},
				{MapPosition: 5},
				{MapPosition: 3, BestOpponentAttack: clash.WarAttack{DestructionPercentage: 80}},
				{MapPosition: 8, BestOpponentAttack: clash.WarAttack{DestructionPercentage: 42}},
				{MapPosition: 1},
			},
		},
	}
	round := 2
	expectedMessage := `<b>Novos Ataques No Dia 3:</b>
3. player1 - 2 estrelas (80%)
5. player2 - 0 estrelas (42%)`
	action := UpdateCWLPlanning{}
	// act
	message := action.generateNewAttacksMessage(newAttacks, war, round)
	// test
	if strings.TrimSpace(message) != strings.TrimSpace(expectedMessage) {
		t.Errorf("expected %v, got %v", expectedMessage, message)
	}
}

func TestNewWarAttacks(t *testing.T) {
	// setup
	actualAttacks := []cwl_planning.ActualAttack{
		{Player: "player1", Stars: 2, Position: 0, Finished: true},
		{Player: "player2", Position: 1, Finished: false},
		{Player: "player3", Position: 2, Finished: false},
		{Player: "player5", Stars: 2, Position: 4, Finished: true},
	}
	members := []clash.Player{
		{Name: "player1", Tag: "tag1"},
		{Name: "player2", Tag: "tag2"},
		{Name: "player3", Tag: "tag3"},
		{Name: "player4", Tag: "tag4"},
		{Name: "player5", Tag: "tag5"},
	}
	war := clash.War{
		Clan: clash.WarClan{
			Members: []clash.WarPlayer{
				{Name: "player1", Tag: "tag1"},
				{Name: "player3", Tag: "tag3"},
				{Name: "player5", Tag: "tag5"},
			},
		},
		Opponent: clash.WarClan{
			Members: []clash.WarPlayer{
				{MapPosition: 2, OpponentAttacks: 1, BestOpponentAttack: clash.WarAttack{AttackerTag: "tag2", Stars: 2}},
				{MapPosition: 0, OpponentAttacks: 1, BestOpponentAttack: clash.WarAttack{AttackerTag: "tag1", Stars: 2}},
				{MapPosition: 8, OpponentAttacks: 0},
				{MapPosition: 6, OpponentAttacks: 1, BestOpponentAttack: clash.WarAttack{AttackerTag: "tag5", Stars: 3}},
				{MapPosition: 4, OpponentAttacks: 1, BestOpponentAttack: clash.WarAttack{AttackerTag: "tag3", Stars: 0}},
			},
		},
	}
	warSize := 5
	expectedNewAttacks := []cwl_planning.ActualAttack{
		{Player: "player2", Stars: 2, Position: 1, Finished: true},
		{Player: "player3", Stars: 0, Position: 2, Finished: true},
		{Player: "player5", Stars: 3, Position: 3, Finished: true},
	}
	action := UpdateCWLPlanning{}
	// act
	newAttacks := action.newWarAttacks(actualAttacks, war, warSize, members)
	// test
	if !compareActualAttacks(newAttacks, expectedNewAttacks) {
		t.Errorf("expected %v, got %v", expectedNewAttacks, newAttacks)
	}
}

func TestHandleUpdateAttacks(t *testing.T) {
	// setup
	var callback func(ctx context.Context) error

	var messages []string
	expectedMessages := []string{`<b>Novos Ataques No Dia 1:</b>
1. player2 - 2 estrelas (0%)
3. player3 - 0 estrelas (0%)`,
		`player2 lockou o 2 e atacou outra base, muito escroto`,
		`<b>Novos Ataques No Dia 2:</b>
2. player1 - 2 estrelas (20%)`,
		`player1 atacou o 2, base que player2 tinha lockado, muuuito escroto`}

	mockBot := &mocks.Bot{}
	mockBot.On("SendMessage", mock.Anything, mock.MatchedBy(func(message string) bool {
		if len(strings.TrimSpace(message)) != 0 {
			messages = append(messages, strings.TrimSpace(message))
		}
		return true
	})).Return(0, nil)

	mockClashAPI := &mocks.ClashAPI{}
	wars := []clash.War{
		{
			Clan: clash.WarClan{Members: []clash.WarPlayer{{Name: "player1", Tag: "tag1"}, {Name: "player2", Tag: "tag2"}, {Name: "player3", Tag: "tag3"}}},
			Opponent: clash.WarClan{
				Members: []clash.WarPlayer{
					{MapPosition: 2, OpponentAttacks: 1, BestOpponentAttack: clash.WarAttack{AttackerTag: "tag2", Stars: 2}},
					{MapPosition: 4, OpponentAttacks: 0},
					{MapPosition: 8, OpponentAttacks: 1, BestOpponentAttack: clash.WarAttack{AttackerTag: "tag3", Stars: 0}},
				},
			},
		},
		{
			Clan: clash.WarClan{Members: []clash.WarPlayer{{Name: "player1", Tag: "tag1"}, {Name: "player2", Tag: "tag2"}, {Name: "player3", Tag: "tag3"}}},
			Opponent: clash.WarClan{
				Members: []clash.WarPlayer{
					{MapPosition: 4, OpponentAttacks: 1, BestOpponentAttack: clash.WarAttack{AttackerTag: "tag1", Stars: 2, DestructionPercentage: 20}},
					{MapPosition: 1, OpponentAttacks: 0},
					{MapPosition: 5, OpponentAttacks: 0},
				},
			},
		},
		{
			Clan: clash.WarClan{Members: []clash.WarPlayer{{Name: "player1", Tag: "tag1"}, {Name: "player2", Tag: "tag2"}, {Name: "player3", Tag: "tag3"}}},
		},
	}
	mockClashAPI.On("MainClanCWLWar", mock.Anything, mock.Anything).Return(func(ctx context.Context, round int) clash.War {
		return wars[round]
	}, nil)
	mockClashAPI.On("MainClanMembers", mock.Anything, mock.Anything).Return([]clash.Player{
		{Name: "player1", Tag: "tag1"}, {Name: "player2", Tag: "tag2"}, {Name: "player3", Tag: "tag3"},
	}, nil)

	mockScheduler := &mocks.Scheduler{}
	mockScheduler.On("Schedule", mock.Anything, mock.MatchedBy(func(savedCallback func(ctx context.Context) error) bool {
		callback = savedCallback
		return true
	})).Return(nil, nil)

	mockCWLPlanning := &mocks.CWLPlanning{}
	mockCWLPlanning.On("Rounds", mock.Anything).Return(len(wars))
	mockCWLPlanning.On("WarSize", mock.Anything).Return(3)

	expectedNewActualAttacks := [][]cwl_planning.ActualAttack{
		{
			cwl_planning.ActualAttack{Player: "player2", Stars: 2, Position: 0, Finished: true},
			cwl_planning.ActualAttack{Player: "player3", Stars: 0, Position: 2, Finished: true},
			cwl_planning.ActualAttack{Position: 1},
		},
		{cwl_planning.ActualAttack{Player: "player1", Stars: 2, Position: 1, Finished: true}},
		nil,
	}
	var newActualAttacks [][]cwl_planning.ActualAttack
	mockCWLPlanning.On("ActualAttacks", mock.Anything, mock.Anything).Return(func(ctx context.Context, round int) []cwl_planning.ActualAttack {
		return []cwl_planning.ActualAttack{{
			Player:   "player2",
			Position: 1,
			Finished: false,
		}}
	}, nil)
	mockCWLPlanning.On("UpdateActualAttacks", mock.Anything, mock.Anything, mock.MatchedBy(func(actualAttacks []cwl_planning.ActualAttack) bool {
		newActualAttacks = append(newActualAttacks, actualAttacks)
		return true
	})).Return(nil)
	mockCWLPlanning.On("UpdateOverwrittenAttacks", mock.Anything, mock.Anything, mock.Anything).Return(nil)

	setupUpdateCWLPlanning(mockBot, mockCWLPlanning, mockScheduler, mockClashAPI)

	// act
	callback(context.Background())

	// test
	if !reflect.DeepEqual(messages, expectedMessages) {
		t.Errorf("expected %v, got %v", expectedMessages, messages)
	}
	if !reflect.DeepEqual(newActualAttacks, expectedNewActualAttacks) {
		t.Errorf("expected %v, got %v", expectedNewActualAttacks, newActualAttacks)
	}
}
