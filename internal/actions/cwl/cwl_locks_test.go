package cwl

import (
	"context"
	"reflect"
	"strings"
	"testing"

	"github.com/stretchr/testify/mock"
	"gitlab.com/reinodovo/botdovo/internal/bot"
	"gitlab.com/reinodovo/botdovo/internal/clash"
	"gitlab.com/reinodovo/botdovo/internal/cwl_planning"
	"gitlab.com/reinodovo/botdovo/internal/mocks"
	"gitlab.com/reinodovo/botdovo/internal/utils"
)

func TestFindAttackStateByPosition(t *testing.T) {
	// setup
	actualAttacks := []cwl_planning.ActualAttack{
		{Player: "player1", Stars: 3, Position: 0, Finished: true},
		{Player: "player2", Stars: 0, Position: 1, Finished: false},
	}
	tests := []struct {
		position       int64
		expectedAttack cwl_planning.ActualAttack
		expectedState  int
	}{
		{position: 1, expectedAttack: actualAttacks[1], expectedState: reserved},
		{position: 0, expectedAttack: actualAttacks[0], expectedState: finished},
		{position: 2, expectedAttack: cwl_planning.ActualAttack{}, expectedState: free},
	}
	action := CWLAttackLocks{}
	for _, test := range tests {
		// act
		attack, state := action.findAttackStateByPosition(actualAttacks, test.position)
		// test
		if attack != test.expectedAttack {
			t.Fatalf("expected attack %v, got %v", test.expectedAttack, attack)
		}
		if state != test.expectedState {
			t.Fatalf("expected state %v, got %v", test.expectedState, state)
		}
	}
}

func TestFindAttackStateByPlayer(t *testing.T) {
	// setup
	actualAttacks := []cwl_planning.ActualAttack{
		{Player: "player1", Stars: 3, Position: 0, Finished: true},
		{Player: "player2", Stars: 0, Position: 1, Finished: false},
	}
	tests := []struct {
		player         string
		expectedAttack cwl_planning.ActualAttack
		expectedState  int
	}{
		{player: "player2", expectedAttack: actualAttacks[1], expectedState: reserved},
		{player: "player1", expectedAttack: actualAttacks[0], expectedState: finished},
		{player: "player3", expectedAttack: cwl_planning.ActualAttack{}, expectedState: free},
	}
	action := CWLAttackLocks{}
	for _, test := range tests {
		// act
		attack, state := action.findAttackStateByPlayer(actualAttacks, test.player)
		// test
		if attack != test.expectedAttack {
			t.Fatalf("expected attack %v, got %v", test.expectedAttack, attack)
		}
		if state != test.expectedState {
			t.Fatalf("expected state %v, got %v", test.expectedState, state)
		}
	}
}

func TestIsPlayerInRoundRoster(t *testing.T) {
	// setup
	roster := []cwl_planning.PlayerRoster{
		{
			Player: "player1",
			Attacks: []cwl_planning.RosterAttack{
				{AttackerCode: '1'},
				{},
				{AttackerCode: '1'},
			},
		},
		{
			Player: "player2",
			Attacks: []cwl_planning.RosterAttack{
				{AttackerCode: '2'},
			},
		},
	}
	tests := []struct {
		player         string
		round          int
		expectedResult bool
	}{
		{player: "player1", round: 2, expectedResult: true},
		{player: "player1", round: 1, expectedResult: false},
		{player: "player2", round: 0, expectedResult: true},
		{player: "player2", round: 2, expectedResult: false},
		{player: "player3", round: 0, expectedResult: false},
	}
	action := CWLAttackLocks{}
	for _, test := range tests {
		// act
		result := action.isPlayerInRoundRoster(roster, test.player, test.round)
		// test
		if result != test.expectedResult {
			t.Fatalf("expected %v, got %v", test.expectedResult, result)
		}
	}
}

func TestParseLockArgs(t *testing.T) {
	// setup
	tests := []struct {
		args             []string
		warSize          int
		expectedPlayer   string
		expectedPosition int64
		expectedError    error
	}{
		{
			args:             []string{"player1", "10"},
			warSize:          10,
			expectedPlayer:   "player1",
			expectedPosition: 9,
			expectedError:    nil,
		},
		{
			args:          []string{"10", "player1"},
			expectedError: invalidPositionError,
		},
		{
			args:          []string{"player1", "0"},
			expectedError: invalidPositionError,
		},
		{
			args:          []string{"player1", "10"},
			warSize:       9,
			expectedError: invalidPositionError,
		},
		{
			args:          []string{"player1", "10", "9"},
			expectedError: utils.InvalidArgumentsError,
		},
		{
			args:          []string{"player1"},
			expectedError: utils.InvalidArgumentsError,
		},
		{
			args:          []string{},
			expectedError: utils.InvalidArgumentsError,
		},
	}
	action := CWLAttackLocks{}
	for _, test := range tests {
		// act
		player, position, err := action.parseLockArgs(test.warSize, test.args...)
		// test
		if err != test.expectedError {
			t.Fatalf("expected error to be %v, got %v", test.expectedError, err)
		}
		if err != nil {
			continue
		}
		if player != test.expectedPlayer {
			t.Fatalf("expected player to be %v, got %v", test.expectedPlayer, player)
		}
		if position != test.expectedPosition {
			t.Fatalf("expected position to be %v, got %v", test.expectedPosition, position)
		}
	}
}

func TestParseUnlockArgs(t *testing.T) {
	// setup
	tests := []struct {
		args           []string
		expectedPlayer string
		expectedError  error
	}{
		{
			args:           []string{"player1"},
			expectedPlayer: "player1",
		},
		{
			args:          []string{"player1", "10"},
			expectedError: utils.InvalidArgumentsError,
		},
		{
			args:          []string{},
			expectedError: utils.InvalidArgumentsError,
		},
	}
	action := CWLAttackLocks{}
	for _, test := range tests {
		// act
		player, err := action.parseUnlockArgs(test.args...)
		// test
		if err != test.expectedError {
			t.Fatalf("expected error to be %v, got %v", test.expectedError, err)
		}
		if err != nil {
			continue
		}
		if player != test.expectedPlayer {
			t.Fatalf("expected player to be %v, got %v", test.expectedPlayer, player)
		}
	}
}

func TestCWLAttacksLock(t *testing.T) {
	// setup
	var rankCommand bot.Command

	mockBot := &mocks.Bot{}
	mockBot.On("SetCommand", mock.Anything, "unlock", mock.Anything, mock.Anything).Return(nil)
	mockBot.On("SetCommand", mock.Anything, "lock", mock.MatchedBy(func(command bot.Command) bool {
		rankCommand = command
		return true
	}), mock.Anything).Return(nil)

	round := 1
	mockClashAPI := &mocks.ClashAPI{}
	mockClashAPI.On("MainClanCurrentWar", mock.Anything).Return(clash.War{}, round, nil)

	tests := []struct {
		actualAttacks   []cwl_planning.ActualAttack
		args            []string
		expectedLock    cwl_planning.ActualAttack
		expectedMessage string
	}{
		{
			actualAttacks:   []cwl_planning.ActualAttack{},
			args:            []string{},
			expectedLock:    cwl_planning.ActualAttack{},
			expectedMessage: "não entendi, tu tá usando o comando direito?",
		},
		{
			actualAttacks:   []cwl_planning.ActualAttack{},
			args:            []string{"player"},
			expectedLock:    cwl_planning.ActualAttack{},
			expectedMessage: "não entendi, tu tá usando o comando direito?",
		},
		{
			actualAttacks:   []cwl_planning.ActualAttack{},
			args:            []string{"player3", "5"},
			expectedLock:    cwl_planning.ActualAttack{},
			expectedMessage: "player3 não tá marcado pra atacar no dia 2",
		},
		{
			actualAttacks:   []cwl_planning.ActualAttack{{Player: "player2", Position: 0}},
			args:            []string{"player2", "5"},
			expectedLock:    cwl_planning.ActualAttack{},
			expectedMessage: "essa reserva não pode ser feita",
		},
		{
			actualAttacks:   []cwl_planning.ActualAttack{{Player: "player1", Position: 4}},
			args:            []string{"player2", "5"},
			expectedLock:    cwl_planning.ActualAttack{},
			expectedMessage: "essa reserva não pode ser feita",
		},
		{
			actualAttacks:   []cwl_planning.ActualAttack{},
			args:            []string{"player1", "5"},
			expectedLock:    cwl_planning.ActualAttack{Player: "player1", Position: 4},
			expectedMessage: "<b>reservado</b> (5. player1)",
		},
	}

	for idx, test := range tests {
		mockCWLPlanning := &mocks.CWLPlanning{}
		mockCWLPlanning.On("WarSize", mock.Anything).Return(5)
		roster := []cwl_planning.PlayerRoster{
			{Player: "player1", Attacks: []cwl_planning.RosterAttack{{AttackerCode: 1}, {AttackerCode: 1}, {AttackerCode: 1}}},
			{Player: "player2", Attacks: []cwl_planning.RosterAttack{{AttackerCode: 0}, {AttackerCode: 1}, {AttackerCode: 0}}},
			{Player: "player3", Attacks: []cwl_planning.RosterAttack{{AttackerCode: 1}, {AttackerCode: 0}, {AttackerCode: 1}}},
		}
		mockCWLPlanning.On("GetRoster", mock.Anything).Return(roster, nil)
		mockCWLPlanning.On("ActualAttacks", mock.Anything, round).Return(test.actualAttacks, nil)

		setupCWLAttackLocks(mockBot, mockCWLPlanning, mockClashAPI)

		// test
		mockCWLPlanning.On("UpdateActualAttacks", mock.Anything, round, mock.MatchedBy(func(actualAttacks []cwl_planning.ActualAttack) bool {
			expectedActualAttacks := []cwl_planning.ActualAttack{test.expectedLock}
			if !reflect.DeepEqual(actualAttacks, expectedActualAttacks) {
				t.Errorf("expected %v, got %v on test %v", expectedActualAttacks, actualAttacks, idx)
			}
			return true
		})).Return(nil)
		mockBot.On("SendMessage", mock.Anything, mock.MatchedBy(func(message string) bool {
			if strings.TrimSpace(message) != strings.TrimSpace(test.expectedMessage) {
				t.Errorf("expected %v, got %v on test %v", test.expectedMessage, message, idx)
			}
			return true
		})).Return(0, nil)

		// act
		rankCommand(context.Background(), test.args...)
	}
}

func TestCWLAttacksUnlock(t *testing.T) {
	// setup
	var rankCommand bot.Command

	mockBot := &mocks.Bot{}
	mockBot.On("SetCommand", mock.Anything, "lock", mock.Anything, mock.Anything).Return(nil)
	mockBot.On("SetCommand", mock.Anything, "unlock", mock.MatchedBy(func(command bot.Command) bool {
		rankCommand = command
		return true
	}), mock.Anything).Return(nil)

	round := 1
	mockClashAPI := &mocks.ClashAPI{}
	mockClashAPI.On("MainClanCurrentWar", mock.Anything).Return(clash.War{}, round, nil)

	tests := []struct {
		actualAttacks   []cwl_planning.ActualAttack
		args            []string
		expectedLock    cwl_planning.ActualAttack
		expectedMessage string
	}{
		{
			actualAttacks:   []cwl_planning.ActualAttack{},
			args:            []string{},
			expectedLock:    cwl_planning.ActualAttack{},
			expectedMessage: "não entendi, tu tá usando o comando direito?",
		},
		{
			actualAttacks:   []cwl_planning.ActualAttack{{Player: "player1"}},
			args:            []string{"player1"},
			expectedLock:    cwl_planning.ActualAttack{Player: ""},
			expectedMessage: "<b>reserva removida</b>, player1",
		},
		{
			actualAttacks:   []cwl_planning.ActualAttack{},
			args:            []string{"player1"},
			expectedLock:    cwl_planning.ActualAttack{},
			expectedMessage: "player1 não tem reserva",
		},
		{
			actualAttacks:   []cwl_planning.ActualAttack{{Player: "player1", Finished: true}},
			args:            []string{"player1"},
			expectedLock:    cwl_planning.ActualAttack{},
			expectedMessage: "player1 já atacou",
		},
	}

	for idx, test := range tests {
		mockCWLPlanning := &mocks.CWLPlanning{}
		mockCWLPlanning.On("WarSize", mock.Anything).Return(5)
		mockCWLPlanning.On("ActualAttacks", mock.Anything, round).Return(test.actualAttacks, nil)

		setupCWLAttackLocks(mockBot, mockCWLPlanning, mockClashAPI)

		// test
		mockCWLPlanning.On("UpdateActualAttacks", mock.Anything, round, mock.MatchedBy(func(actualAttacks []cwl_planning.ActualAttack) bool {
			expectedActualAttacks := []cwl_planning.ActualAttack{test.expectedLock}
			if !reflect.DeepEqual(actualAttacks[0].Player, expectedActualAttacks[0].Player) {
				t.Errorf("expected `%v`, got `%v` on test %v", expectedActualAttacks[0].Player, actualAttacks[0].Player, idx)
			}
			return true
		})).Return(nil)
		mockBot.On("SendMessage", mock.Anything, mock.MatchedBy(func(message string) bool {
			if strings.TrimSpace(message) != strings.TrimSpace(test.expectedMessage) {
				t.Errorf("expected %v, got %v on test %v", test.expectedMessage, message, idx)
			}
			return true
		})).Return(0, nil)

		// act
		rankCommand(context.Background(), test.args...)
	}
}
