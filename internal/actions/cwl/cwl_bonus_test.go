package cwl

import (
	"context"
	"errors"
	"math/rand"
	"reflect"
	"strings"
	"testing"

	"github.com/stretchr/testify/mock"
	"gitlab.com/reinodovo/botdovo/internal/clash"
	"gitlab.com/reinodovo/botdovo/internal/configuration_manager"
	"gitlab.com/reinodovo/botdovo/internal/cwl_planning/cwl_sheets"
	"gitlab.com/reinodovo/botdovo/internal/mocks"
	"gitlab.com/reinodovo/botdovo/internal/utils"
)

func TestGenerateBonusMessage(t *testing.T) {
	// setup
	mockCWLSheetsManagement := &mocks.CWLSheetsManagement{}
	mockCWLSheetsManagement.On("OverviewSheetsURL").Return("http://example.com", nil)

	tests := []struct {
		bonuses         []configuration_manager.CWLSingleBonusConfig
		state           string
		expectedMessage string
	}{
		{
			state: pendingTotal,
			expectedMessage: `<b>HORA DO BÔNUS</b>

Finalmente acabou essa desgraça, parabêns aos envolvidos
Mindiga a quantidade total de bônus`,
		},
		{
			bonuses: []configuration_manager.CWLSingleBonusConfig{{Person: "person1", Account: "account1"}, {Person: "person2", Account: "account2"}},
			state:   pendingAccounts,
			expectedMessage: `<b>HORA DO BÔNUS</b>
person1 - account1
person2 - account2

Mindiga a pessoa e a conta que vai receber um dos bônus`,
		},
		{
			bonuses: []configuration_manager.CWLSingleBonusConfig{{Person: "person1", Account: "account1"}, {Person: "person2", Account: "account2"}},
			state:   pendingSend,
			expectedMessage: `<b>HORA DO BÔNUS</b>
person1 - account1
person2 - account2

Quando doar os bônus no clash responde "TUDONOSSO"`,
		},
		{
			bonuses: []configuration_manager.CWLSingleBonusConfig{{Person: "person1", Account: "account1"}, {Person: "person2", Account: "account2"}},
			state:   sent,
			expectedMessage: `<b>HORA DO BÔNUS</b>
person1 - account1
person2 - account2

Tudo nosso
Lembra de atualizar <a href="http://example.com">a planilha dos bônus</a>`,
		},
	}

	cwlBonus := CWLBonus{cwlSheets: mockCWLSheetsManagement}

	for idx, test := range tests {
		// act
		message := cwlBonus.generateBonusMessage(test.bonuses, test.state)

		// test
		if strings.TrimSpace(message) != strings.TrimSpace(test.expectedMessage) {
			t.Errorf("expected %v, got %v on test %v", test.expectedMessage, message, idx)
		}
	}
}

func TestHandleCWLFinished(t *testing.T) {
	// setup
	var callback func(ctx context.Context) error

	mockBot := &mocks.Bot{}
	expectedMessages := []string{`<b>HORA DO BÔNUS</b>

Finalmente acabou essa desgraça, parabêns aos envolvidos
Mindiga a quantidade total de bônus`}
	var messages []string
	mockBot.On("SendMessageAndPin", mock.Anything, mock.MatchedBy(func(message string) bool {
		messages = append(messages, strings.TrimSpace(message))
		return true
	})).Return(0, nil)
	mockBot.On("SetReplyCallback", mock.Anything, mock.Anything, mock.Anything).Return(nil).Twice()
	mockBot.On("EditMessage", mock.Anything, mock.Anything, mock.Anything).Return(nil).Once()

	mockClashAPI := &mocks.ClashAPI{}
	mockClashAPI.On("MainClanCWL", mock.Anything).Return(clash.CWL{Group: clash.LeagueGroup{Season: "season", State: clash.CWL_ENDED}}, nil)

	mockScheduler := &mocks.Scheduler{}
	mockScheduler.On("Schedule", mock.Anything, mock.MatchedBy(func(savedCallback func(ctx context.Context) error) bool {
		callback = savedCallback
		return true
	})).Return(nil, nil)

	mockConfigurationManager := &mocks.ConfigurationManager{}
	var bonus configuration_manager.CWLBonusConfig
	mockConfigurationManager.On("CWLBonus").Return(func() configuration_manager.CWLBonusConfig {
		return bonus
	}, nil)
	expectedBonus := configuration_manager.CWLBonusConfig{
		Season: "season",
		State:  pendingTotal,
	}
	mockConfigurationManager.On("SetCWLBonus", mock.MatchedBy(func(receivedBonus configuration_manager.CWLBonusConfig) bool {
		bonus = receivedBonus
		return true
	})).Return(nil)

	setupCWLBonus(mockBot, mockConfigurationManager, mockClashAPI, &cwl_sheets.CWLSheets{}, mockScheduler)

	// act
	callback(context.Background())

	// test
	if !reflect.DeepEqual(messages, expectedMessages) {
		t.Errorf("expected %v, got %v", expectedMessages, messages)
	}
	if !reflect.DeepEqual(bonus, expectedBonus) {
		t.Errorf("expected %v, got %v", expectedBonus, bonus)
	}
}

func TestParseTotalAmountiArgs(t *testing.T) {
	// setup
	tests := []struct {
		expectedTotalAmount int
		shouldReturnError   bool
		args                []string
	}{
		{
			expectedTotalAmount: 5,
			shouldReturnError:   false,
			args:                []string{"5"},
		},
		{
			shouldReturnError: true,
			args:              []string{"5", "5"},
		},
		{
			shouldReturnError: true,
			args:              []string{},
		},
		{
			shouldReturnError: true,
			args:              []string{"a"},
		},
	}

	cwlBonus := CWLBonus{}

	for idx, test := range tests {
		// act
		totalAmount, err := cwlBonus.parseTotalAmountArgs(test.args...)

		// test
		if test.shouldReturnError != (err != nil) {
			t.Errorf("should return error is %v, got %v on test %v", test.shouldReturnError, err, idx)
		}
		if totalAmount != test.expectedTotalAmount {
			t.Errorf("expected %v, got %v on test %v", test.expectedTotalAmount, totalAmount, idx)
		}
	}
}

func TestParseAccountBonusArgs(t *testing.T) {
	// setup
	tests := []struct {
		expectedPerson       string
		expectedAccount      string
		expectedShouldRemove bool
		expectedError        error
		args                 []string
	}{
		{
			expectedPerson:  "person",
			expectedAccount: "account",
			expectedError:   nil,
			args:            []string{"person", "account"},
		},
		{
			expectedError: utils.InvalidArgumentsError,
			args:          []string{"test"},
		},
		{
			expectedError: utils.InvalidArgumentsError,
			args:          []string{"remover", "person", "account"},
		},
		{
			expectedPerson:       "person",
			expectedAccount:      "account",
			expectedShouldRemove: true,
			expectedError:        nil,
			args:                 []string{"remove", "person", "account"},
		},
		{
			expectedError: utils.InvalidArgumentsError,
			args:          []string{"a", "b", "c", "d"},
		},
	}

	cwlBonus := CWLBonus{}

	for idx, test := range tests {
		// act
		person, account, shouldRemove, err := cwlBonus.parseAccountBonusArgs(test.args...)

		// test
		if !errors.Is(err, test.expectedError) {
			t.Errorf("expected %v, got %v on test %v", test.expectedError, err, idx)
			continue
		}
		if person != test.expectedPerson {
			t.Errorf("expected %v, got %v on test %v", test.expectedPerson, person, idx)
		}
		if account != test.expectedAccount {
			t.Errorf("expected %v, got %v on test %v", test.expectedAccount, account, idx)
		}
		if shouldRemove != test.expectedShouldRemove {
			t.Errorf("expected %v, got %v on test %v", test.expectedShouldRemove, shouldRemove, idx)
		}
	}
}

func TestParseSendBonus(t *testing.T) {
	// setup
	tests := []struct {
		expectedCommand string
		expectedError   error
		args            []string
	}{
		{
			expectedCommand: "command",
			expectedError:   nil,
			args:            []string{"command"},
		},
		{
			expectedError: utils.InvalidArgumentsError,
			args:          []string{},
		},
		{
			expectedError: utils.InvalidArgumentsError,
			args:          []string{"a", "b"},
		},
	}

	cwlBonus := CWLBonus{}

	for idx, test := range tests {
		// act
		command, err := cwlBonus.parseSendBonusArgs(test.args...)

		// test
		if !errors.Is(err, test.expectedError) {
			t.Errorf("expected %v, got %v on test %v", test.expectedError, err, idx)
			continue
		}
		if command != test.expectedCommand {
			t.Errorf("expected %v, got %v on test %v", test.expectedCommand, command, idx)
		}
	}
}

func TestHandleBonusReply(t *testing.T) {
	// setup
	mockCWLSheetsManagement := &mocks.CWLSheetsManagement{}
	mockCWLSheetsManagement.On("OverviewSheetsURL").Return("http://example.com", nil)

	messageId := 10

	tests := []struct {
		bonus           configuration_manager.CWLBonusConfig
		args            []string
		expectedBonus   configuration_manager.CWLBonusConfig
		expectedMessage string
	}{
		{
			bonus:           configuration_manager.CWLBonusConfig{State: pendingTotal},
			args:            []string{"test"},
			expectedBonus:   configuration_manager.CWLBonusConfig{},
			expectedMessage: `manda a quantidade total de bônus primeiro`,
		},
		{
			bonus:           configuration_manager.CWLBonusConfig{State: pendingTotal},
			args:            []string{"5"},
			expectedBonus:   configuration_manager.CWLBonusConfig{State: pendingAccounts, TotalAmount: 5},
			expectedMessage: `Tamo junto`,
		},
		{
			bonus: configuration_manager.CWLBonusConfig{State: pendingAccounts, TotalAmount: 1},
			args:  []string{"person", "account"},
			expectedBonus: configuration_manager.CWLBonusConfig{State: pendingSend, TotalAmount: 1, Bonuses: []configuration_manager.CWLSingleBonusConfig{
				{Person: "person", Account: "account"},
			}},
			expectedMessage: `Tamo junto`,
		},
		{
			bonus: configuration_manager.CWLBonusConfig{State: pendingAccounts, TotalAmount: 5},
			args:  []string{"person", "account"},
			expectedBonus: configuration_manager.CWLBonusConfig{State: pendingAccounts, TotalAmount: 5, Bonuses: []configuration_manager.CWLSingleBonusConfig{
				{Person: "person", Account: "account"},
			}},
			expectedMessage: `Tamo junto`,
		},
		{
			bonus: configuration_manager.CWLBonusConfig{State: pendingAccounts, TotalAmount: 5, Bonuses: []configuration_manager.CWLSingleBonusConfig{
				{Person: "person", Account: "account"},
			}},
			expectedBonus:   configuration_manager.CWLBonusConfig{State: pendingAccounts, TotalAmount: 5, Bonuses: []configuration_manager.CWLSingleBonusConfig{}},
			args:            []string{"remove", "person", "account"},
			expectedMessage: `Tamo junto`,
		},
		{
			bonus:           configuration_manager.CWLBonusConfig{State: pendingSend},
			expectedBonus:   configuration_manager.CWLBonusConfig{State: sent},
			args:            []string{doneCommand},
			expectedMessage: `Tamo junto`,
		},
	}

	for idx, test := range tests {
		rand.Seed(0)

		mockBot := &mocks.Bot{}
		var messages []string
		mockBot.On("Reply", mock.Anything, mock.MatchedBy(func(message string) bool {
			messages = append(messages, strings.TrimSpace(message))
			return true
		}), messageId).Return(0, nil)

		mockConfigurationManager := &mocks.ConfigurationManager{}
		var bonus configuration_manager.CWLBonusConfig
		mockConfigurationManager.On("SetCWLBonus", mock.MatchedBy(func(receivedBonus configuration_manager.CWLBonusConfig) bool {
			bonus = receivedBonus
			return true
		})).Return(nil)

		cwlBonus := CWLBonus{
			bot:       mockBot,
			config:    mockConfigurationManager,
			cwlSheets: mockCWLSheetsManagement,
		}

		// act
		cwlBonus.handleBonusReply(context.Background(), test.bonus, messageId, test.args...)

		// test
		if !reflect.DeepEqual(messages, []string{test.expectedMessage}) {
			t.Errorf("expected %v, got %v on test %v", test.expectedMessage, messages, idx)
		}
		if !reflect.DeepEqual(bonus, test.expectedBonus) {
			t.Errorf("expected %v, got %v on test %v", test.expectedBonus, bonus, idx)
		}
	}
}
