package cwl

import (
	"context"
	"reflect"
	"strings"
	"testing"

	"github.com/stretchr/testify/mock"
	"gitlab.com/reinodovo/botdovo/internal/clash"
	"gitlab.com/reinodovo/botdovo/internal/cwl_planning"
	"gitlab.com/reinodovo/botdovo/internal/mocks"
)

func TestGenerateMislabeledAttacksMessage(t *testing.T) {
	// setup
	tests := []struct {
		mislabeledAttacks []mislabeledRosterAttack
		expectedMessage   string
	}{
		{
			mislabeledAttacks: []mislabeledRosterAttack{
				{name: "player1", round: 2, missed: true},
				{name: "player1", round: 3, missed: false},
				{name: "player2", round: 5, missed: false},
				{name: "player3", round: 2, missed: true},
			},
			expectedMessage: `<b>Lista de filhos da puta que não atacaram</b>
player1 não atacou no dia 3
player3 não atacou no dia 3

<b>Lista de filhos da puta que na verdade atacaram</b>
player1 atacou no dia 4
player2 atacou no dia 6`,
		},
		{
			mislabeledAttacks: []mislabeledRosterAttack{
				{name: "player1", round: 3, missed: false},
				{name: "player2", round: 5, missed: false},
			},
			expectedMessage: `<b>Lista de filhos da puta que na verdade atacaram</b>
player1 atacou no dia 4
player2 atacou no dia 6`,
		},
		{
			mislabeledAttacks: []mislabeledRosterAttack{
				{name: "player1", round: 6, missed: true},
			},
			expectedMessage: `<b>Lista de filhos da puta que não atacaram</b>
player1 não atacou no dia 7`,
		},
		{
			mislabeledAttacks: []mislabeledRosterAttack{},
			expectedMessage:   "",
		},
	}
	action := FixMislabeledAttacks{}
	for idx, test := range tests {
		// act
		message := action.generateMislabeledAttacksMessage(test.mislabeledAttacks)
		// test
		if strings.TrimSpace(message) != strings.TrimSpace(test.expectedMessage) {
			t.Errorf("expected %v on test %v, got %v", test.expectedMessage, idx, message)
		}
	}
}

func TestMislabeledAttacks(t *testing.T) {
	// setup
	roster := []cwl_planning.PlayerRoster{
		{
			Player: "player1",
			Attacks: []cwl_planning.RosterAttack{
				{AttackerCode: '1', MissedAttack: false},
				{},
				{AttackerCode: '1', MissedAttack: false},
			},
		},
		{
			Player: "player2",
			Attacks: []cwl_planning.RosterAttack{
				{},
				{AttackerCode: '2', MissedAttack: false},
				{AttackerCode: '2', MissedAttack: true},
			},
		},
	}
	tests := []struct {
		war                       clash.War
		round                     int
		expectedMislabeledAttacks []mislabeledRosterAttack
	}{
		{
			war: clash.War{Clan: clash.WarClan{Members: []clash.WarPlayer{
				{Name: "player1", Attacks: make([]clash.WarAttack, 1)},
				{Name: "player2", Attacks: make([]clash.WarAttack, 0)},
			}}},
			round:                     0,
			expectedMislabeledAttacks: []mislabeledRosterAttack{},
		},
		{
			war: clash.War{Clan: clash.WarClan{Members: []clash.WarPlayer{
				{Name: "player1", Attacks: make([]clash.WarAttack, 1)},
				{Name: "player2", Attacks: make([]clash.WarAttack, 0)},
			}}},
			round: 1,
			expectedMislabeledAttacks: []mislabeledRosterAttack{
				{name: "player2", round: 1, missed: true},
			},
		},
		{
			war: clash.War{Clan: clash.WarClan{Members: []clash.WarPlayer{
				{Name: "player1", Attacks: make([]clash.WarAttack, 0)},
				{Name: "player2", Attacks: make([]clash.WarAttack, 1)},
			}}},
			round: 2,
			expectedMislabeledAttacks: []mislabeledRosterAttack{
				{name: "player1", round: 2, missed: true},
				{name: "player2", round: 2, missed: false},
			},
		},
	}
	action := FixMislabeledAttacks{}
	for idx, test := range tests {
		// act
		mislabeledAttacks := action.mislabeledAttacks(roster, test.war, test.round)
		// test
		if !compareMislabeledAttacks(mislabeledAttacks, test.expectedMislabeledAttacks) {
			t.Errorf("expected %v on test %v, got %v", test.expectedMislabeledAttacks, idx, mislabeledAttacks)
		}
	}
}

func TestFixRosterMislabeledAttacks(t *testing.T) {
	// setup
	roster := []cwl_planning.PlayerRoster{
		{
			Player: "player1",
			Attacks: []cwl_planning.RosterAttack{
				{AttackerCode: '1', MissedAttack: false},
				{},
				{AttackerCode: '1', MissedAttack: false},
			},
		},
		{
			Player: "player2",
			Attacks: []cwl_planning.RosterAttack{
				{},
				{AttackerCode: '2', MissedAttack: false},
				{AttackerCode: '2', MissedAttack: true},
			},
		},
	}
	mislabeledAttacks := []mislabeledRosterAttack{
		{name: "player1", round: 0, missed: true},
		{name: "player1", round: 2, missed: true},
		{name: "player2", round: 2, missed: false},
	}
	expectedFixedRoster := []cwl_planning.PlayerRoster{
		{
			Player: "player1",
			Attacks: []cwl_planning.RosterAttack{
				{AttackerCode: '1', MissedAttack: true},
				{},
				{AttackerCode: '1', MissedAttack: true},
			},
		},
		{
			Player: "player2",
			Attacks: []cwl_planning.RosterAttack{
				{},
				{AttackerCode: '2', MissedAttack: false},
				{AttackerCode: '2', MissedAttack: false},
			},
		},
	}

	action := FixMislabeledAttacks{}
	// act
	fixedRoster := action.fixRosterMislabeledAttacks(roster, mislabeledAttacks)
	// test
	if !compareRosters(fixedRoster, expectedFixedRoster) {
		t.Errorf("expected %v, got %v", expectedFixedRoster, fixedRoster)
	}
}

func TestHandleFixMislabeledAttacks(t *testing.T) {
	// setup
	var callback func(ctx context.Context) error

	var messages []string
	expectedMessages := []string{`<b>Lista de filhos da puta que não atacaram</b>
player1 não atacou no dia 1

<b>Lista de filhos da puta que na verdade atacaram</b>
player1 atacou no dia 2`}

	mockBot := &mocks.Bot{}
	mockBot.On("SendMessage", mock.Anything, mock.MatchedBy(func(message string) bool {
		messages = append(messages, strings.TrimSpace(message))
		return true
	})).Return(0, nil)

	mockClashAPI := &mocks.ClashAPI{}
	wars := []clash.War{
		{State: clash.WAR_ENDED, Clan: clash.WarClan{Members: []clash.WarPlayer{
			{Name: "player1", Attacks: make([]clash.WarAttack, 0)},
			{Name: "player2", Attacks: make([]clash.WarAttack, 0)},
		}}},
		{State: clash.WAR_ENDED, Clan: clash.WarClan{Members: []clash.WarPlayer{
			{Name: "player1", Attacks: make([]clash.WarAttack, 1)},
			{Name: "player2", Attacks: make([]clash.WarAttack, 1)},
		}}},
	}
	mockClashAPI.On("MainClanCWLWar", mock.Anything, mock.Anything).Return(func(ctx context.Context, round int) clash.War {
		return wars[round]
	}, nil)

	mockScheduler := &mocks.Scheduler{}
	mockScheduler.On("Schedule", mock.Anything, mock.MatchedBy(func(savedCallback func(ctx context.Context) error) bool {
		callback = savedCallback
		return true
	})).Return(nil, nil)

	mockCWLPlanning := &mocks.CWLPlanning{}
	mockCWLPlanning.On("Rounds", mock.Anything).Return(len(wars))
	roster := []cwl_planning.PlayerRoster{
		{Player: "player1", Attacks: []cwl_planning.RosterAttack{{AttackerCode: 1}, {AttackerCode: 1, MissedAttack: true}}},
		{Player: "player2", Attacks: []cwl_planning.RosterAttack{{AttackerCode: 1, MissedAttack: true}, {AttackerCode: 1}}},
	}

	expectedRosterUpdate := []cwl_planning.PlayerRoster{
		{Player: "player1", Attacks: []cwl_planning.RosterAttack{{AttackerCode: 1, MissedAttack: true}, {AttackerCode: 1}}},
		{Player: "player2", Attacks: []cwl_planning.RosterAttack{{AttackerCode: 1, MissedAttack: true}, {AttackerCode: 1}}},
	}
	var rosterUpdate []cwl_planning.PlayerRoster
	mockCWLPlanning.On("GetRoster", mock.Anything).Return(roster, nil)
	mockCWLPlanning.On("UpdateRoster", mock.Anything, mock.MatchedBy(func(roster []cwl_planning.PlayerRoster) bool {
		rosterUpdate = roster
		return true
	})).Return(nil).Once()

	setupFixMislabeledAttacks(mockBot, mockCWLPlanning, mockScheduler, mockClashAPI)

	// act
	callback(context.Background())

	// test
	if !reflect.DeepEqual(messages, expectedMessages) {
		t.Errorf("expected %v, got %v", expectedMessages, messages)
	}
	if !reflect.DeepEqual(rosterUpdate, expectedRosterUpdate) {
		t.Errorf("expected %v, got %v", expectedRosterUpdate, rosterUpdate)
	}
}
