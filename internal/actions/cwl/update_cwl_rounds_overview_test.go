package cwl

import (
	"context"
	"math/rand"
	"reflect"
	"strings"
	"testing"

	"github.com/stretchr/testify/mock"
	"gitlab.com/reinodovo/botdovo/internal/clash"
	"gitlab.com/reinodovo/botdovo/internal/cwl_planning"
	"gitlab.com/reinodovo/botdovo/internal/mocks"
)

func TestGenerateRoundsOverviewMessage(t *testing.T) {
	// setup
	rand.Seed(0)

	finishedWars := []cwl_planning.RoundOverview{
		{Enemy: "enemy1", Stars: 20, EnemyStars: 10, Won: true},
		{Enemy: "enemy2", Stars: 20, EnemyStars: 20, Won: false},
		{Enemy: "enemy3", Stars: 20, EnemyStars: 30, Won: false},
		{Enemy: "enemy4", Stars: 20, EnemyStars: 20, Won: true},
	}
	expectedMessage := `<b>Comemos cu</b> [20 x 10]
<b>Comeram o nosso cu</b> [20 x 20]
<b>Nada nosso</b> [20 x 30]
<b>Tudo nosso</b> [20 x 20]`
	action := UpdateCWLRoundsOverview{}
	// act
	message := action.generateRoundsOverviewMessage(finishedWars)
	// test
	if strings.TrimSpace(message) != strings.TrimSpace(expectedMessage) {
		t.Errorf("expected %v, got %v", expectedMessage, message)
	}
}

func TestNewFinishedWarsOverviews(t *testing.T) {
	// setup
	previousOverviews := []cwl_planning.RoundOverview{
		{Stars: 40, Enemy: "enemy1", EnemyStars: 45, Won: false},
		{Stars: 45, Enemy: "enemy2", EnemyStars: 45, Won: true},
	}
	currentOverviews := []cwl_planning.RoundOverview{
		{Stars: 45, Enemy: "enemy1", EnemyStars: 40, Won: true},
		{Stars: 45, Enemy: "enemy2", EnemyStars: 45, Won: true},
		{Stars: 45, Enemy: "enemy3", EnemyStars: 45, Won: true},
	}
	expectedNewOverviews := []cwl_planning.RoundOverview{
		{Stars: 45, Enemy: "enemy1", EnemyStars: 40, Won: true},
		{Stars: 45, Enemy: "enemy3", EnemyStars: 45, Won: true},
	}
	action := UpdateCWLRoundsOverview{}
	// act
	newOverviews := action.newFinishedWarsOverviews(previousOverviews, currentOverviews)
	// test
	if !compareRoundOverviews(newOverviews, expectedNewOverviews) {
		t.Errorf("expected %v, got %v", expectedNewOverviews, newOverviews)
	}
}

func TestHandleUpdateRoundsOverview(t *testing.T) {
	// setup
	rand.Seed(0)

	var callback func(ctx context.Context) error

	expectedMessage := `<b>Mamamos</b> [45 x 45]
<b>Nada deles</b> [45 x 40]`
	var message string

	mockBot := &mocks.Bot{}
	mockBot.On("SetCommand", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
	mockBot.On("SendMessage", mock.Anything, mock.MatchedBy(func(receivedMessage string) bool {
		message = receivedMessage
		return true
	}), mock.Anything).Return(0, nil)

	mockCWLPlanning := &mocks.CWLPlanning{}
	mockCWLPlanning.On("Rounds", mock.Anything).Return(4)
	mockCWLPlanning.On("GetRoundsOverview", mock.Anything).Return(
		[]cwl_planning.RoundOverview{
			{Stars: 40, Enemy: "enemy1", EnemyStars: 45, Won: false},
			{Stars: 45, Enemy: "enemy2", EnemyStars: 45, Won: true},
		},
		nil,
	)

	expectedRoundsOverview := []cwl_planning.RoundOverview{
		{Stars: 40, Enemy: "enemy1", EnemyStars: 45, Won: false},
		{Stars: 45, Enemy: "enemy2", EnemyStars: 45, Won: false},
		{Stars: 45, Enemy: "enemy3", EnemyStars: 40, Won: true},
	}
	var roundsOverview []cwl_planning.RoundOverview
	mockCWLPlanning.On("UpdateRoundsOverview", mock.Anything, mock.MatchedBy(func(receivedRoundsOverview []cwl_planning.RoundOverview) bool {
		roundsOverview = receivedRoundsOverview
		return true
	})).Return(nil).Once()

	mockScheduler := &mocks.Scheduler{}
	mockScheduler.On("Schedule", mock.Anything, mock.MatchedBy(func(savedCallback func(ctx context.Context) error) bool {
		callback = savedCallback
		return true
	})).Return(nil, nil)

	mockClashAPI := &mocks.ClashAPI{}
	wars := []clash.War{
		{State: clash.WAR_ENDED, Clan: clash.WarClan{Stars: 40}, Opponent: clash.WarClan{Name: "enemy1", Stars: 45}},
		{State: clash.WAR_ENDED, Clan: clash.WarClan{Stars: 45, DestructionPercentage: 10}, Opponent: clash.WarClan{Name: "enemy2", Stars: 45, DestructionPercentage: 11}},
		{State: clash.WAR_ENDED, Clan: clash.WarClan{Stars: 45}, Opponent: clash.WarClan{Name: "enemy3", Stars: 40}},
		{},
	}
	mockClashAPI.On("MainClanCWLWar", mock.Anything, mock.Anything).Return(func(ctx context.Context, round int) clash.War {
		return wars[round]
	}, nil)

	setupUpdateCWLRoundsOverview(mockBot, mockCWLPlanning, mockScheduler, mockClashAPI)

	// act
	callback(context.Background())

	// test
	if strings.TrimSpace(message) != strings.TrimSpace(expectedMessage) {
		t.Errorf("expected %v, got %v", expectedMessage, message)
	}
	if !reflect.DeepEqual(roundsOverview, expectedRoundsOverview) {
		t.Errorf("expected %v, got %v", expectedRoundsOverview, roundsOverview)
	}
}
