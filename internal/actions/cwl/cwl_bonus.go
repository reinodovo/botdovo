package cwl

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/reinodovo/botdovo/internal/bot"
	"gitlab.com/reinodovo/botdovo/internal/clash"
	"gitlab.com/reinodovo/botdovo/internal/configuration_manager"
	"gitlab.com/reinodovo/botdovo/internal/scheduler"
	"gitlab.com/reinodovo/botdovo/internal/utils"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
)

type CWLSheetsManagement interface {
	OverviewSheetsURL() (string, error)
}

type CWLBonus struct {
	bot       bot.Bot
	clashAPI  clash.ClashAPI
	config    configuration_manager.ConfigurationManager
	cwlSheets CWLSheetsManagement
}

func setupCWLBonus(bot bot.Bot, config configuration_manager.ConfigurationManager, clashAPI clash.ClashAPI, cwlSheets CWLSheetsManagement, cron scheduler.Scheduler) error {
	cwlBonus := &CWLBonus{
		bot:       bot,
		clashAPI:  clashAPI,
		config:    config,
		cwlSheets: cwlSheets,
	}
	if _, err := cron.Schedule(scheduler.Hour, cwlBonus.handleCWLFinished); err != nil {
		return err
	}
	return nil
}

const (
	pendingTotal    = "PENDING_TOTAL"
	pendingAccounts = "PENDING_ACCOUNTS"
	pendingSend     = "PENDING_SEND"
	sent            = "SENT"
)

const messageHeader string = "<b>HORA DO BÔNUS</b>\n"
const doneCommand string = "TUDONOSSO"
const removeCommand string = "remove"

func (action *CWLBonus) generateBonusMessage(bonuses []configuration_manager.CWLSingleBonusConfig, state string) string {
	var buffer bytes.Buffer
	buffer.WriteString(messageHeader)
	for _, bonus := range bonuses {
		buffer.WriteString(fmt.Sprintf("%v - %v\n", bonus.Person, bonus.Account))
	}
	buffer.WriteString("\n")
	switch state {
	case pendingTotal:
		buffer.WriteString("Finalmente acabou essa desgraça, parabêns aos envolvidos\n")
		buffer.WriteString("Mindiga a quantidade total de bônus\n")
	case pendingAccounts:
		buffer.WriteString("Mindiga a pessoa e a conta que vai receber um dos bônus")
	case pendingSend:
		buffer.WriteString(fmt.Sprintf("Quando doar os bônus no clash responde \"%v\"", doneCommand))
	case sent:
		url, _ := action.cwlSheets.OverviewSheetsURL()
		buffer.WriteString("Tudo nosso\n")
		buffer.WriteString(fmt.Sprintf("Lembra de atualizar <a href=\"%v\">a planilha dos bônus</a>\n", url))
	}
	return buffer.String()
}

func (action *CWLBonus) sendBonusMessage(ctx context.Context, firstMessage bool) error {
	ctx, span := otel.Tracer(tracerName).Start(ctx, "send_bonus_message")
	defer span.End()

	bonus, err := action.config.CWLBonus()
	utils.RecordErrorIfNotNil(span, err)
	if err != nil {
		return err
	}
	message := action.generateBonusMessage(bonus.Bonuses, bonus.State)
	if firstMessage {
		messageId, err := action.bot.SendMessageAndPin(ctx, message)
		utils.RecordErrorIfNotNil(span, err)
		if err != nil && !errors.Is(err, bot.PinMessageError) {
			return err
		}
		bonus.MessageId = messageId
		span.SetAttributes(attribute.Int("message_id", bonus.MessageId))
		err = action.config.SetCWLBonus(bonus)
		utils.RecordErrorIfNotNil(span, err)
		if err != nil {
			return err
		}
		return action.bot.SetReplyCallback(ctx, bonus.MessageId, action.bonusReplyCallback)
	} else {
		return action.bot.EditMessage(ctx, message, bonus.MessageId)
	}
}

func (action *CWLBonus) handleCWLFinished(ctx context.Context) error {
	ctx, span := otel.Tracer(tracerName).Start(ctx, "handle_cwl_finished")
	defer span.End()

	bonus, err := action.config.CWLBonus()
	if err != nil {
		return err
	}
	span.SetAttributes(attribute.Int("message_id", bonus.MessageId))
	action.bot.SetReplyCallback(ctx, bonus.MessageId, action.bonusReplyCallback)
	action.sendBonusMessage(ctx, false)
	cwl, err := action.clashAPI.MainClanCWL(ctx)
	utils.RecordErrorIfNotNil(span, err)
	if err != nil {
		return err
	}
	if cwl.Group.State != clash.CWL_ENDED {
		return nil
	}
	if bonus.Season == cwl.Group.Season {
		return nil
	}
	span.SetAttributes(attribute.String("current_season_config", bonus.Season))
	span.SetAttributes(attribute.String("season", cwl.Group.Season))
	bonus = configuration_manager.CWLBonusConfig{State: pendingTotal, Season: cwl.Group.Season}
	err = action.config.SetCWLBonus(bonus)
	utils.RecordErrorIfNotNil(span, err)
	if err != nil {
		return err
	}
	err = action.sendBonusMessage(ctx, true)
	if err != nil {
		err := fmt.Errorf("error generating initial bonus message: %v", err)
		utils.RecordErrorIfNotNil(span, err)
		return err
	}
	return err
}

func (action *CWLBonus) parseTotalAmountArgs(args ...string) (int, error) {
	if len(args) != 1 {
		return 0, utils.InvalidArgumentsError
	}
	totalAmount, err := strconv.ParseInt(args[0], 10, 32)
	return int(totalAmount), err
}

func (action *CWLBonus) parseAccountBonusArgs(args ...string) (string, string, bool, error) {
	if len(args) == 2 {
		return args[0], args[1], false, nil
	} else if len(args) == 3 {
		if args[0] != removeCommand {
			return "", "", false, utils.InvalidArgumentsError
		}
		return args[1], args[2], true, nil
	}
	return "", "", false, utils.InvalidArgumentsError
}

func (action *CWLBonus) parseSendBonusArgs(args ...string) (string, error) {
	if len(args) == 1 {
		return args[0], nil
	}
	return "", utils.InvalidArgumentsError
}

func (action *CWLBonus) bonusReplyCallback(ctx context.Context, text string, messageId int) error {
	args := strings.Fields(text)
	bonus, err := action.config.CWLBonus()
	if err != nil {
		_, err := action.bot.Reply(ctx, "deu ruim, tenta de novo", messageId)
		return err
	}
	err = action.handleBonusReply(ctx, bonus, messageId, args...)
	if err != nil {
		return err
	}
	return action.sendBonusMessage(ctx, false)
}

func (action *CWLBonus) handleBonusReply(ctx context.Context, bonus configuration_manager.CWLBonusConfig, messageId int, args ...string) error {
	if bonus.State == pendingTotal {
		totalAmount, err := action.parseTotalAmountArgs(args...)
		if err != nil {
			_, err := action.bot.Reply(ctx, "manda a quantidade total de bônus primeiro", messageId)
			return err
		}
		bonus.TotalAmount = int(totalAmount)
		bonus.State = pendingAccounts
	} else if bonus.State == pendingAccounts || bonus.State == pendingSend {
		person, account, shouldRemove, err := action.parseAccountBonusArgs(args...)
		if err != nil {
			if bonus.State == pendingSend {
				command, err := action.parseSendBonusArgs(args...)
				if err != nil || command != doneCommand {
					_, err := action.bot.Reply(ctx, "não enche meu saco", messageId)
					return err
				}
				bonus.State = sent
			} else {
				_, err := action.bot.Reply(ctx, fmt.Sprintf("manda o nome da pessoa e o nome da conta (coloca um '%v' no início se quiser remover)", removeCommand), messageId)
				return err
			}
		}
		if shouldRemove {
			search := configuration_manager.CWLSingleBonusConfig{
				Person:  person,
				Account: account,
			}
			removed := false
			for idx, singleBonus := range bonus.Bonuses {
				if singleBonus == search {
					bonus.Bonuses = append(bonus.Bonuses[:idx], bonus.Bonuses[idx+1:]...)
					bonus.State = pendingAccounts
					removed = true
					break
				}
			}
			if !removed {
				_, err := action.bot.Reply(ctx, "cê é burro?", messageId)
				return err
			}
		} else if bonus.State == pendingAccounts {
			bonus.Bonuses = append(bonus.Bonuses, configuration_manager.CWLSingleBonusConfig{
				Person:  person,
				Account: account,
			})
			if len(bonus.Bonuses) == bonus.TotalAmount {
				bonus.State = pendingSend
			}
		}
	}
	_, err := action.bot.Reply(ctx, utils.RandomString(utils.PositiveReplies), messageId)
	if err != nil {
		return err
	}
	return action.config.SetCWLBonus(bonus)
}
