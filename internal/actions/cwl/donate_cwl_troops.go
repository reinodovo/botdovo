package cwl

import (
	"context"
	"fmt"
	"sort"
	"time"

	"gitlab.com/reinodovo/botdovo/internal/bot"
	"gitlab.com/reinodovo/botdovo/internal/clash"
	"gitlab.com/reinodovo/botdovo/internal/cwl_planning"
	"gitlab.com/reinodovo/botdovo/internal/database"
	"gitlab.com/reinodovo/botdovo/internal/device"
	"gitlab.com/reinodovo/botdovo/internal/scheduler"
	"gitlab.com/reinodovo/botdovo/internal/utils"
	"go.opentelemetry.io/otel"
)

const donationInfoCollectionName = "donation_info"
const donationInfoKeyName = "donation_info"

type DonateCWLTroops struct {
	bot         bot.Bot
	cwlPlanning cwl_planning.CWLPlanning
	clashAPI    clash.ClashAPI
	database    database.Database
	device      device.Device
}

func setupDonateCWLTroops(bot bot.Bot, cwlPlanning cwl_planning.CWLPlanning, cron scheduler.Scheduler, clashAPI clash.ClashAPI, database database.Database, device device.Device) error {
	donateCWLTroops := &DonateCWLTroops{
		bot:         bot,
		cwlPlanning: cwlPlanning,
		clashAPI:    clashAPI,
		database:    database,
		device:      device,
	}
	if _, err := cron.Schedule(scheduler.Minute, donateCWLTroops.handleDonations); err != nil {
		return err
	}
	return nil
}

type DonationInfo struct {
	Donations          cwl_planning.Donations
	Index              int
	HasTrainingStarted bool
	NextActionDate     time.Time
}

func donationsFinished(donationInfo DonationInfo) bool {
	numberOfTroops := 0
	for _, troopDonations := range donationInfo.Donations {
		if len(troopDonations) > numberOfTroops {
			numberOfTroops = len(troopDonations)
		}
	}
	return donationInfo.Index >= numberOfTroops
}

func troopQuantity(donationInfo DonationInfo, preparationWar clash.War) int {
	quantity := 0
	for _, member := range preparationWar.Clan.Members {
		if troopDonations, ok := donationInfo.Donations[member.Name]; ok {
			if len(troopDonations) <= donationInfo.Index {
				continue
			}
			quantity += troopDonations[donationInfo.Index]
		}
	}
	return quantity
}

func getDonations(donationInfo DonationInfo, preparationWar clash.War) []int {
	donations := make([]int, preparationWar.TeamSize)
	sort.Slice(preparationWar.Clan.Members, func(i, j int) bool {
		return preparationWar.Clan.Members[i].MapPosition < preparationWar.Clan.Members[j].MapPosition
	})
	for idx, member := range preparationWar.Clan.Members {
		if troopDonations, ok := donationInfo.Donations[member.Name]; ok {
			donations[idx] = troopDonations[donationInfo.Index]
		}
	}
	return donations
}

func (action *DonateCWLTroops) performDonationAction(ctx context.Context, donationInfo DonationInfo, preparationWar clash.War, preparationDay int) (DonationInfo, bool, error) {
	var err error
	finished := false
	if donationsFinished(donationInfo) {
		finished = true
		nextPreparationStart := utils.ParseTime(preparationWar.StartTime).Add(30 * time.Minute)
		donationInfo.Index = 0
		donationInfo.Donations, err = action.cwlPlanning.Donations(ctx)
		donationInfo.HasTrainingStarted = false
		donationInfo.NextActionDate = nextPreparationStart
	} else if donationInfo.HasTrainingStarted {
		donations := getDonations(donationInfo, preparationWar)
		err = action.device.DonateTroops(ctx, preparationDay, donations)
		if err != nil {
			return donationInfo, finished, err
		}
		donationInfo.Index++
		donationInfo.HasTrainingStarted = false
		donationInfo.NextActionDate = time.Now().Add(5 * time.Minute)
	} else if !donationInfo.HasTrainingStarted {
		for troopQuantity(donationInfo, preparationWar) == 0 && !donationsFinished(donationInfo) {
			donationInfo.Index++
		}
		if donationsFinished(donationInfo) {
			return action.performDonationAction(ctx, donationInfo, preparationWar, preparationDay)
		}
		quantity := troopQuantity(donationInfo, preparationWar)
		waitTimeInSeconds, err := action.device.TrainTroops(ctx, donationInfo.Index, quantity)
		if err != nil {
			return donationInfo, finished, err
		}
		donationInfo.HasTrainingStarted = true
		donationInfo.NextActionDate = time.Now().Add(time.Duration(waitTimeInSeconds) * time.Second)
	}
	return donationInfo, finished, nil
}

func (action *DonateCWLTroops) handleDonations(ctx context.Context) error {
	ctx, span := otel.Tracer(tracerName).Start(ctx, "handle_donations")
	defer span.End()

	if !action.clashAPI.CWLIsHappening(ctx) {
		return nil
	}

	var donationInfo DonationInfo
	err := action.database.Get(ctx, donationInfoCollectionName, donationInfoKeyName, &donationInfo)
	if err != nil {
		utils.RecordErrorIfNotNil(span, err)
		return err
	}
	if donationInfo.Donations == nil || len(donationInfo.Donations) == 0 {
		donationInfo.Donations, err = action.cwlPlanning.Donations(ctx)
		if err != nil {
			utils.RecordErrorIfNotNil(span, err)
			return err
		}
	}

	currentTime := time.Now()
	if currentTime.Before(donationInfo.NextActionDate) {
		return nil
	}

	preparationWar, preparationDay, err := action.clashAPI.MainClanCurrentPreparation(ctx)
	if err != nil {
		return nil
	}

	donationInfo, finished, err := action.performDonationAction(ctx, donationInfo, preparationWar, preparationDay)
	if err != nil {
		utils.RecordErrorIfNotNil(span, err)
		return err
	}

	err = action.database.SaveObject(ctx, donationInfoCollectionName, donationInfoKeyName, donationInfo)
	if err != nil {
		utils.RecordErrorIfNotNil(span, err)
		return err
	}

	if !finished {
		return nil
	}

	_, err = action.bot.SendMessage(ctx, fmt.Sprintf("Doei as tropas do dia %v", preparationDay+1))
	if err != nil {
		utils.RecordErrorIfNotNil(span, err)
		return err
	}

	return nil
}
