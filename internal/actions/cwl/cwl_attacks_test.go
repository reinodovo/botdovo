package cwl

import (
	"context"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/mock"
	"gitlab.com/reinodovo/botdovo/internal/bot"
	"gitlab.com/reinodovo/botdovo/internal/clash"
	"gitlab.com/reinodovo/botdovo/internal/cwl_planning"
	"gitlab.com/reinodovo/botdovo/internal/mocks"
)

func TestWarRemainingTime(t *testing.T) {
	// setup
	expectedDuration := time.Hour + time.Minute
	// adding milliseconds to account for delay until time.Now() is called again, warRemainingTime should use an interface
	war := clash.War{EndTime: time.Now().UTC().Add(expectedDuration + 500*time.Millisecond).Format(cwlWarTimeFormat)}

	action := CWLAttacks{}

	duration := action.warRemainingTime(war)

	if duration != expectedDuration {
		t.Errorf("expected %v, got %v", expectedDuration, duration)
	}
}

func TestCWLAttacksGenerateMessage(t *testing.T) {
	// setup
	dummyWar := clash.War{
		Clan:     clash.WarClan{Name: "clan1", Stars: 20},
		Opponent: clash.WarClan{Name: "clan2", Stars: 22},
	}
	tests := []struct {
		war             warInfo
		expectedMessage string
	}{
		{
			war: warInfo{
				finishedAttacks: []finishedAttack{
					{ActualAttack: cwl_planning.ActualAttack{Player: "player2", Stars: 3, Position: 0}},
					{ActualAttack: cwl_planning.ActualAttack{Player: "player1", Stars: 2, Position: 3}},
				},
				pendingAttacks: []pendingAttack{
					{IdealAttack: cwl_planning.IdealAttack{Player: "player3", Stars: 2.8, Position: 1}, reserved: false},
					{IdealAttack: cwl_planning.IdealAttack{Player: "player3", Stars: 1.5, Position: 2}, reserved: true},
				},
				round:           2,
				remainingTime:   time.Hour,
				expectedStars:   9.3,
				hasIdealAttacks: true,
				war:             dummyWar,
			},
			expectedMessage: `<b>Dia #3</b>
clan1 20 x 22 clan2

Tempo Restante: <b>1h0m0s</b>
Estrelas Esperadas: <b>9.3</b>

<b>Ataques Pendentes</b>
2. player3 - 2.8 estrelas 
3. player3 - 1.5 estrelas <b>[reservado]</b>

<b>Ataques Feitos</b>
1. player2 - 3 estrelas
4. player1 - 2 estrelas`,
		},
		{
			war: warInfo{
				finishedAttacks: []finishedAttack{
					{ActualAttack: cwl_planning.ActualAttack{Player: "player1", Stars: 3, Position: 0}},
				},
				pendingAttacks:  []pendingAttack{},
				round:           0,
				remainingTime:   10 * time.Minute,
				expectedStars:   10,
				hasIdealAttacks: false,
				war:             dummyWar,
			},
			expectedMessage: `<b>Dia #1</b>
clan1 20 x 22 clan2

Tempo Restante: <b>10m0s</b>

<b>Ataques Pendentes</b>
Ademilson tá pensando ou alguem fez merda

<b>Ataques Feitos</b>
1. player1 - 3 estrelas`,
		},
		{
			war: warInfo{
				finishedAttacks: []finishedAttack{
					{ActualAttack: cwl_planning.ActualAttack{Player: "player1", Stars: 3, Position: 0}},
				},
				pendingAttacks:  []pendingAttack{},
				round:           0,
				remainingTime:   10 * time.Minute,
				expectedStars:   3,
				hasIdealAttacks: true,
				war:             dummyWar,
			},
			expectedMessage: `<b>Dia #1</b>
clan1 20 x 22 clan2

Tempo Restante: <b>10m0s</b>
Estrelas: <b>3</b>

<b>Ataques Feitos</b>
1. player1 - 3 estrelas`,
		},
		{
			war: warInfo{
				finishedAttacks: []finishedAttack{},
				pendingAttacks: []pendingAttack{
					{IdealAttack: cwl_planning.IdealAttack{Player: "player1", Stars: 2.8, Position: 0}, reserved: false},
					{IdealAttack: cwl_planning.IdealAttack{Player: "player2", Stars: 1.5, Position: 1}, reserved: true},
				},
				round:           6,
				remainingTime:   13 * time.Second,
				expectedStars:   4.3,
				hasIdealAttacks: true,
				war:             dummyWar,
			},
			expectedMessage: `<b>Dia #7</b>
clan1 20 x 22 clan2

Tempo Restante: <b>13s</b>
Estrelas Esperadas: <b>4.3</b>

<b>Ataques Pendentes</b>
1. player1 - 2.8 estrelas 
2. player2 - 1.5 estrelas <b>[reservado]</b>`,
		},
	}
	action := CWLAttacks{}
	for idx, test := range tests {
		// act
		message := action.generateMessage(test.war)
		// test
		if strings.TrimSpace(message) != strings.TrimSpace(test.expectedMessage) {
			t.Errorf("message and expectedMessage did not match for test %v", idx)
		}
	}
}

func TestExpectedStars(t *testing.T) {
	// setup
	idealAttacks := []cwl_planning.IdealAttack{
		{Stars: 1.5},
		{Stars: 2},
		{Stars: 3},
	}
	action := CWLAttacks{}
	expectedSum := float32(6.5)
	eps := float32(0.00001)
	// act
	sum := action.expectedStars(idealAttacks)
	// test
	if expectedSum+eps < sum || sum < expectedSum-eps {
		t.Errorf("expected %v, got %v", expectedSum, sum)
	}
}

func TestCWLAttacks(t *testing.T) {
	//setup
	idealAttacks := []cwl_planning.IdealAttack{
		{Position: 0, Stars: 1.5},
		{Position: 1, Stars: 3},
		{Position: 2, Stars: 2.8},
	}
	actualAttacks := []cwl_planning.ActualAttack{
		{Position: 1, Stars: 3, Finished: true},
		{Position: 2, Stars: 0, Finished: false},
	}
	warSize := 3
	action := CWLAttacks{}
	expectedFinishedAttacks := []finishedAttack{
		{ActualAttack: cwl_planning.ActualAttack{Position: 1, Stars: 3, Finished: true}},
	}
	expectedPendingAttacks := []pendingAttack{
		{IdealAttack: cwl_planning.IdealAttack{Position: 0, Stars: 1.5}},
		{IdealAttack: cwl_planning.IdealAttack{Position: 2, Stars: 2.8}, reserved: true},
	}
	// act
	finishedAttacks, pendingAttacks := action.cwlAttacks(warSize, idealAttacks, actualAttacks)
	// test
	if len(expectedFinishedAttacks) != len(finishedAttacks) {
		t.Fatalf("expected %v finished attacks, got %v", len(expectedFinishedAttacks), len(finishedAttacks))
	}
	for idx, finishedAttack := range finishedAttacks {
		expectedFinishedAttack := expectedFinishedAttacks[idx]
		if finishedAttack != expectedFinishedAttack {
			t.Errorf("finishedAttack at position %v differs: expected %v, got %v", idx, expectedFinishedAttack, finishedAttack)
		}
	}
	if len(expectedPendingAttacks) != len(pendingAttacks) {
		t.Fatalf("expected %v finished attacks, got %v", len(expectedPendingAttacks), len(pendingAttacks))
	}
	for idx, pendingAttack := range pendingAttacks {
		expectedPendingAttack := expectedPendingAttacks[idx]
		if pendingAttack != expectedPendingAttack {
			t.Errorf("pendingAttack at position %v differs: expected %v, got %v", idx, expectedPendingAttack, pendingAttack)
		}
	}
}

func TestCWLAttacksCommand(t *testing.T) {
	// setup
	var rankCommand bot.Command

	mockBot := &mocks.Bot{}
	mockBot.On("SetCommand", mock.Anything, "cwl_attacks", mock.MatchedBy(func(command bot.Command) bool {
		rankCommand = command
		return true
	}), mock.Anything).Return(nil)

	mockCWLPlanning := &mocks.CWLPlanning{}
	mockCWLPlanning.On("WarSize", mock.Anything).Return(5)
	mockCWLPlanning.On("AllAttacks", mock.Anything, mock.AnythingOfType("int")).Return(
		[]cwl_planning.IdealAttack{
			{Player: "player1", Stars: 2.8, Position: 3},
			{Player: "player2", Stars: 2, Position: 0},
			{Player: "player3", Stars: 1, Position: 1},
			{Player: "player4", Stars: 2.6, Position: 2},
			{Player: "player5", Stars: 3, Position: 4},
		},
		[]cwl_planning.ActualAttack{
			{Player: "player1", Stars: 2, Position: 3, Finished: true},
			{Player: "player4", Position: 2, Finished: false},
		},
		nil,
	)

	mockClashAPI := &mocks.ClashAPI{}
	// setting war end time to now so not to mess up the test, the time.Now() usage should be controlled by an interface so it can be mocked
	mockClashAPI.On("MainClanCurrentWar", mock.Anything).Return(clash.War{
		EndTime:  time.Now().UTC().Format(cwlWarTimeFormat),
		Clan:     clash.WarClan{Name: "clan1", Stars: 20},
		Opponent: clash.WarClan{Name: "clan2", Stars: 22},
	}, 2, nil)

	setupCWLAttacks(mockBot, mockCWLPlanning, mockClashAPI)

	expectedMessage := `<b>Dia #3</b>
clan1 20 x 22 clan2

Tempo Restante: <b>0s</b>
Estrelas Esperadas: <b>11.4</b>

<b>Ataques Pendentes</b>
1. player2 - 2 estrelas 
2. player3 - 1 estrelas 
3. player4 - 2.6 estrelas <b>[reservado]</b>
5. player5 - 3 estrelas 

<b>Ataques Feitos</b>
4. player1 - 2 estrelas`

	// test
	mockBot.On("SendMessage", mock.Anything, mock.MatchedBy(func(message string) bool {
		if strings.TrimSpace(message) != strings.TrimSpace(expectedMessage) {
			t.Errorf("expected %v, got %v", expectedMessage, message)
		}
		return true
	})).Return(0, nil)

	// act
	rankCommand(context.Background())
}
