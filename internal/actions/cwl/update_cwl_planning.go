package cwl

import (
	"bytes"
	"context"
	"fmt"
	"sort"

	"gitlab.com/reinodovo/botdovo/internal/bot"
	"gitlab.com/reinodovo/botdovo/internal/clash"
	"gitlab.com/reinodovo/botdovo/internal/cwl_planning"
	"gitlab.com/reinodovo/botdovo/internal/scheduler"
	"gitlab.com/reinodovo/botdovo/internal/utils"
	"go.opentelemetry.io/otel/trace"
)

type UpdateCWLPlanning struct {
	bot         bot.Bot
	cwlPlanning cwl_planning.CWLPlanning
	clashAPI    clash.ClashAPI
}

func setupUpdateCWLPlanning(bot bot.Bot, cwlPlanning cwl_planning.CWLPlanning, cron scheduler.Scheduler, clashAPI clash.ClashAPI) error {
	updateCWLAttacks := &UpdateCWLPlanning{
		bot:         bot,
		cwlPlanning: cwlPlanning,
		clashAPI:    clashAPI,
	}
	if _, err := cron.Schedule(scheduler.Minute, updateCWLAttacks.handleUpdateAttacks); err != nil {
		return err
	}
	return nil
}

func getPlayerName(clan clash.WarClan, members []clash.Player, tag string) string {
	for _, player := range members {
		if player.Tag == tag {
			return player.Name
		}
	}
	for _, player := range clan.Members {
		if player.Tag == tag {
			return player.Name
		}
	}
	return ""
}

func (action *UpdateCWLPlanning) generateNewAttacksMessage(newAttacks []cwl_planning.ActualAttack, war clash.War, round int) string {
	sort.Slice(war.Opponent.Members, func(i, j int) bool {
		return war.Opponent.Members[i].MapPosition < war.Opponent.Members[j].MapPosition
	})
	var buffer bytes.Buffer
	buffer.WriteString(fmt.Sprintf("<b>Novos Ataques No Dia %v:</b>\n", round+1))
	for _, attack := range newAttacks {
		percentage := war.Opponent.Members[attack.Position].BestOpponentAttack.DestructionPercentage
		buffer.WriteString(fmt.Sprintf("%v. %v - %v estrelas (%v%%)\n", attack.Position+1, attack.Player, attack.Stars, percentage))
	}
	return buffer.String()
}

func (action *UpdateCWLPlanning) newWarAttacks(actualAttacks []cwl_planning.ActualAttack, war clash.War, warSize int, members []clash.Player) []cwl_planning.ActualAttack {
	allActualAttacks := make([]cwl_planning.ActualAttack, warSize)
	for _, attack := range actualAttacks {
		allActualAttacks[attack.Position] = attack
	}

	sort.Slice(war.Opponent.Members, func(i, j int) bool {
		return war.Opponent.Members[i].MapPosition < war.Opponent.Members[j].MapPosition
	})
	var newAttacks []cwl_planning.ActualAttack
	for idx, opponent := range war.Opponent.Members {
		if opponent.OpponentAttacks > 0 {
			attack := cwl_planning.ActualAttack{
				Player:   getPlayerName(war.Clan, members, opponent.BestOpponentAttack.AttackerTag),
				Stars:    opponent.BestOpponentAttack.Stars,
				Position: idx,
				Finished: true,
			}
			sheetsAttack := allActualAttacks[idx]
			if sheetsAttack != attack {
				newAttacks = append(newAttacks, attack)
			}
		}
	}
	return newAttacks
}

type staleLock struct {
	cwl_planning.ActualAttack
	samePerson bool
	attacker   string
}

func (action *UpdateCWLPlanning) generateStaleLocksMessage(staleLocks []staleLock) string {
	var buffer bytes.Buffer
	for _, staleLock := range staleLocks {
		if staleLock.samePerson {
			buffer.WriteString(fmt.Sprintf("%v lockou o %v e atacou outra base, muito escroto\n", staleLock.attacker, staleLock.ActualAttack.Position+1))
		} else {
			buffer.WriteString(fmt.Sprintf("%v atacou o %v, base que %v tinha lockado, muuuito escroto\n", staleLock.attacker, staleLock.ActualAttack.Position+1, staleLock.ActualAttack.Player))
		}
	}
	return buffer.String()
}

func (action *UpdateCWLPlanning) findStaleLocks(actualAttacks []cwl_planning.ActualAttack, newAttacks []cwl_planning.ActualAttack) []staleLock {
	var staleLocks []staleLock
	for _, attack := range actualAttacks {
		if attack.Finished {
			continue
		}
		for _, newAttack := range newAttacks {
			if attack.Player == newAttack.Player && attack.Position != newAttack.Position {
				staleLocks = append(staleLocks, staleLock{
					ActualAttack: attack,
					samePerson:   true,
					attacker:     newAttack.Player,
				})
			}
			if attack.Player != newAttack.Player && attack.Position == newAttack.Position {
				staleLocks = append(staleLocks, staleLock{
					ActualAttack: attack,
					samePerson:   false,
					attacker:     newAttack.Player,
				})
			}
		}
	}
	return staleLocks
}

func positionWasAttacked(attacks []cwl_planning.ActualAttack, position int) bool {
	for _, attack := range attacks {
		if attack.Position == position {
			return true
		}
	}
	return false
}

func locksToRemove(newAttacks []cwl_planning.ActualAttack, staleLocks []staleLock) []cwl_planning.ActualAttack {
	var locksToRemove []cwl_planning.ActualAttack
	for _, staleLock := range staleLocks {
		if staleLock.samePerson && !positionWasAttacked(newAttacks, staleLock.Position) {
			locksToRemove = append(locksToRemove, cwl_planning.ActualAttack{Position: staleLock.ActualAttack.Position})
		}
	}
	return locksToRemove
}

func (action *UpdateCWLPlanning) handleUpdateAttacks(ctx context.Context) error {
	span := trace.SpanFromContext(ctx)

	members, err := action.clashAPI.MainClanMembers(ctx)
	if err != nil {
		utils.RecordErrorIfNotNil(span, err)
		members = []clash.Player{}
	}

	for round := 0; round < action.cwlPlanning.Rounds(ctx); round++ {
		war, err := action.clashAPI.MainClanCWLWar(ctx, round)
		if err != nil {
			// ignore error because war might not be available
			continue
		}
		actualAttacks, err := action.cwlPlanning.ActualAttacks(ctx, round)
		if err != nil {
			err = fmt.Errorf("an error occurred while trying to read CWL attacks for round %v: %v", round, err)
			utils.RecordErrorIfNotNil(span, err)
			return nil
		}
		warSize := action.cwlPlanning.WarSize(ctx)
		newAttacks := action.newWarAttacks(actualAttacks, war, warSize, members)

		staleLocks := action.findStaleLocks(actualAttacks, newAttacks)
		locksToRemove := locksToRemove(newAttacks, staleLocks)

		err = action.cwlPlanning.UpdateActualAttacks(ctx, round, append(newAttacks, locksToRemove...))
		if err != nil {
			err = fmt.Errorf("an error occurred while trying to update CWL attacks for round %v: %v", round, err)
			utils.RecordErrorIfNotNil(span, err)
			continue
		}

		err = action.cwlPlanning.UpdateOverwrittenAttacks(ctx, round, war)
		if err != nil {
			err = fmt.Errorf("an error occurred while trying to update overwritten attacks for round %v: %v", round, err)
			utils.RecordErrorIfNotNil(span, err)
			continue
		}

		if len(newAttacks) == 0 {
			continue
		}
		message := action.generateNewAttacksMessage(newAttacks, war, round)
		action.bot.SendMessage(ctx, message)
		message = action.generateStaleLocksMessage(staleLocks)
		action.bot.SendMessage(ctx, message)
	}
	return nil
}
