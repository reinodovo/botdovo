package cwl

import (
	"bytes"
	"context"
	"fmt"

	"gitlab.com/reinodovo/botdovo/internal/bot"
	"gitlab.com/reinodovo/botdovo/internal/clash"
	"gitlab.com/reinodovo/botdovo/internal/cwl_planning"
	"gitlab.com/reinodovo/botdovo/internal/scheduler"
	"gitlab.com/reinodovo/botdovo/internal/utils"
)

type UpdateCWLRoundsOverview struct {
	bot         bot.Bot
	cwlPlanning cwl_planning.CWLPlanning
	clashAPI    clash.ClashAPI
}

func setupUpdateCWLRoundsOverview(bot bot.Bot, cwlPlanning cwl_planning.CWLPlanning, cron scheduler.Scheduler, clashAPI clash.ClashAPI) error {
	updateCWLRoundsOverview := &UpdateCWLRoundsOverview{
		bot:         bot,
		cwlPlanning: cwlPlanning,
		clashAPI:    clashAPI,
	}
	if _, err := cron.Schedule(scheduler.Hour, updateCWLRoundsOverview.handleUpdateRoundsOverview); err != nil {
		return err
	}
	return nil
}

func (action *UpdateCWLRoundsOverview) generateRoundsOverviewMessage(finishedWars []cwl_planning.RoundOverview) string {
	var buffer bytes.Buffer
	winMessages := []string{"Tudo nosso", "Nada deles", "Comemos cu", "Mamaram"}
	loseMessages := []string{"Tudo deles", "Nada nosso", "Perdemo, tururu", "Comeram o nosso cu", "Mamamos"}
	for _, finishedWar := range finishedWars {
		result := utils.RandomString(loseMessages)
		if finishedWar.Won {
			result = utils.RandomString(winMessages)
		}
		buffer.WriteString(fmt.Sprintf("<b>%v</b> [%v x %v]\n", result, finishedWar.Stars, finishedWar.EnemyStars))
	}
	return buffer.String()
}

func (action *UpdateCWLRoundsOverview) finishedWarsOverviews(ctx context.Context) []cwl_planning.RoundOverview {
	var finishedWarOverviews []cwl_planning.RoundOverview
	for round := 0; round < action.cwlPlanning.Rounds(ctx); round++ {
		war, err := action.clashAPI.MainClanCWLWar(ctx, round)
		if err != nil || war.State != clash.WAR_ENDED {
			// error is ignored because round might not be available
			break
		}
		roundOverview := cwl_planning.RoundOverview{
			Stars:      war.Clan.Stars,
			Enemy:      war.Opponent.Name,
			EnemyStars: war.Opponent.Stars,
			Won:        war.Clan.Stars > war.Opponent.Stars || (war.Clan.Stars == war.Opponent.Stars && war.Clan.DestructionPercentage > war.Opponent.DestructionPercentage),
		}
		finishedWarOverviews = append(finishedWarOverviews, roundOverview)
	}
	return finishedWarOverviews
}

func (aciton *UpdateCWLRoundsOverview) newFinishedWarsOverviews(previousOverviews, currentOverviews []cwl_planning.RoundOverview) []cwl_planning.RoundOverview {
	var newFinishedWars []cwl_planning.RoundOverview
	for round, overview := range currentOverviews {
		if len(previousOverviews) <= round || previousOverviews[round] != overview {
			newFinishedWars = append(newFinishedWars, overview)
		}
	}
	return newFinishedWars
}

func (action *UpdateCWLRoundsOverview) handleUpdateRoundsOverview(ctx context.Context) error {
	previousOverviews, err := action.cwlPlanning.GetRoundsOverview(ctx)
	if err != nil {
		return err
	}
	currentOverviews := action.finishedWarsOverviews(ctx)
	newFinishedWars := action.newFinishedWarsOverviews(previousOverviews, currentOverviews)
	err = action.cwlPlanning.UpdateRoundsOverview(ctx, currentOverviews)
	if err != nil {
		return err
	}
	message := action.generateRoundsOverviewMessage(newFinishedWars)
	_, err = action.bot.SendMessage(ctx, message)
	return err
}
