package cwl

import (
	"context"

	"gitlab.com/reinodovo/botdovo/internal/bot"
	"gitlab.com/reinodovo/botdovo/internal/device"
	"gitlab.com/reinodovo/botdovo/internal/scheduler"
	"gitlab.com/reinodovo/botdovo/internal/utils"
	"go.opentelemetry.io/otel"
)

type StartCWL struct {
	bot    bot.Bot
	device device.Device
}

func setupStartCWL(bot bot.Bot, cron scheduler.Scheduler, device device.Device) error {
	startCWL := &StartCWL{
		bot:    bot,
		device: device,
	}
	if _, err := cron.Schedule(scheduler.CWLStart, startCWL.handleStart); err != nil {
		return err
	}
	return nil
}

func (action *StartCWL) handleStart(ctx context.Context) error {
	ctx, span := otel.Tracer(tracerName).Start(ctx, "handle_start")
	defer span.End()

	err := action.device.StartCWL(ctx)
	if err != nil {
		utils.RecordErrorIfNotNil(span, err)
		return err
	}

	_, err = action.bot.SendMessage(ctx, "Comecei a pesquisar a CWL")
	if err != nil {
		utils.RecordErrorIfNotNil(span, err)
		return err
	}

	return nil
}
