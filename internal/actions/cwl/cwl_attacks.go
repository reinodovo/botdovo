package cwl

import (
	"bytes"
	"context"
	"fmt"
	"time"

	"gitlab.com/reinodovo/botdovo/internal/bot"
	"gitlab.com/reinodovo/botdovo/internal/clash"
	"gitlab.com/reinodovo/botdovo/internal/cwl_planning"
	"gitlab.com/reinodovo/botdovo/internal/utils"
	"go.opentelemetry.io/otel/trace"
)

var (
	invalidPositionError error = fmt.Errorf("Invalid Position")
)

var cwlWarTimeFormat string = "20060102T150405.000Z"

type SheetsAttacksLocation struct {
	attackerColumn string
	starsColumn    string
	row            int64
}

type CWLAttacks struct {
	bot         bot.Bot
	cwlPlanning cwl_planning.CWLPlanning
	clashAPI    clash.ClashAPI
}

func setupCWLAttacks(bot bot.Bot, cwlPlanning cwl_planning.CWLPlanning, clashAPI clash.ClashAPI) error {
	action := &CWLAttacks{
		bot:         bot,
		cwlPlanning: cwlPlanning,
		clashAPI:    clashAPI,
	}
	cwlAttacksExplanation := "ataques pendentes e realizados da guerra atual da CWL"
	if err := bot.SetCommand(context.Background(), "cwl_attacks", action.cwlAttacksCommand, cwlAttacksExplanation); err != nil {
		return err
	}
	return nil
}

func (action *CWLAttacks) warRemainingTime(war clash.War) time.Duration {
	endTime, _ := time.Parse(cwlWarTimeFormat, war.EndTime)
	diff := endTime.Sub(time.Now().UTC())
	return diff.Truncate(time.Second)
}

func (action *CWLAttacks) expectedStars(idealAttacks []cwl_planning.IdealAttack) float32 {
	var expectedStars float32 = 0
	for _, attack := range idealAttacks {
		expectedStars += attack.Stars
	}
	return expectedStars
}

type pendingAttack struct {
	cwl_planning.IdealAttack
	reserved bool
}

type finishedAttack struct {
	cwl_planning.ActualAttack
}

type warInfo struct {
	finishedAttacks []finishedAttack
	pendingAttacks  []pendingAttack
	round           int
	remainingTime   time.Duration
	expectedStars   float32
	hasIdealAttacks bool
	war             clash.War
}

func (action *CWLAttacks) generateMessage(war warInfo) string {
	var buffer bytes.Buffer
	buffer.WriteString(fmt.Sprintf("<b>Dia #%v</b>\n", war.round+1))
	buffer.WriteString(fmt.Sprintf("%v %v x %v %v\n\n", war.war.Clan.Name, war.war.Clan.Stars, war.war.Opponent.Stars, war.war.Opponent.Name))
	buffer.WriteString(fmt.Sprintf("Tempo Restante: <b>%v</b>\n", war.remainingTime))

	if war.hasIdealAttacks {
		if len(war.pendingAttacks) == 0 {
			buffer.WriteString(fmt.Sprintf("Estrelas: <b>%v</b>\n", war.expectedStars))
		} else {
			buffer.WriteString(fmt.Sprintf("Estrelas Esperadas: <b>%v</b>\n", war.expectedStars))
		}
	}

	if len(war.pendingAttacks) > 0 {
		buffer.WriteString("\n")
		buffer.WriteString("<b>Ataques Pendentes</b>\n")
		for _, attack := range war.pendingAttacks {
			suffix := ""
			if attack.reserved {
				suffix = "<b>[reservado]</b>"
			}
			buffer.WriteString(fmt.Sprintf("%v. %v - %v estrelas %v\n", attack.Position+1, attack.Player, attack.Stars, suffix))
		}
	} else if !war.hasIdealAttacks {
		buffer.WriteString("\n")
		buffer.WriteString("<b>Ataques Pendentes</b>\n")
		buffer.WriteString("Ademilson tá pensando ou alguem fez merda\n")
	}

	if len(war.finishedAttacks) > 0 {
		buffer.WriteString("\n")
		buffer.WriteString("<b>Ataques Feitos</b>\n")
		for _, attack := range war.finishedAttacks {
			buffer.WriteString(fmt.Sprintf("%v. %v - %v estrelas\n", attack.Position+1, attack.Player, attack.Stars))
		}
	}

	return buffer.String()
}

func (aciton *CWLAttacks) cwlAttacks(warSize int, idealAttacks []cwl_planning.IdealAttack, actualAttacks []cwl_planning.ActualAttack) (finished []finishedAttack, pending []pendingAttack) {
	hasActualAttack := make([]bool, warSize)
	finishedActualAttack := make([]bool, warSize)
	for _, attack := range actualAttacks {
		if attack.Finished {
			finished = append(finished, finishedAttack{ActualAttack: attack})
			finishedActualAttack[attack.Position] = true
		}
		hasActualAttack[attack.Position] = true
	}

	for _, attack := range idealAttacks {
		if !finishedActualAttack[attack.Position] {
			pending = append(pending, pendingAttack{
				IdealAttack: attack,
				reserved:    hasActualAttack[attack.Position],
			})
		}
	}
	return
}

func (action *CWLAttacks) cwlAttacksCommand(ctx context.Context, args ...string) error {
	span := trace.SpanFromContext(ctx)

	war, round, err := action.clashAPI.MainClanCurrentWar(ctx)
	if err != nil {
		utils.RecordErrorIfNotNil(span, err)
		_, err := action.bot.SendMessage(ctx, "não consegui descobrir qual é o round atual")
		return err
	}

	idealAttacks, actualAttacks, err := action.cwlPlanning.AllAttacks(ctx, round)
	if err != nil {
		utils.RecordErrorIfNotNil(span, err)
		_, err := action.bot.SendMessage(ctx, "não consegui ler a planilha")
		return err
	}

	warSize := action.cwlPlanning.WarSize(ctx)
	finishedAttacks, pendingAttacks := action.cwlAttacks(warSize, idealAttacks, actualAttacks)

	message := action.generateMessage(warInfo{
		finishedAttacks: finishedAttacks,
		pendingAttacks:  pendingAttacks,
		round:           round,
		remainingTime:   action.warRemainingTime(war),
		expectedStars:   action.expectedStars(idealAttacks),
		hasIdealAttacks: len(idealAttacks) != 0,
		war:             war,
	})
	_, err = action.bot.SendMessage(ctx, message)
	return err
}
