package cwl

import (
	"bytes"
	"context"
	"fmt"

	"gitlab.com/reinodovo/botdovo/internal/bot"
	"gitlab.com/reinodovo/botdovo/internal/clash"
	"gitlab.com/reinodovo/botdovo/internal/cwl_planning"
	"gitlab.com/reinodovo/botdovo/internal/scheduler"
	"gitlab.com/reinodovo/botdovo/internal/utils"
	"go.opentelemetry.io/otel/trace"
)

type FixMislabeledAttacks struct {
	bot         bot.Bot
	cwlPlanning cwl_planning.CWLPlanning
	clashAPI    clash.ClashAPI
}

func setupFixMislabeledAttacks(bot bot.Bot, cwlPlanning cwl_planning.CWLPlanning, cron scheduler.Scheduler, clashAPI clash.ClashAPI) error {
	fixMislabeledAttacks := &FixMislabeledAttacks{
		bot:         bot,
		cwlPlanning: cwlPlanning,
		clashAPI:    clashAPI,
	}
	if _, err := cron.Schedule(scheduler.Hour, fixMislabeledAttacks.handleFixMislabeledAttacks); err != nil {
		return err
	}
	return nil
}

func (action *FixMislabeledAttacks) rosterPlayerNameMapping(roster []cwl_planning.PlayerRoster) map[string]int {
	rosterMapping := make(map[string]int)
	for idx, playerRoster := range roster {
		rosterMapping[playerRoster.Player] = idx
	}
	return rosterMapping
}

type mislabeledRosterAttack struct {
	name   string
	round  int
	missed bool
}

func (action *FixMislabeledAttacks) generateMislabeledAttacksMessage(mislabeledAttacks []mislabeledRosterAttack) string {
	var didAttackBuffer bytes.Buffer
	var didNotAttackBuffer bytes.Buffer
	for _, mislabeledAttack := range mislabeledAttacks {
		if mislabeledAttack.missed {
			didNotAttackBuffer.WriteString(fmt.Sprintf("%v não atacou no dia %v\n", mislabeledAttack.name, mislabeledAttack.round+1))
		} else {
			didAttackBuffer.WriteString(fmt.Sprintf("%v atacou no dia %v\n", mislabeledAttack.name, mislabeledAttack.round+1))
		}
	}
	var buffer bytes.Buffer
	if didNotAttackBuffer.Len() != 0 {
		buffer.WriteString("<b>Lista de filhos da puta que não atacaram</b>\n")
		buffer.WriteString(didNotAttackBuffer.String())
		buffer.WriteString("\n")
	}
	if didAttackBuffer.Len() != 0 {
		buffer.WriteString("<b>Lista de filhos da puta que na verdade atacaram</b>\n")
		buffer.WriteString(didAttackBuffer.String())
		buffer.WriteString("\n")
	}
	return buffer.String()
}

func (action *FixMislabeledAttacks) mislabeledAttacks(roster []cwl_planning.PlayerRoster, war clash.War, round int) []mislabeledRosterAttack {
	var mislabeledAttacks []mislabeledRosterAttack
	rosterMapping := action.rosterPlayerNameMapping(roster)
	for _, member := range war.Clan.Members {
		missedAttack := len(member.Attacks) == 0
		if idx, ok := rosterMapping[member.Name]; ok {
			if roster[idx].Attacks[round].AttackerCode != 0 && roster[idx].Attacks[round].MissedAttack != missedAttack {
				mislabeledAttacks = append(mislabeledAttacks, mislabeledRosterAttack{name: member.Name, round: round, missed: missedAttack})
			}
		}
	}
	return mislabeledAttacks
}

func (action *FixMislabeledAttacks) fixRosterMislabeledAttacks(roster []cwl_planning.PlayerRoster, mislabeledAttacks []mislabeledRosterAttack) []cwl_planning.PlayerRoster {
	rosterMapping := action.rosterPlayerNameMapping(roster)
	for _, mislabeledAttack := range mislabeledAttacks {
		if idx, ok := rosterMapping[mislabeledAttack.name]; ok {
			roster[idx].Attacks[mislabeledAttack.round].MissedAttack = mislabeledAttack.missed
		}
	}
	return roster
}

func (action *FixMislabeledAttacks) handleFixMislabeledAttacks(ctx context.Context) error {
	span := trace.SpanFromContext(ctx)

	roster, err := action.cwlPlanning.GetRoster(ctx)
	if err != nil {
		err := fmt.Errorf("could not get roster information: %v", err)
		utils.RecordErrorIfNotNil(span, err)
		return err
	}

	var mislabeledAttacks []mislabeledRosterAttack
	for round := 0; round < action.cwlPlanning.Rounds(ctx); round++ {
		war, err := action.clashAPI.MainClanCWLWar(ctx, round)
		if err != nil || war.State != clash.WAR_ENDED {
			// ignore error because war might not be available
			continue
		}
		mislabeledRoundAttacks := action.mislabeledAttacks(roster, war, round)
		mislabeledAttacks = append(mislabeledAttacks, mislabeledRoundAttacks...)
	}
	roster = action.fixRosterMislabeledAttacks(roster, mislabeledAttacks)
	err = action.cwlPlanning.UpdateRoster(ctx, roster)
	if err != nil {
		err := fmt.Errorf("an error occurred while updating the roster: %v", err)
		utils.RecordErrorIfNotNil(span, err)
		return err
	}

	message := action.generateMislabeledAttacksMessage(mislabeledAttacks)
	if len(message) == 0 {
		return nil
	}
	_, err = action.bot.SendMessage(ctx, message)
	return err
}
