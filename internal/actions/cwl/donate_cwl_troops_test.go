package cwl

import (
	"context"
	"reflect"
	"testing"
	"time"

	"github.com/stretchr/testify/mock"
	"gitlab.com/reinodovo/botdovo/internal/clash"
	"gitlab.com/reinodovo/botdovo/internal/mocks"
	"gitlab.com/reinodovo/botdovo/internal/utils"
)

func TestEmptyDonationInfo(t *testing.T) {
	mockDatabase := &mocks.Database{}
	mockDatabase.On("Get", mock.Anything, donationInfoCollectionName, donationInfoKeyName, mock.Anything).Return(nil).Run(func(args mock.Arguments) {
		donationInfo := args.Get(3).(*DonationInfo)
		*donationInfo = DonationInfo{}
	})
	donations := map[string][]int{
		"player1": {1, 2, 3},
		"player2": {1, 2, 3},
		"player3": {1, 2, 3},
	}
	expectedDonationInfo := DonationInfo{
		Donations:          donations,
		Index:              0,
		HasTrainingStarted: true,
		NextActionDate:     time.Now(),
	}
	mockDatabase.On("SaveObject", mock.Anything, donationInfoCollectionName, donationInfoKeyName, mock.MatchedBy(func(donationInfo DonationInfo) bool {
		return compareDonationInfo(donationInfo, expectedDonationInfo)
	})).Return(nil).Once()

	mockClashAPI := &mocks.ClashAPI{}
	mockClashAPI.On("CWLIsHappening", mock.Anything).Return(true, nil)
	mockClashAPI.On("MainClanCurrentPreparation", mock.Anything).Return(clash.War{
		StartTime: "20210101T000000.000Z",
		TeamSize:  3,
		Clan: clash.WarClan{
			Members: []clash.WarPlayer{
				{
					Name:        "player1",
					MapPosition: 1,
				},
				{
					Name:        "player2",
					MapPosition: 2,
				},
				{
					Name:        "player3",
					MapPosition: 3,
				},
			},
		},
	}, 0, nil)

	mockCWLPlanning := &mocks.CWLPlanning{}
	mockCWLPlanning.On("Donations", mock.Anything).Return(donations, nil).Once()

	mockDevice := &mocks.Device{}
	mockDevice.On("TrainTroops", mock.Anything, 0, 3).Return(0, nil).Once()

	action := DonateCWLTroops{
		database:    mockDatabase,
		clashAPI:    mockClashAPI,
		cwlPlanning: mockCWLPlanning,
		device:      mockDevice,
	}

	action.handleDonations(context.Background())

	mockDatabase.AssertExpectations(t)
}

func compareDonationInfo(donationInfo DonationInfo, expectedDonationInfo DonationInfo) bool {
	if donationInfo.Index != expectedDonationInfo.Index {
		return false
	} else if donationInfo.HasTrainingStarted != expectedDonationInfo.HasTrainingStarted {
		return false
	} else if !reflect.DeepEqual(donationInfo.Donations, expectedDonationInfo.Donations) {
		return false
	} else if donationInfo.NextActionDate.Equal(expectedDonationInfo.NextActionDate) {
		return true
	} else if !donationInfo.NextActionDate.After(expectedDonationInfo.NextActionDate) {
		return false
	} else if !donationInfo.NextActionDate.Before(expectedDonationInfo.NextActionDate.Add(time.Second)) {
		return false
	}
	return true
}

const (
	ActionTrain = iota
	ActionDonate
	ActionUpdateDonations
)

func TestDifferentScenarios(t *testing.T) {
	donations := map[string][]int{
		"player1": {1, 0, 2, 0, 0, 2, 0},
		"player2": {0, 0, 2, 0, 0, 3, 0},
		"player3": {1, 0, 2, 0, 0, 2, 0},
	}
	actionDate := time.Now()
	timeString := "20210101T000000.000Z"
	warStartTime := utils.ParseTime(timeString)
	preparationWar := clash.War{
		StartTime: timeString,
		TeamSize:  3,
		Clan: clash.WarClan{
			Members: []clash.WarPlayer{
				{
					Name:        "player1",
					MapPosition: 1,
				},
				{
					Name:        "player2",
					MapPosition: 2,
				},
				{
					Name:        "player3",
					MapPosition: 3,
				},
			},
		},
	}
	trainingTime := 20
	var tests = []struct {
		donationInfo         DonationInfo
		preparationWar       clash.War
		preparationDay       int
		expectedDonationInfo DonationInfo
		expectedAction       int
		trainIndex           int
		trainQuantity        int
		donations            []int
	}{
		{
			donationInfo: DonationInfo{
				Donations:          donations,
				Index:              0,
				HasTrainingStarted: false,
				NextActionDate:     actionDate,
			},
			preparationWar: preparationWar,
			preparationDay: 0,
			expectedDonationInfo: DonationInfo{
				Donations:          donations,
				Index:              0,
				HasTrainingStarted: true,
				NextActionDate:     actionDate.Add(time.Duration(trainingTime) * time.Second),
			},
			expectedAction: ActionTrain,
			trainIndex:     0,
			trainQuantity:  2,
		},
		{
			donationInfo: DonationInfo{
				Donations:          donations,
				Index:              0,
				HasTrainingStarted: true,
				NextActionDate:     actionDate,
			},
			preparationWar: preparationWar,
			preparationDay: 0,
			expectedDonationInfo: DonationInfo{
				Donations:          donations,
				Index:              1,
				HasTrainingStarted: false,
				NextActionDate:     actionDate.Add(time.Duration(5) * time.Minute),
			},
			expectedAction: ActionDonate,
			donations:      []int{1, 0, 1},
		},
		{
			donationInfo: DonationInfo{
				Donations:          donations,
				Index:              3,
				HasTrainingStarted: false,
				NextActionDate:     actionDate,
			},
			preparationWar: preparationWar,
			preparationDay: 0,
			expectedDonationInfo: DonationInfo{
				Donations:          donations,
				Index:              5,
				HasTrainingStarted: true,
				NextActionDate:     actionDate.Add(time.Duration(trainingTime) * time.Second),
			},
			expectedAction: ActionTrain,
			trainIndex:     5,
			trainQuantity:  7,
		},
		{
			donationInfo: DonationInfo{
				Donations:          donations,
				Index:              6,
				HasTrainingStarted: false,
				NextActionDate:     actionDate,
			},
			preparationWar: preparationWar,
			preparationDay: 0,
			expectedDonationInfo: DonationInfo{
				Donations:          donations,
				Index:              0,
				HasTrainingStarted: false,
				NextActionDate:     warStartTime.Add(30 * time.Minute),
			},
			expectedAction: ActionUpdateDonations,
		},
	}
	for _, test := range tests {
		mockDevice := &mocks.Device{}
		if test.expectedAction == ActionTrain {
			mockDevice.On("TrainTroops", mock.Anything, test.trainIndex, test.trainQuantity).Return(trainingTime, nil).Once()
		} else if test.expectedAction == ActionDonate {
			mockDevice.On("DonateTroops", mock.Anything, test.preparationDay, test.donations).Return(nil).Once()
		}

		mockCWLPlanning := &mocks.CWLPlanning{}
		if test.expectedAction == ActionUpdateDonations {
			mockCWLPlanning.On("Donations", mock.Anything).Return(donations, nil).Once()
		}

		action := DonateCWLTroops{device: mockDevice, cwlPlanning: mockCWLPlanning}

		donationInfo, _, err := action.performDonationAction(context.Background(), test.donationInfo, test.preparationWar, test.preparationDay)
		if err != nil {
			t.Errorf("Unexpected error: %v", err)
		}
		if !compareDonationInfo(donationInfo, test.expectedDonationInfo) {
			t.Errorf("Expected donation info to be %v, got %v", test.expectedDonationInfo, donationInfo)
		}
		mockDevice.AssertExpectations(t)
		mockCWLPlanning.AssertExpectations(t)
	}
}
