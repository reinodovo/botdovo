package cwl

import (
	"context"
	"fmt"
	"strconv"

	"gitlab.com/reinodovo/botdovo/internal/bot"
	"gitlab.com/reinodovo/botdovo/internal/clash"
	"gitlab.com/reinodovo/botdovo/internal/cwl_planning"
	"gitlab.com/reinodovo/botdovo/internal/utils"
)

type CWLAttackLocks struct {
	bot         bot.Bot
	cwlPlanning cwl_planning.CWLPlanning
	clashAPI    clash.ClashAPI
}

func setupCWLAttackLocks(bot bot.Bot, cwlPlanning cwl_planning.CWLPlanning, clashAPI clash.ClashAPI) error {
	cwlAttackLocks := CWLAttackLocks{
		bot:         bot,
		cwlPlanning: cwlPlanning,
		clashAPI:    clashAPI,
	}
	lockExplanation := "[conta] [posição] reserva um ataque na CWL"
	if err := bot.SetCommand(context.Background(), "lock", cwlAttackLocks.lock, lockExplanation); err != nil {
		return err
	}
	unlockExplanation := "[conta] retira uma reserva na CWL"
	if err := bot.SetCommand(context.Background(), "unlock", cwlAttackLocks.unlock, unlockExplanation); err != nil {
		return err
	}
	return nil
}

const (
	free int = iota
	reserved
	finished
)

func (action *CWLAttackLocks) findAttackStateByPosition(actualAttacks []cwl_planning.ActualAttack, position int64) (cwl_planning.ActualAttack, int) {
	for _, attack := range actualAttacks {
		if attack.Position == int(position) {
			if attack.Finished {
				return attack, finished
			} else {
				return attack, reserved
			}
		}
	}
	return cwl_planning.ActualAttack{}, free
}

func (action *CWLAttackLocks) findAttackStateByPlayer(actualAttacks []cwl_planning.ActualAttack, player string) (cwl_planning.ActualAttack, int) {
	for _, attack := range actualAttacks {
		if attack.Player == player {
			if attack.Finished {
				return attack, finished
			} else {
				return attack, reserved
			}
		}
	}
	return cwl_planning.ActualAttack{}, free
}

func (action *CWLAttackLocks) isPlayerInRoundRoster(roster []cwl_planning.PlayerRoster, player string, round int) bool {
	var targetRoster cwl_planning.PlayerRoster
	for _, playerRoster := range roster {
		if playerRoster.Player == player {
			targetRoster = playerRoster
		}
	}
	if len(targetRoster.Attacks) <= round || targetRoster.Attacks[round].AttackerCode == rune(0) {
		return false
	}
	return true
}

func (action *CWLAttackLocks) parseLockArgs(warSize int, args ...string) (string, int64, error) {
	if len(args) == 2 {
		player := args[0]
		position, err := strconv.ParseInt(args[1], 10, 32)
		if err != nil || position < 1 || position > int64(warSize) {
			return player, position, invalidPositionError
		}
		position--
		return player, position, nil
	} else {
		return "", 0, utils.InvalidArgumentsError
	}
}

func (action *CWLAttackLocks) parseUnlockArgs(args ...string) (string, error) {
	if len(args) == 1 {
		player := args[0]
		return player, nil
	} else {
		return "", utils.InvalidArgumentsError
	}
}

func (action *CWLAttackLocks) lock(ctx context.Context, args ...string) error {
	warSize := action.cwlPlanning.WarSize(ctx)
	player, position, err := action.parseLockArgs(warSize, args...)
	if err != nil {
		_, err := action.bot.SendMessage(ctx, "não entendi, tu tá usando o comando direito?")
		return err
	}
	_, round, err := action.clashAPI.MainClanCurrentWar(ctx)
	if err != nil {
		_, err := action.bot.SendMessage(ctx, "não consegui descobrir qual é o round atual")
		return err
	}
	actualAttacks, actualAttacksErr := action.cwlPlanning.ActualAttacks(ctx, round)
	roster, rosterErr := action.cwlPlanning.GetRoster(ctx)
	if actualAttacksErr != nil || rosterErr != nil {
		_, err := action.bot.SendMessage(ctx, "não consegui pegar informações do sheets")
		return err
	}
	if !action.isPlayerInRoundRoster(roster, player, round) {
		_, err := action.bot.SendMessage(ctx, fmt.Sprintf("%v não tá marcado pra atacar no dia %v", player, round+1))
		return err
	}
	_, positionAttackState := action.findAttackStateByPosition(actualAttacks, position)
	_, playerAttackState := action.findAttackStateByPlayer(actualAttacks, player)
	if positionAttackState != free || playerAttackState != free {
		_, err := action.bot.SendMessage(ctx, "essa reserva não pode ser feita")
		return err
	}
	lock := cwl_planning.ActualAttack{
		Player:   player,
		Position: int(position),
		Finished: false,
	}
	err = action.cwlPlanning.UpdateActualAttacks(ctx, round, []cwl_planning.ActualAttack{lock})
	if err != nil {
		_, err := action.bot.SendMessage(ctx, "parece que deu merda na hora de reservar")
		return err
	}
	_, err = action.bot.SendMessage(ctx, fmt.Sprintf("<b>reservado</b> (%v. %v)", position+1, player))
	return err
}

func (action *CWLAttackLocks) unlock(ctx context.Context, args ...string) error {
	player, err := action.parseUnlockArgs(args...)
	if err != nil {
		_, err := action.bot.SendMessage(ctx, "não entendi, tu tá usando o comando direito?")
		return err
	}
	_, round, err := action.clashAPI.MainClanCurrentWar(ctx)
	if err != nil {
		_, err := action.bot.SendMessage(ctx, "não consegui descobrir qual é o round atual")
		return err
	}
	actualAttacks, err := action.cwlPlanning.ActualAttacks(ctx, round)
	if err != nil {
		_, err := action.bot.SendMessage(ctx, "não consegui pegar informações do sheets")
		return err
	}
	lockedAttack, playerAttackState := action.findAttackStateByPlayer(actualAttacks, player)
	if playerAttackState == finished {
		_, err := action.bot.SendMessage(ctx, fmt.Sprintf("%v já atacou", player))
		return err
	} else if playerAttackState == free {
		_, err := action.bot.SendMessage(ctx, fmt.Sprintf("%v não tem reserva", player))
		return err
	}
	lockedAttack.Player = ""
	err = action.cwlPlanning.UpdateActualAttacks(ctx, round, []cwl_planning.ActualAttack{lockedAttack})
	if err != nil {
		_, err := action.bot.SendMessage(ctx, "parece que deu merda na hora de tirar a reserva")
		return err
	}
	_, err = action.bot.SendMessage(ctx, fmt.Sprintf("<b>reserva removida</b>, %v", player))
	return err
}
