package actions

import (
	"context"
	"fmt"
	"log"

	"gitlab.com/reinodovo/botdovo/internal/bot"
	"gitlab.com/reinodovo/botdovo/internal/cwl_planning/cwl_sheets"
	"gitlab.com/reinodovo/botdovo/internal/scheduler"
	"gitlab.com/reinodovo/botdovo/internal/utils"
	"go.opentelemetry.io/otel/trace"
)

const (
	updateTokenCommand = "update_token"
	setTokenCommand    = "set_token"
	setSheetsIdCommand = "set_sheets_id"
	planningUrl        = "http://cwl.reinodovo.org/planning"
)

type SheetsManagement struct {
	bot       bot.Bot
	cwlSheets *cwl_sheets.CWLSheets
	logger    *log.Logger
}

func NewSheetsManagement(bot bot.Bot, cwlSheets *cwl_sheets.CWLSheets, cron scheduler.Scheduler, logger *log.Logger) *SheetsManagement {
	sheetsManagement := &SheetsManagement{
		bot:       bot,
		cwlSheets: cwlSheets,
		logger:    logger,
	}

	bot.SetCommand(context.Background(), updateTokenCommand, sheetsManagement.updateToken, "mostra o link para atualizar o token")
	bot.SetCommand(context.Background(), setTokenCommand, sheetsManagement.setToken, "[token] atualiza o token")
	bot.SetCommand(context.Background(), setSheetsIdCommand, sheetsManagement.setSheetsId, "[id] atualiza a planilha do mês")

	_, err := cron.Schedule(scheduler.Saturday, sheetsManagement.scheduleUpdateToken)
	if err != nil {
		logger.Printf("could not create schedulings for SheetsManagement: %v", err)
	}
	_, err = cron.Schedule(scheduler.Month, cwlSheets.UnsetSheetsId)
	if err != nil {
		log.Printf("could not schedule sheets id reset job: %v", err)
	}
	return sheetsManagement
}

func (action *SheetsManagement) scheduleUpdateToken(ctx context.Context) error {
	return action.updateToken(ctx)
}

func (action *SheetsManagement) updateToken(ctx context.Context, args ...string) error {
	authURL, err := action.cwlSheets.GetAuthURL()
	if err != nil {
		action.logger.Printf("error getting sheets auth URL: %v", err)
		return err
	}
	message := fmt.Sprintf("Para atualizar o token clique <a href=\"%v\">aqui</a> e mande o código gerado para o comando %v", authURL, setTokenCommand)
	_, err = action.bot.SendMessage(ctx, message)
	if err != nil {
		action.logger.Printf("error send auth message to bot: %v", err)
		return err
	}
	return nil
}

func (action *SheetsManagement) setToken(ctx context.Context, args ...string) error {
	if len(args) == 1 {
		saveTokenErr := action.cwlSheets.SaveToken(args[0])
		updateSheetsServiceErr := action.cwlSheets.UpdateToken()
		if saveTokenErr != nil || updateSheetsServiceErr != nil {
			_, err := action.bot.SendMessage(ctx, "Deu merda, tenta de novo")
			return err
		}
		_, err := action.bot.SendMessage(ctx, utils.RandomString(utils.PositiveReplies))
		return err
	} else {
		_, err := action.bot.SendMessage(ctx, "Manda o código como argumento, filho da puta")
		return err
	}
}

func (action *SheetsManagement) setSheetsId(ctx context.Context, args ...string) error {
	if len(args) == 1 {
		err := action.cwlSheets.SetSheetsId(args[0])
		if err != nil {
			span := trace.SpanFromContext(ctx)
			utils.RecordErrorIfNotNil(span, err)

			_, err := action.bot.SendMessage(ctx, "Deu merda, tenta de novo")
			return err
		}
		_, err = action.bot.SendMessageAndPin(ctx, planningUrl)
		return err
	}
	_, err := action.bot.SendMessage(ctx, "Manda o id como argumento, filho da puta")
	return err
}
