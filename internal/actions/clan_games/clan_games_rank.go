package clan_games

import (
	"bytes"
	"context"
	"fmt"
	"math"
	"sort"
	"strings"
	"time"

	"gitlab.com/reinodovo/botdovo/internal/bot"
	"gitlab.com/reinodovo/botdovo/internal/clash"
	"gitlab.com/reinodovo/botdovo/internal/configuration_manager"
	"gitlab.com/reinodovo/botdovo/internal/information_manager"
	"gitlab.com/reinodovo/botdovo/internal/utils"
)

var detailKey = "detail"

type clanGamesRank struct {
	bot     bot.Bot
	config  configuration_manager.ConfigurationManager
	utils   ClanGamesUtils
	trigger func(ctx context.Context) error
}

func setupClanGamesRank(bot bot.Bot, config configuration_manager.ConfigurationManager, mgr information_manager.InformationManager, utils ClanGamesUtils) error {
	clanGames := &clanGamesRank{config: config, bot: bot, utils: utils}
	clanGamesExplanation := "rank de pontuação no clan games para esse mês por pessoa (detail, d - mostra pontuação por conta)"
	if err := bot.SetCommand(context.Background(), "clan_games", clanGames.rankCommand, clanGamesExplanation); err != nil {
		return err
	}
	var err error = nil
	clanGames.trigger, err = mgr.GenerateTrigger(information_manager.Player, information_manager.Month, clanGames.handleClanGamesRank)
	return err
}

func (action *clanGamesRank) remainingTimeString() string {
	remaining := action.utils.RemainingTime()
	days := math.Floor(remaining.Hours() / 24)
	remaining -= time.Duration(days*24) * time.Hour
	timeString := remaining.Truncate(time.Second).String()
	if days > 0 {
		timeString = fmt.Sprintf("%vd%v", int(days), timeString)
	}
	return timeString
}

func generateRankMessage(players []playerClanGames, shouldShowAccounts, started, ended bool, remainingTime string) string {
	if !started {
		return "Não enche meu saco"
	}

	total := 0
	for _, player := range players {
		total += player.points
	}

	var buffer bytes.Buffer
	buffer.WriteString(fmt.Sprintf("<b>Clan Games</b> - <b>%v</b> pontos\n", total))
	if !ended {
		buffer.WriteString(fmt.Sprintf("Tempo Restante: <b>%v</b>\n", remainingTime))
	} else {
		buffer.WriteString("Acabou a doença\n")
	}
	buffer.WriteString("\n")

	for rank, player := range players {
		buffer.WriteString(fmt.Sprintf("<b>%v.</b> %v - <b>%v</b>\n", rank+1, player.name, player.points))
		if shouldShowAccounts {
			sortAccountsByClanGamesPoints(player.accounts)
			for _, account := range player.accounts {
				buffer.WriteString(fmt.Sprintf("        %v - <b>%v</b>\n", account.name, account.points))
			}
		}
	}

	return strings.TrimSpace(buffer.String())
}

func sortPlayersByClanGamesPoints(players []playerClanGames) {
	sort.Slice(players[:], func(i, j int) bool {
		return players[i].points > players[j].points
	})
}

func sortAccountsByClanGamesPoints(accounts []accountClanGames) {
	sort.Slice(accounts[:], func(i, j int) bool {
		return accounts[i].points > accounts[j].points
	})
}

func (action *clanGamesRank) groupAccountsClanGamePointsByOwner(accounts []accountClanGames) []playerClanGames {
	owners := make(map[string]playerClanGames)
	for _, account := range accounts {
		owner, _ := action.config.AccountOwner(account.tag)
		updatedPlayerClanGames := playerClanGames{
			name:     owner,
			points:   account.points,
			accounts: []accountClanGames{account},
		}
		if playerInfo, ok := owners[owner]; ok {
			updatedPlayerClanGames.points += playerInfo.points
			updatedPlayerClanGames.accounts = append(playerInfo.accounts, account)
		}
		owners[owner] = updatedPlayerClanGames
	}

	var players []playerClanGames
	for _, ownerInfo := range owners {
		players = append(players, ownerInfo)
	}
	return players
}

func (action *clanGamesRank) handleClanGamesRank(ctx context.Context, previous, current []clash.Player) error {
	accounts := allAccountsClanGamesPoints(previous, current)
	players := action.groupAccountsClanGamePointsByOwner(accounts)

	sortPlayersByClanGamesPoints(players)

	shouldShowAccounts := utils.BoolFromContext(ctx, detailKey)
	message := generateRankMessage(players, shouldShowAccounts, action.utils.Started(), action.utils.Ended(), action.remainingTimeString())
	_, err := action.bot.SendMessage(ctx, message)
	return err
}

func (action *clanGamesRank) rankCommand(ctx context.Context, args ...string) error {
	if len(args) == 0 {
		ctx = context.WithValue(ctx, detailKey, false)
		return action.trigger(ctx)
	}
	if len(args) == 1 && (args[0] == "detail" || args[0] == "d") {
		ctx = context.WithValue(ctx, detailKey, true)
		return action.trigger(ctx)
	}
	_, err := action.bot.SendMessage(ctx, "fala direito fdp")
	return err
}
