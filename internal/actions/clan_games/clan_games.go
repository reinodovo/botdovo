package clan_games

import (
	"time"

	"github.com/aptible/supercronic/cronexpr"
	"gitlab.com/reinodovo/botdovo/internal/bot"
	"gitlab.com/reinodovo/botdovo/internal/clash"
	"gitlab.com/reinodovo/botdovo/internal/configuration_manager"
	"gitlab.com/reinodovo/botdovo/internal/information_manager"
	"gitlab.com/reinodovo/botdovo/internal/utils"
)

const (
	clanGamesAchievement     = "Games Champion"
	maxPoints            int = 4000
)

func SetupClanGames(bot bot.Bot, config configuration_manager.ConfigurationManager, mgr information_manager.InformationManager) error {
	if err := setupClanGamesUpdates(bot, mgr); err != nil {
		return err
	}
	if err := setupClanGamesRank(bot, config, mgr, Utils{}); err != nil {
		return err
	}
	return nil
}

type accountClanGames struct {
	name   string
	tag    string
	points int
}

type playerClanGames struct {
	name     string
	points   int
	accounts []accountClanGames
}

func accountClanGamesPoints(player clash.Player) int {
	for _, achievement := range player.Achievements {
		if achievement.Name == clanGamesAchievement {
			return int(achievement.Value)
		}
	}
	return 0
}

func allAccountsClanGamesPoints(previous, current []clash.Player) []accountClanGames {
	previousByTag := make(map[string]clash.Player)
	for _, player := range previous {
		previousByTag[player.Tag] = player
	}

	var accounts []accountClanGames
	for _, player := range current {
		oldPoints := 0
		if previousPlayer, ok := previousByTag[player.Tag]; ok {
			oldPoints = accountClanGamesPoints(previousPlayer)
		}
		currentPoints := accountClanGamesPoints(player)
		var points = utils.Min(currentPoints-oldPoints, maxPoints)
		if points == 0 {
			continue
		}
		accounts = append(accounts, accountClanGames{name: player.Name, tag: player.Tag, points: points})
	}
	return accounts
}

type ClanGamesUtils interface {
	Started() bool
	Ended() bool
	RemainingTime() time.Duration
}

type Utils struct{}

const (
	monthStartCronExpression = "0 0 1 * *"
	startTimeCronExpression  = "0 8 22 * *"
	endTimeCronExpression    = "0 8 28 * *"
	durationHours            = 6 * 24
)

func (u Utils) Started() bool {
	clanGamesStart := cronexpr.MustParse(startTimeCronExpression).Next(time.Now().UTC())
	nextMonthStart := cronexpr.MustParse(monthStartCronExpression).Next(time.Now().UTC())
	return clanGamesStart.After(nextMonthStart)
}

func (u Utils) Ended() bool {
	clanGamesEnd := cronexpr.MustParse(endTimeCronExpression).Next(time.Now().UTC())
	nextMonthStart := cronexpr.MustParse(monthStartCronExpression).Next(time.Now().UTC())
	return clanGamesEnd.After(nextMonthStart)
}

func (u Utils) RemainingTime() time.Duration {
	clanGamesEnd := cronexpr.MustParse(endTimeCronExpression).Next(time.Now().UTC())
	return clanGamesEnd.Sub(time.Now().UTC())
}
