package clan_games

import (
	"context"
	"reflect"
	"sort"
	"testing"

	"github.com/stretchr/testify/mock"
	"gitlab.com/reinodovo/botdovo/internal/clash"
	"gitlab.com/reinodovo/botdovo/internal/mocks"
)

func TestGenerateAccountClanGamesUpdateMessage(t *testing.T) {
	// setup
	account := accountClanGames{
		name:   "player1",
		tag:    "tag1",
		points: 150,
	}
	expectedMessage := "player1 fez <b>150</b> pontos no clan games, obrigado pelo seu sacrifício."
	// act
	message := generateAccountClanGamesUpdateMessage(account)
	// test
	if message != expectedMessage {
		t.Error("message and expectedMessage did not match")
	}
}

func TestHandleAccountClanGamesUpdate(t *testing.T) {
	// setup
	mockConfigurationManager := &mocks.ConfigurationManager{}
	mockConfigurationManager.On("AccountOwner", mock.MatchedBy(func(tag string) bool {
		return tag == "tag1"
	})).Return("player1", nil)
	mockConfigurationManager.On("AccountOwner", mock.MatchedBy(func(tag string) bool {
		return tag == "tag2" || tag == "tag3"
	})).Return("player2", nil)

	previous := []clash.Player{
		{Name: "account1", Tag: "tag1", Achievements: []clash.Achievement{{Name: clanGamesAchievement, Value: 50000}}},
		{Name: "account2", Tag: "tag2", Achievements: []clash.Achievement{{Name: clanGamesAchievement, Value: 5000}}},
		{Name: "account3", Tag: "tag3", Achievements: []clash.Achievement{{Name: clanGamesAchievement, Value: 45000}}},
	}
	current := []clash.Player{
		{Name: "account1", Tag: "tag1", Achievements: []clash.Achievement{{Name: clanGamesAchievement, Value: 51000}}},
		{Name: "account2", Tag: "tag2", Achievements: []clash.Achievement{{Name: clanGamesAchievement, Value: 5000}}},
		{Name: "account3", Tag: "tag3", Achievements: []clash.Achievement{{Name: clanGamesAchievement, Value: 45350}}},
	}
	expectedMessages := []string{
		"account1 fez <b>1000</b> pontos no clan games, obrigado pelo seu sacrifício.",
		"account3 fez <b>350</b> pontos no clan games, obrigado pelo seu sacrifício.",
	}

	var messages []string
	mockBot := &mocks.Bot{}
	mockBot.On("SendMessage", mock.Anything, mock.MatchedBy(func(message string) bool {
		messages = append(messages, message)
		return true
	})).Return(0, nil)

	clanGamesUpdates := clanGamesUpdates{bot: mockBot}

	// act
	clanGamesUpdates.handleAccountClanGamesUpdate(context.Background(), previous, current)

	// test
	sort.Strings(messages)
	sort.Strings(expectedMessages)
	if !reflect.DeepEqual(messages, expectedMessages) {
		t.Errorf("expected %v, got %v", expectedMessages, messages)
	}
}
