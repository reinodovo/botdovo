package clan_games

import (
	"context"
	"reflect"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/mock"
	"gitlab.com/reinodovo/botdovo/internal/bot"
	"gitlab.com/reinodovo/botdovo/internal/clash"
	"gitlab.com/reinodovo/botdovo/internal/information_manager"
	"gitlab.com/reinodovo/botdovo/internal/mocks"
)

func TestGenerateRankMessage(t *testing.T) {
	// setup
	tests := []struct {
		players            []playerClanGames
		shouldShowAccounts bool
		started            bool
		ended              bool
		remainingTime      string
		expectedMessage    string
	}{
		{
			players: []playerClanGames{
				{
					name:   "player1",
					points: 1000,
					accounts: []accountClanGames{
						{name: "account1", tag: "tag1", points: 1000},
					},
				},
				{
					name:   "player2",
					points: 500,
					accounts: []accountClanGames{
						{name: "account2", tag: "tag2", points: 150},
						{name: "account3", tag: "tag3", points: 350},
					},
				},
			},
			shouldShowAccounts: false,
			started:            true,
			ended:              false,
			remainingTime:      "3d3h",
			expectedMessage: `<b>Clan Games</b> - <b>1500</b> pontos
Tempo Restante: <b>3d3h</b>

<b>1.</b> player1 - <b>1000</b>
<b>2.</b> player2 - <b>500</b>`,
		},
		{
			players: []playerClanGames{
				{
					name:   "player1",
					points: 1000,
					accounts: []accountClanGames{
						{name: "account1", tag: "tag1", points: 1000},
					},
				},
				{
					name:   "player2",
					points: 500,
					accounts: []accountClanGames{
						{name: "account2", tag: "tag2", points: 150},
						{name: "account3", tag: "tag3", points: 350},
					},
				},
			},
			shouldShowAccounts: true,
			started:            true,
			ended:              false,
			remainingTime:      "3d3h",
			expectedMessage: `<b>Clan Games</b> - <b>1500</b> pontos
Tempo Restante: <b>3d3h</b>

<b>1.</b> player1 - <b>1000</b>
        account1 - <b>1000</b>
<b>2.</b> player2 - <b>500</b>
        account3 - <b>350</b>
        account2 - <b>150</b>`,
		},
		{
			players:            []playerClanGames{},
			shouldShowAccounts: false,
			started:            true,
			ended:              false,
			remainingTime:      "3d3h",
			expectedMessage: `<b>Clan Games</b> - <b>0</b> pontos
Tempo Restante: <b>3d3h</b>`,
		},
		{
			players:            []playerClanGames{},
			shouldShowAccounts: true,
			started:            true,
			ended:              false,
			remainingTime:      "3d3h",
			expectedMessage: `<b>Clan Games</b> - <b>0</b> pontos
Tempo Restante: <b>3d3h</b>`,
		},
		{
			players:            []playerClanGames{},
			shouldShowAccounts: false,
			started:            false,
			ended:              false,
			remainingTime:      "3d3h",
			expectedMessage:    "Não enche meu saco",
		},
		{
			players:            []playerClanGames{},
			shouldShowAccounts: false,
			started:            true,
			ended:              true,
			remainingTime:      "3d3h",
			expectedMessage: `<b>Clan Games</b> - <b>0</b> pontos
Acabou a doença`,
		},
	}
	for idx, test := range tests {
		// act
		message := generateRankMessage(test.players, test.shouldShowAccounts, test.started, test.ended, test.remainingTime)
		// test
		if strings.TrimSpace(message) != strings.TrimSpace(test.expectedMessage) {
			t.Errorf("expected %v, got %v on test %v", test.expectedMessage, message, idx)
		}
	}
}

func TestSortPlayersByClanGamesPoints(t *testing.T) {
	// setup
	player1 := playerClanGames{
		name:   "player1",
		points: 1000,
		accounts: []accountClanGames{
			{name: "account1", tag: "tag1", points: 1000},
		},
	}
	player2 := playerClanGames{
		name:   "player2",
		points: 500,
		accounts: []accountClanGames{
			{name: "account2", tag: "tag2", points: 150},
			{name: "account3", tag: "tag3", points: 350},
		},
	}
	expectedPlayers := []playerClanGames{player1, player2}
	// act
	players := []playerClanGames{player2, player1}
	sortPlayersByClanGamesPoints(players)
	// test
	if !reflect.DeepEqual(players, expectedPlayers) {
		t.Errorf("expected %v, got %v", expectedPlayers, players)
	}
}

func TestSortAccountsByClanGamesPoints(t *testing.T) {
	// setup
	account1 := accountClanGames{name: "account1", tag: "tag1", points: 1000}
	account2 := accountClanGames{name: "account2", tag: "tag2", points: 150}
	account3 := accountClanGames{name: "account3", tag: "tag3", points: 350}
	expectedAccounts := []accountClanGames{account1, account3, account2}
	// act
	accounts := []accountClanGames{account2, account3, account1}
	sortAccountsByClanGamesPoints(accounts)
	// test
	if !reflect.DeepEqual(accounts, expectedAccounts) {
		t.Errorf("expected %v, got %v", expectedAccounts, accounts)
	}
}

func TestGroupAccountsClanGamePointsByOwner(t *testing.T) {
	// setup
	mockConfigurationManager := &mocks.ConfigurationManager{}
	mockConfigurationManager.On("AccountOwner", mock.MatchedBy(func(tag string) bool {
		return tag == "tag1"
	})).Return("player1", nil)
	mockConfigurationManager.On("AccountOwner", mock.MatchedBy(func(tag string) bool {
		return tag == "tag2" || tag == "tag3"
	})).Return("player2", nil)
	clanGames := clanGamesRank{config: mockConfigurationManager}

	account1 := accountClanGames{name: "account1", tag: "tag1", points: 1000}
	account2 := accountClanGames{name: "account2", tag: "tag2", points: 150}
	account3 := accountClanGames{name: "account3", tag: "tag3", points: 350}
	accounts := []accountClanGames{account1, account2, account3}
	player1 := playerClanGames{
		name:     "player1",
		points:   1000,
		accounts: []accountClanGames{account1},
	}
	player2 := playerClanGames{
		name:     "player2",
		points:   500,
		accounts: []accountClanGames{account2, account3},
	}
	expectedPlayers := []playerClanGames{player1, player2}
	// act
	players := clanGames.groupAccountsClanGamePointsByOwner(accounts)
	sortPlayersByClanGamesPoints(players)
	// test
	if !reflect.DeepEqual(players, expectedPlayers) {
		t.Errorf("expected %v, got %v", expectedPlayers, players)
	}
}

func TestRankCommand(t *testing.T) {
	// setup
	mockConfigurationManager := &mocks.ConfigurationManager{}
	mockConfigurationManager.On("AccountOwner", mock.MatchedBy(func(tag string) bool {
		return tag == "tag1"
	})).Return("player1", nil)
	mockConfigurationManager.On("AccountOwner", mock.MatchedBy(func(tag string) bool {
		return tag == "tag2" || tag == "tag3"
	})).Return("player2", nil)

	previous := []clash.Player{
		{Name: "account1", Tag: "tag1", Achievements: []clash.Achievement{{Name: clanGamesAchievement, Value: 50000}}},
		{Name: "account2", Tag: "tag2", Achievements: []clash.Achievement{{Name: clanGamesAchievement, Value: 5000}}},
		{Name: "account3", Tag: "tag3", Achievements: []clash.Achievement{{Name: clanGamesAchievement, Value: 45000}}},
	}
	current := []clash.Player{
		{Name: "account1", Tag: "tag1", Achievements: []clash.Achievement{{Name: clanGamesAchievement, Value: 51000}}},
		{Name: "account2", Tag: "tag2", Achievements: []clash.Achievement{{Name: clanGamesAchievement, Value: 5150}}},
		{Name: "account3", Tag: "tag3", Achievements: []clash.Achievement{{Name: clanGamesAchievement, Value: 45350}}},
	}

	mockInformationManager := &mocks.InformationManager{}
	mockInformationManager.On("GenerateTrigger", mock.Anything, mock.Anything, mock.Anything).Return(
		func(data information_manager.Data, interval information_manager.Interval, callback interface{}) func(ctx context.Context) error {
			if callback, ok := callback.(func(ctx context.Context, a, b []clash.Player) error); ok {
				return func(ctx context.Context) error {
					callback(ctx, previous, current)
					return nil
				}
			} else {
				return func(ctx context.Context) error {
					return nil
				}
			}
		},
		nil,
	)

	tests := []struct {
		args            []string
		expectedMessage string
	}{
		{
			args: []string{},
			expectedMessage: `<b>Clan Games</b> - <b>1500</b> pontos
Tempo Restante: <b>3d3h30m10s</b>

<b>1.</b> player1 - <b>1000</b>
<b>2.</b> player2 - <b>500</b>`,
		},
		{
			args: []string{"d"},
			expectedMessage: `<b>Clan Games</b> - <b>1500</b> pontos
Tempo Restante: <b>3d3h30m10s</b>

<b>1.</b> player1 - <b>1000</b>
        account1 - <b>1000</b>
<b>2.</b> player2 - <b>500</b>
        account3 - <b>350</b>
        account2 - <b>150</b>`,
		},
		{
			args: []string{"detail"},
			expectedMessage: `<b>Clan Games</b> - <b>1500</b> pontos
Tempo Restante: <b>3d3h30m10s</b>

<b>1.</b> player1 - <b>1000</b>
        account1 - <b>1000</b>
<b>2.</b> player2 - <b>500</b>
        account3 - <b>350</b>
        account2 - <b>150</b>`,
		},
		{
			args:            []string{"d", "t"},
			expectedMessage: "fala direito fdp",
		},
		{
			args:            []string{"t"},
			expectedMessage: "fala direito fdp",
		},
	}
	for idx, test := range tests {
		var rankCommand bot.Command

		mockBot := &mocks.Bot{}
		mockBot.On("SetCommand", mock.Anything, "clan_games", mock.MatchedBy(func(command bot.Command) bool {
			rankCommand = command
			return true
		}), mock.Anything).Return(nil)
		mockBot.On("SendMessage", mock.Anything, mock.MatchedBy(func(message string) bool {
			// test
			if strings.TrimSpace(message) != strings.TrimSpace(test.expectedMessage) {
				t.Errorf("expected %v, got %v for test %v", test.expectedMessage, message, idx)
			}
			return true
		})).Return(0, nil)

		mockUtils := &mocks.ClanGamesUtils{}
		mockUtils.On("Started").Return(true)
		mockUtils.On("Ended").Return(false)
		mockUtils.On("RemainingTime").Return(75*time.Hour + 30*time.Minute + 10*time.Second)

		setupClanGamesRank(mockBot, mockConfigurationManager, mockInformationManager, mockUtils)

		// act
		rankCommand(context.Background(), test.args...)
	}
}
