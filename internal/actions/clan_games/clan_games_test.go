package clan_games

import (
	"reflect"
	"testing"

	"gitlab.com/reinodovo/botdovo/internal/clash"
)

func TestAccountClanGamesPoints(t *testing.T) {
	// setup
	tests := []struct {
		player         clash.Player
		expectedPoints int64
	}{
		{
			player: clash.Player{
				Name: "player1",
				Tag:  "tag1",
				Achievements: []clash.Achievement{
					{Name: "Games Champion", Value: 3000},
				},
			},
			expectedPoints: 3000,
		},
		{
			player: clash.Player{
				Name: "player1",
				Tag:  "tag1",
				Achievements: []clash.Achievement{
					{Name: "Something Else", Value: 3000},
				},
			},
			expectedPoints: 0,
		},
		{
			player:         clash.Player{Name: "player1", Tag: "tag1"},
			expectedPoints: 0,
		},
	}
	for idx, test := range tests {
		// act
		points := accountClanGamesPoints(test.player)
		// test
		if points != int(test.expectedPoints) {
			t.Errorf("points and expectedPoints did not match for test %v", idx)
		}
	}
}

func TestAllAccountsClanGamesPoints(t *testing.T) {
	// setup
	previous := []clash.Player{
		{
			Name:         "player1",
			Tag:          "tag1",
			Achievements: []clash.Achievement{{Name: clanGamesAchievement, Value: 3000}},
		},
		{
			Name:         "player2",
			Tag:          "tag2",
			Achievements: []clash.Achievement{{Name: clanGamesAchievement, Value: 3000}},
		},
		{
			Name:         "player3",
			Tag:          "tag3",
			Achievements: []clash.Achievement{{Name: clanGamesAchievement, Value: 3000}},
		},
	}
	current := []clash.Player{
		{
			Name:         "player1",
			Tag:          "tag1",
			Achievements: []clash.Achievement{{Name: clanGamesAchievement, Value: 3500}},
		},
		{
			Name:         "player2",
			Tag:          "tag2",
			Achievements: []clash.Achievement{{Name: clanGamesAchievement, Value: 3000}},
		},
		{
			Name:         "player3",
			Tag:          "tag3",
			Achievements: []clash.Achievement{{Name: clanGamesAchievement, Value: 13000}},
		},
	}
	expectedAccounts := []accountClanGames{
		{name: "player1", tag: "tag1", points: 500},
		{name: "player3", tag: "tag3", points: 4000},
	}
	// act
	accounts := allAccountsClanGamesPoints(previous, current)
	// test
	if !reflect.DeepEqual(accounts, expectedAccounts) {
		t.Error("accounts and expectedAccounts did not match")
	}
}
