package clan_games

import (
	"context"
	"fmt"

	"gitlab.com/reinodovo/botdovo/internal/bot"
	"gitlab.com/reinodovo/botdovo/internal/clash"
	"gitlab.com/reinodovo/botdovo/internal/information_manager"
	"gitlab.com/reinodovo/botdovo/internal/utils"
	"go.opentelemetry.io/otel/trace"
)

type clanGamesUpdates struct {
	bot bot.Bot
}

func setupClanGamesUpdates(bot bot.Bot, mgr information_manager.InformationManager) error {
	clanGamesUpdates := clanGamesUpdates{bot: bot}
	_, err := mgr.Subscribe(information_manager.Player, information_manager.Minute, clanGamesUpdates.handleAccountClanGamesUpdate)
	return err
}

func generateAccountClanGamesUpdateMessage(account accountClanGames) string {
	return fmt.Sprintf("%v fez <b>%v</b> pontos no clan games, obrigado pelo seu sacrifício.", account.name, account.points)
}

func (action *clanGamesUpdates) handleAccountClanGamesUpdate(ctx context.Context, previous, current []clash.Player) error {
	accounts := allAccountsClanGamesPoints(previous, current)
	for _, account := range accounts {
		message := generateAccountClanGamesUpdateMessage(account)
		_, err := action.bot.SendMessage(ctx, message)

		span := trace.SpanFromContext(ctx)
		utils.RecordErrorIfNotNil(span, err)
	}
	return nil
}
