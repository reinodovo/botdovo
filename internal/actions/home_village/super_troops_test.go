package home_village

import (
	"context"
	"strings"
	"testing"

	"github.com/stretchr/testify/mock"
	"gitlab.com/reinodovo/botdovo/internal/bot"
	"gitlab.com/reinodovo/botdovo/internal/clash"
	"gitlab.com/reinodovo/botdovo/internal/mocks"
)

func compareActiveSuperTroops(a, b []activeSuperTroop) bool {
	if len(a) != len(b) {
		return false
	}
	for idx, activeA := range a {
		activeB := b[idx]
		if activeA.player.Tag != activeB.player.Tag {
			return false
		}
		if activeA.troop != activeB.troop {
			return false
		}
	}
	return true
}

func TestSuperTroopsGenerateMessage(t *testing.T) {
	// setup
	tests := []struct {
		activeSuperTroops []activeSuperTroop
		expectedMessage   string
	}{
		{
			activeSuperTroops: []activeSuperTroop{
				{
					player: clash.Player{Name: "player1"},
					troop:  clash.Army{Name: "troop1"},
				},
				{
					player: clash.Player{Name: "player2"},
					troop:  clash.Army{Name: "troop2"},
				},
			},
			expectedMessage: `player1 - troop1
player2 - troop2`,
		},
		{
			activeSuperTroops: []activeSuperTroop{},
			expectedMessage:   "Nenhuma super troop ativada",
		},
	}
	action := SuperTroops{}
	for idx, test := range tests {
		// act
		message := action.generateMessage(test.activeSuperTroops)
		// test
		if strings.TrimSpace(message) != strings.TrimSpace(test.expectedMessage) {
			t.Errorf("message and expectedMessage did not match for test %v", idx)
		}
	}
}

func TestActiveSuperTroops(t *testing.T) {
	// setup
	players := []clash.Player{
		{
			Tag: "tag1",
			Troops: []clash.Army{
				{Name: "Troop1", SuperTroopIsActive: false},
				{Name: "Troop2", SuperTroopIsActive: true},
			},
		},
		{
			Tag: "tag2",
			Troops: []clash.Army{
				{Name: "Troop1", SuperTroopIsActive: false},
				{Name: "Troop2", SuperTroopIsActive: false},
			},
		},
		{
			Tag: "tag3",
			Troops: []clash.Army{
				{Name: "Troop1", SuperTroopIsActive: true},
				{Name: "Troop2", SuperTroopIsActive: true},
			},
		},
	}
	expectedActiveSuperTroops := []activeSuperTroop{
		{
			player: players[0],
			troop:  clash.Army{Name: "Troop2", SuperTroopIsActive: true},
		},
		{
			player: players[2],
			troop:  clash.Army{Name: "Troop1", SuperTroopIsActive: true},
		},
		{
			player: players[2],
			troop:  clash.Army{Name: "Troop2", SuperTroopIsActive: true},
		},
	}
	action := SuperTroops{}
	// act
	activeSuperTroops := action.activeSuperTroops(players)
	// test
	if !compareActiveSuperTroops(activeSuperTroops, expectedActiveSuperTroops) {
		t.Fatalf("expected %v, got %v", expectedActiveSuperTroops, activeSuperTroops)
	}
}

func TestSuperTroopsCommand(t *testing.T) {
	// setup
	var superTroopsCommand bot.Command

	mockBot := &mocks.Bot{}
	mockBot.On("SetCommand", mock.Anything, "super_troops", mock.MatchedBy(func(command bot.Command) bool {
		superTroopsCommand = command
		return true
	}), mock.Anything).Return(nil)

	mockClashAPI := &mocks.ClashAPI{}
	mockClashAPI.On("MainClanMembers", mock.Anything).Return([]clash.Player{
		{
			Tag:  "tag1",
			Name: "player1",
			Troops: []clash.Army{
				{Name: "Troop1", SuperTroopIsActive: false},
				{Name: "Troop2", SuperTroopIsActive: true},
			},
		},
		{
			Tag:  "tag2",
			Name: "player2",
			Troops: []clash.Army{
				{Name: "Troop1", SuperTroopIsActive: false},
				{Name: "Troop2", SuperTroopIsActive: false},
			},
		},
		{
			Tag:  "tag3",
			Name: "player3",
			Troops: []clash.Army{
				{Name: "Troop1", SuperTroopIsActive: true},
				{Name: "Troop2", SuperTroopIsActive: true},
			},
		},
	}, nil)

	expectedMessage := `player1 - Troop2
player3 - Troop1
player3 - Troop2`

	setupSuperTroops(mockBot, mockClashAPI)

	mockBot.On("SendMessage", mock.Anything, mock.MatchedBy(func(message string) bool {
		// test
		if strings.TrimSpace(message) != strings.TrimSpace(expectedMessage) {
			t.Errorf("expected %v, got %v", expectedMessage, message)
		}
		return true
	})).Return(0, nil)

	// act
	superTroopsCommand(context.Background())
}
