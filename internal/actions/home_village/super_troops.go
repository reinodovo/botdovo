package home_village

import (
	"bytes"
	"context"
	"fmt"

	"gitlab.com/reinodovo/botdovo/internal/bot"
	"gitlab.com/reinodovo/botdovo/internal/clash"
)

type SuperTroops struct {
	bot      bot.Bot
	clashAPI clash.ClashAPI
}

func setupSuperTroops(bot bot.Bot, clashAPI clash.ClashAPI) error {
	superTroops := &SuperTroops{
		bot:      bot,
		clashAPI: clashAPI,
	}
	superTroopsExplanation := "mostra todas as super tropas desbloqueadas"
	return bot.SetCommand(context.Background(), "super_troops", superTroops.Do, superTroopsExplanation)
}

type activeSuperTroop struct {
	player clash.Player
	troop  clash.Army
}

func (action *SuperTroops) generateMessage(activeSuperTroops []activeSuperTroop) string {
	if len(activeSuperTroops) == 0 {
		return "Nenhuma super troop ativada"
	}

	var buffer bytes.Buffer
	for _, activeSuperTroop := range activeSuperTroops {
		buffer.WriteString(fmt.Sprintf("%v - %v\n", activeSuperTroop.player.Name, activeSuperTroop.troop.Name))
	}
	return buffer.String()
}

func (action *SuperTroops) activeSuperTroops(players []clash.Player) []activeSuperTroop {
	var activeSuperTroops []activeSuperTroop
	for _, player := range players {
		for _, troop := range player.Troops {
			if troop.SuperTroopIsActive {
				activeSuperTroops = append(activeSuperTroops, activeSuperTroop{
					player: player,
					troop:  troop,
				})
			}
		}
	}
	return activeSuperTroops
}

func (action *SuperTroops) Do(ctx context.Context, args ...string) error {
	players, err := action.clashAPI.MainClanMembers(ctx)
	if err != nil {
		return err
	}

	activeSuperTroops := action.activeSuperTroops(players)

	message := action.generateMessage(activeSuperTroops)
	_, err = action.bot.SendMessage(ctx, message)
	return err
}
