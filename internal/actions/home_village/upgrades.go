package home_village

import (
	"bytes"
	"context"
	"fmt"
	"sort"

	"gitlab.com/reinodovo/botdovo/internal/bot"
	"gitlab.com/reinodovo/botdovo/internal/clash"
	"gitlab.com/reinodovo/botdovo/internal/information_manager"
	"gitlab.com/reinodovo/botdovo/internal/utils"
)

const village string = "home"

type Upgrades struct {
	bot          bot.Bot
	mgr          information_manager.InformationManager
	dayTrigger   func(ctx context.Context) error
	monthTrigger func(ctx context.Context) error
}

func setupUpgrades(bot bot.Bot, mgr information_manager.InformationManager) error {
	upgrades := &Upgrades{
		bot: bot,
		mgr: mgr,
	}
	upgradesExplanation := "upgrades do dia (month, m - upgrades do mês)"
	if err := bot.SetCommand(context.Background(), "upgrades", upgrades.Do, upgradesExplanation); err != nil {
		return err
	}
	var err error
	if upgrades.dayTrigger, err = mgr.Subscribe(information_manager.Player, information_manager.Day, upgrades.handleDailyUpgrades); err != nil {
		return err
	}
	if upgrades.monthTrigger, err = mgr.Subscribe(information_manager.Player, information_manager.Month, upgrades.handleMonthlyUpgrades); err != nil {
		return err
	}
	return nil
}

type upgrade struct {
	name     string
	oldLevel int64
	newLevel int64
}

type playerUpgrades struct {
	player   clash.Player
	upgrades []upgrade
}

func (action *Upgrades) generatePlayerMessage(player clash.Player, upgrades []upgrade) string {
	if len(upgrades) == 0 {
		return ""
	}
	var buffer bytes.Buffer
	buffer.WriteString(fmt.Sprintf("<b>%v</b>\n", player.Name))
	for _, upgrade := range upgrades {
		buffer.WriteString(fmt.Sprintf("%v %v -> %v\n", upgrade.name, upgrade.oldLevel, upgrade.newLevel))
	}
	return buffer.String()
}

func (action *Upgrades) armyUpgrades(old, current []clash.Army) []upgrade {
	var diff []upgrade
	for _, army := range current {
		if army.Village != village {
			continue
		}
		armyUpgrade := upgrade{
			name:     army.Name,
			oldLevel: 0,
			newLevel: army.Level,
		}
		for _, oldArmy := range old {
			if oldArmy.Village != village {
				continue
			}
			if oldArmy.Name == army.Name {
				armyUpgrade.oldLevel = oldArmy.Level
			}
		}
		if armyUpgrade.oldLevel < armyUpgrade.newLevel {
			diff = append(diff, armyUpgrade)
		}
	}
	return diff
}

func (action *Upgrades) getPlayerUpgrades(old, current clash.Player) []upgrade {
	var upgrades []upgrade
	if current.TownHallLevel > old.TownHallLevel {
		upgrades = append(upgrades, upgrade{
			name:     "Town Hall",
			oldLevel: old.TownHallLevel,
			newLevel: current.TownHallLevel,
		})
	}
	if current.TownHallWeaponLevel > old.TownHallWeaponLevel {
		upgrades = append(upgrades, upgrade{
			name:     "Town Hall Weapon",
			oldLevel: old.TownHallWeaponLevel,
			newLevel: current.TownHallWeaponLevel,
		})
	}
	heroUpgrades := action.armyUpgrades(old.Heroes, current.Heroes)
	upgrades = append(upgrades, heroUpgrades...)
	troopUpgrades := action.armyUpgrades(old.Troops, current.Troops)
	upgrades = append(upgrades, troopUpgrades...)
	spellUpgrades := action.armyUpgrades(old.Spells, current.Spells)
	upgrades = append(upgrades, spellUpgrades...)
	return upgrades
}

func (action *Upgrades) generateMessage(allPlayersUpgrades []playerUpgrades, interval information_manager.Interval) string {
	var buffer bytes.Buffer
	if interval == information_manager.Day {
		buffer.WriteString("<b>Upgrades do Dia</b>\n")
	} else {
		buffer.WriteString("<b>Upgrades do Mês</b>\n")
	}
	if len(allPlayersUpgrades) == 0 {
		buffer.WriteString("Nada uppado")
		return buffer.String()
	}
	for _, playerInfo := range allPlayersUpgrades {
		message := action.generatePlayerMessage(playerInfo.player, playerInfo.upgrades)
		if len(message) != 0 {
			buffer.WriteString(message)
			buffer.WriteString("\n")
		}
	}
	return buffer.String()
}

func (action *Upgrades) getAllPlayersUpgrades(previous, current []clash.Player) []playerUpgrades {
	var allPlayersUpgrades []playerUpgrades
	previousByTag := utils.PlayersByTag(previous)
	for _, player := range current {
		var (
			previousPlayer clash.Player
			ok             bool
		)
		if previousPlayer, ok = previousByTag[player.Tag]; !ok {
			previousPlayer = clash.Player{}
		}
		upgrades := action.getPlayerUpgrades(previousPlayer, player)
		if len(upgrades) == 0 {
			continue
		}
		allPlayersUpgrades = append(allPlayersUpgrades, playerUpgrades{
			player:   player,
			upgrades: upgrades,
		})
	}
	return allPlayersUpgrades
}

func (action *Upgrades) Do(ctx context.Context, args ...string) error {
	if len(args) == 0 {
		return action.dayTrigger(ctx)
	}
	if len(args) == 1 && (args[0] == "month" || args[0] == "m") {
		return action.monthTrigger(ctx)
	}
	_, err := action.bot.SendMessage(ctx, "fala direito fdp")
	return err
}

func (action *Upgrades) handleDailyUpgrades(ctx context.Context, previous, current []clash.Player) error {
	return action.handleUpgrades(ctx, previous, current, information_manager.Day)
}

func (action *Upgrades) handleMonthlyUpgrades(ctx context.Context, previous, current []clash.Player) error {
	return action.handleUpgrades(ctx, previous, current, information_manager.Month)
}

func (action *Upgrades) sortPlayersUpgrades(playersUpgrades []playerUpgrades) []playerUpgrades {
	sort.Slice(playersUpgrades[:], func(i, j int) bool {
		if playersUpgrades[i].player.TownHallLevel > playersUpgrades[j].player.TownHallLevel {
			return true
		} else if playersUpgrades[i].player.TownHallLevel == playersUpgrades[j].player.TownHallLevel {
			if playersUpgrades[i].player.TownHallWeaponLevel > playersUpgrades[j].player.TownHallWeaponLevel {
				return true
			} else if playersUpgrades[i].player.TownHallWeaponLevel == playersUpgrades[j].player.TownHallWeaponLevel {
				if playersUpgrades[i].player.ExpLevel > playersUpgrades[j].player.ExpLevel {
					return true
				}
			}
		}
		return false
	})
	return playersUpgrades
}

func (action *Upgrades) handleUpgrades(ctx context.Context, previous, current []clash.Player, interval information_manager.Interval) error {
	allPlayersUpgrades := action.getAllPlayersUpgrades(previous, current)
	allPlayersUpgrades = action.sortPlayersUpgrades(allPlayersUpgrades)
	message := action.generateMessage(allPlayersUpgrades, interval)
	_, err := action.bot.SendMessage(ctx, message)
	return err
}
