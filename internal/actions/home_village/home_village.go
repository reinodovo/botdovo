package home_village

import (
	"gitlab.com/reinodovo/botdovo/internal/bot"
	"gitlab.com/reinodovo/botdovo/internal/clash"
	"gitlab.com/reinodovo/botdovo/internal/configuration_manager"
	"gitlab.com/reinodovo/botdovo/internal/information_manager"
)

func SetupHomeVillage(bot bot.Bot, clashAPI clash.ClashAPI, mgr information_manager.InformationManager, config configuration_manager.ConfigurationManager) error {
	if err := setupFarm(bot, mgr, config); err != nil {
		return err
	}
	if err := setupUpgrades(bot, mgr); err != nil {
		return err
	}
	if err := setupSuperTroops(bot, clashAPI); err != nil {
		return err
	}
	return nil
}
