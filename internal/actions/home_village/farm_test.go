package home_village

import (
	"context"
	"strings"
	"testing"

	"github.com/stretchr/testify/mock"
	"gitlab.com/reinodovo/botdovo/internal/bot"
	"gitlab.com/reinodovo/botdovo/internal/clash"
	"gitlab.com/reinodovo/botdovo/internal/information_manager"
	"gitlab.com/reinodovo/botdovo/internal/mocks"
	"gitlab.com/reinodovo/botdovo/internal/utils"
)

func generateFarmTestPlayer(tag, name string, gold, elixir, darkElixir int64) clash.Player {
	return clash.Player{
		Tag:  tag,
		Name: name,
		Achievements: []clash.Achievement{
			{Name: goldAchievement, Value: gold},
			{Name: elixirAchievement, Value: elixir},
			{Name: darkElixirAchievement, Value: darkElixir},
		},
	}
}

func TestFarmCreateResourceRankMessage(t *testing.T) {
	// setup
	tests := []struct {
		resourceName    string
		playersResource []utils.OwnerProgress[Resource]
		expectedMessage string
	}{
		{
			resourceName: "Gold",
			playersResource: []utils.OwnerProgress[Resource]{
				{
					Owner: "owner1",
					Players: []utils.PlayerProgress[Resource]{
						{Current: clash.Player{Name: "player1"}, Data: 1000000},
					},
				},
				{
					Owner: "owner2",
					Players: []utils.PlayerProgress[Resource]{
						{Current: clash.Player{Name: "player2"}, Data: 1000},
						{Current: clash.Player{Name: "player3"}, Data: 100},
					},
				},
			},
			expectedMessage: `Farmadores de gold
<b>1.</b> owner1 <b>1,000,000</b>
        player1 <b>1,000,000</b>
<b>2.</b> owner2 <b>1,100</b>
        player2 <b>1,000</b>
        player3 <b>100</b>
`,
		},
		{
			resourceName:    "Gold",
			playersResource: []utils.OwnerProgress[Resource]{},
			expectedMessage: "Ninguém farmou gold\n",
		},
	}
	action := Farm{}
	for idx, test := range tests {
		// act
		message := action.createResourceRankMessage(test.resourceName, test.playersResource)
		// test
		if message != test.expectedMessage {
			t.Errorf("expected %v, got %v for test %v", test.expectedMessage, message, idx)
		}
	}
}

func TestFarmGenerateMessage(t *testing.T) {
	// setup
	tests := []struct {
		gold            []utils.OwnerProgress[Resource]
		elixir          []utils.OwnerProgress[Resource]
		darkElixir      []utils.OwnerProgress[Resource]
		interval        information_manager.Interval
		expectedMessage string
	}{
		{
			gold: []utils.OwnerProgress[Resource]{
				{
					Owner: "owner1",
					Players: []utils.PlayerProgress[Resource]{
						{Current: clash.Player{Name: "player1"}, Data: 1000000},
					},
				},
			},
			elixir: []utils.OwnerProgress[Resource]{
				{
					Owner: "owner1",
					Players: []utils.PlayerProgress[Resource]{
						{Current: clash.Player{Name: "player1"}, Data: 40000},
					},
				},
				{
					Owner: "owner2",
					Players: []utils.PlayerProgress[Resource]{
						{Current: clash.Player{Name: "player2"}, Data: 10000},
						{Current: clash.Player{Name: "player3"}, Data: 1000},
					},
				},
			},
			darkElixir: []utils.OwnerProgress[Resource]{
				{
					Owner: "owner2",
					Players: []utils.PlayerProgress[Resource]{
						{Current: clash.Player{Name: "player3"}, Data: 1000},
					},
				},
			},
			interval: information_manager.Day,
			expectedMessage: `<b>Farm do Dia</b>
Farmadores de gold
<b>1.</b> owner1 <b>1,000,000</b>
        player1 <b>1,000,000</b>

Farmadores de elixir
<b>1.</b> owner1 <b>40,000</b>
        player1 <b>40,000</b>
<b>2.</b> owner2 <b>11,000</b>
        player2 <b>10,000</b>
        player3 <b>1,000</b>

Farmadores de dark elixir
<b>1.</b> owner2 <b>1,000</b>
        player3 <b>1,000</b>`,
		},
		{
			gold:       []utils.OwnerProgress[Resource]{},
			elixir:     []utils.OwnerProgress[Resource]{},
			darkElixir: []utils.OwnerProgress[Resource]{},
			interval:   information_manager.Month,
			expectedMessage: `<b>Farm do Mês</b>
Ninguém farmou gold

Ninguém farmou elixir

Ninguém farmou dark elixir`,
		},
	}
	action := Farm{}
	for idx, test := range tests {
		// act
		message := action.generateMessage(test.gold, test.elixir, test.darkElixir, test.interval)
		// test
		if strings.TrimSpace(message) != strings.TrimSpace(test.expectedMessage) {
			t.Errorf("expected %v, got %v for test %v", test.expectedMessage, message, idx)
		}
	}
}

func TestPlayerResourceExtractor(t *testing.T) {
	// setup
	previous := generateFarmTestPlayer("tag1", "player1", 100, 200, 300)
	current := generateFarmTestPlayer("tag1", "player1", 1000, 2000, 3000)
	expectedElixir := 1800
	// act
	elixir := playerResourceExtractor(elixirAchievement)(previous, current)
	// test
	if elixir != int64(expectedElixir) {
		t.Fatalf("expected %v, got %v", expectedElixir, elixir)
	}
}

func TestFarmCommand(t *testing.T) {
	// setup
	previous := []clash.Player{
		generateFarmTestPlayer("tag1", "player1", 100, 100, 100),
		generateFarmTestPlayer("tag2", "player2", 100, 100, 100),
		generateFarmTestPlayer("tag3", "player3", 100, 100, 100),
		generateFarmTestPlayer("tag4", "player4", 100, 100, 100),
		generateFarmTestPlayer("tag5", "player5", 100, 100, 100),
		generateFarmTestPlayer("tag6", "player6", 100, 100, 100),
		generateFarmTestPlayer("tag7", "player7", 600, 200, 300),
		generateFarmTestPlayer("tag8", "player8", 300, 400, 200),
	}
	current := []clash.Player{
		generateFarmTestPlayer("tag1", "player1", 600, 200, 100),
		generateFarmTestPlayer("tag2", "player2", 300, 100, 200),
		generateFarmTestPlayer("tag3", "player3", 500, 100, 600),
		generateFarmTestPlayer("tag4", "player4", 700, 300, 400),
		generateFarmTestPlayer("tag5", "player5", 400, 500, 300),
		generateFarmTestPlayer("tag6", "player6", 200, 400, 500),
		generateFarmTestPlayer("tag7", "player7", 900, 400, 500),
		generateFarmTestPlayer("tag8", "player8", 400, 500, 300),
		generateFarmTestPlayer("tag9", "player9", 200, 400, 500),
	}

	var (
		monthCallback func(ctx context.Context, a, b []clash.Player) error = nil
	)
	mockInformationManager := &mocks.InformationManager{}
	mockInformationManager.On("Subscribe", information_manager.Player, information_manager.Day, mock.MatchedBy(func(callback func(ctx context.Context, a, b []clash.Player) error) bool {
		return true
	})).Return(nil, nil)
	mockInformationManager.On("Subscribe", information_manager.Player, information_manager.Year, mock.MatchedBy(func(callback func(ctx context.Context, a, b []clash.Player) error) bool {
		return true
	})).Return(nil, nil)
	mockInformationManager.On("Subscribe", information_manager.Player, information_manager.Month, mock.MatchedBy(func(callback func(ctx context.Context, a, b []clash.Player) error) bool {
		if monthCallback == nil {
			monthCallback = callback
			return true
		}
		return false
	})).Return(func(ctx context.Context) error {
		return monthCallback(ctx, previous, current)
	}, nil)

	var farmRankCommand bot.Command
	mockBot := &mocks.Bot{}
	mockBot.On("SetCommand", mock.Anything, "farm", mock.MatchedBy(func(command bot.Command) bool {
		farmRankCommand = command
		return true
	}), mock.Anything).Return(nil)

	mockConfig := &mocks.ConfigurationManager{}
	mockConfig.On("AccountOwner", mock.Anything).Return(func(tag string) string {
		owners := map[string]string{
			"tag1": "owners1",
			"tag2": "owners1",
			"tag3": "owners1",
			"tag4": "owners2",
			"tag5": "owners2",
			"tag6": "owners2",
			"tag7": "owners3",
			"tag8": "owners3",
			"tag9": "owners3",
		}
		if owner, ok := owners[tag]; ok {
			return owner
		}
		return "unknown"
	}, nil)

	expectedMessage := `<b>Farm do Mês</b>
Farmadores de gold
<b>1.</b> owners1 <b>1,100</b>
        player1 <b>500</b>
        player3 <b>400</b>
        player2 <b>200</b>
<b>2.</b> owners2 <b>1,000</b>
        player4 <b>600</b>
        player5 <b>300</b>
        player6 <b>100</b>
<b>3.</b> owners3 <b>600</b>
        player7 <b>300</b>
        player9 <b>200</b>
        player8 <b>100</b>

Farmadores de elixir
<b>1.</b> owners2 <b>900</b>
        player5 <b>400</b>
        player6 <b>300</b>
        player4 <b>200</b>
<b>2.</b> owners3 <b>700</b>
        player9 <b>400</b>
        player7 <b>200</b>
        player8 <b>100</b>
<b>3.</b> owners1 <b>100</b>
        player1 <b>100</b>

Farmadores de dark elixir
<b>1.</b> owners2 <b>900</b>
        player6 <b>400</b>
        player4 <b>300</b>
        player5 <b>200</b>
<b>2.</b> owners3 <b>800</b>
        player9 <b>500</b>
        player7 <b>200</b>
        player8 <b>100</b>
<b>3.</b> owners1 <b>600</b>
        player3 <b>500</b>
        player2 <b>100</b>`

	setupFarm(mockBot, mockInformationManager, mockConfig)

	var message string
	mockBot.On("SendMessage", mock.Anything, mock.MatchedBy(func(receivedMessage string) bool {
		message = receivedMessage
		return true
	})).Return(0, nil)

	// act
	farmRankCommand(context.Background())

	// test
	if strings.TrimSpace(message) != expectedMessage {
		t.Errorf("expected %v, got %v", expectedMessage, message)
	}
}
