package home_village

import (
	"bytes"
	"context"
	"fmt"
	"sort"
	"strings"

	"golang.org/x/text/language"
	"golang.org/x/text/message"

	"gitlab.com/reinodovo/botdovo/internal/bot"
	"gitlab.com/reinodovo/botdovo/internal/clash"
	"gitlab.com/reinodovo/botdovo/internal/configuration_manager"
	"gitlab.com/reinodovo/botdovo/internal/information_manager"
	"gitlab.com/reinodovo/botdovo/internal/utils"
)

const (
	goldAchievement       = "Gold Grab"
	elixirAchievement     = "Elixir Escapade"
	darkElixirAchievement = "Heroic Heist"

	farmRankSize = 5
)

type Resource = int64

type Farm struct {
	bot          bot.Bot
	mgr          information_manager.InformationManager
	config       configuration_manager.ConfigurationManager
	monthTrigger func(ctx context.Context) error
}

func setupFarm(bot bot.Bot, mgr information_manager.InformationManager, config configuration_manager.ConfigurationManager) error {
	farm := &Farm{
		bot:    bot,
		mgr:    mgr,
		config: config,
	}
	farmExplanation := "farm do mês"
	if err := bot.SetCommand(context.Background(), "farm", farm.farmCommand, farmExplanation); err != nil {
		return err
	}
	var err error
	if _, err = mgr.Subscribe(information_manager.Player, information_manager.Day, farm.handleDailyFarm); err != nil {
		return err
	}
	if farm.monthTrigger, err = mgr.Subscribe(information_manager.Player, information_manager.Month, farm.handleMonthlyFarm); err != nil {
		return err
	}
	if _, err = mgr.Subscribe(information_manager.Player, information_manager.Year, farm.handleAnnualFarm); err != nil {
		return err
	}
	return nil
}

func playerResourceExtractor(achievementName string) func(previous, current clash.Player) Resource {
	return func(previous, current clash.Player) Resource {
		currentAchievement, err := current.FindAchieventment(achievementName)
		if err != nil {
			return 0
		}
		previousAchievement, err := previous.FindAchieventment(achievementName)
		if err != nil {
			previousAchievement.Value = 0
		}
		return currentAchievement.Value - previousAchievement.Value
	}
}

func ownerPlayersResourcesSum(owner utils.OwnerProgress[Resource]) Resource {
	var sum Resource = 0
	for _, player := range owner.Players {
		sum += player.Data
	}
	return sum
}

func (action *Farm) rankByResourceIgnoringNonPositive(owners []utils.OwnerProgress[Resource]) []utils.OwnerProgress[Resource] {
	var validOwners []utils.OwnerProgress[Resource]
	for i, owner := range owners {
		var validPlayers []utils.PlayerProgress[Resource]
		for _, player := range owner.Players {
			if player.Data > 0 {
				validPlayers = append(validPlayers, player)
			}
		}
		sort.Slice(validPlayers[:], func(i, j int) bool {
			return validPlayers[i].Data > validPlayers[j].Data
		})
		if len(validPlayers) > 0 {
			owners[i].Players = validPlayers
			validOwners = append(validOwners, owners[i])
		}
	}
	owners = validOwners
	sort.Slice(owners[:], func(i, j int) bool {
		return ownerPlayersResourcesSum(owners[i]) > ownerPlayersResourcesSum(owners[j])
	})
	return owners
}

func (action *Farm) ownerResources(previous, current []clash.Player) (gold, elixir, darkElixir []utils.OwnerProgress[Resource]) {
	gold = utils.PlayersProgressByOwner(previous, current, playerResourceExtractor(goldAchievement), action.config)
	elixir = utils.PlayersProgressByOwner(previous, current, playerResourceExtractor(elixirAchievement), action.config)
	darkElixir = utils.PlayersProgressByOwner(previous, current, playerResourceExtractor(darkElixirAchievement), action.config)
	return
}

func (action *Farm) resourcesRank(previous, current []clash.Player) (gold, elixir, darkElixir []utils.OwnerProgress[Resource]) {
	gold, elixir, darkElixir = action.ownerResources(previous, current)
	gold = action.rankByResourceIgnoringNonPositive(gold)
	elixir = action.rankByResourceIgnoringNonPositive(elixir)
	darkElixir = action.rankByResourceIgnoringNonPositive(darkElixir)
	return gold, elixir, darkElixir
}

func (action *Farm) createResourceRankMessage(resourceName string, owners []utils.OwnerProgress[Resource]) string {
	if len(owners) == 0 {
		return fmt.Sprintf("Ninguém farmou %v\n", strings.ToLower(resourceName))
	}
	var buffer bytes.Buffer
	buffer.WriteString(fmt.Sprintf("Farmadores de %v\n", strings.ToLower(resourceName)))
	for rank, owner := range owners {
		p := message.NewPrinter(language.English)
		buffer.WriteString(p.Sprintf("<b>%v.</b> %v <b>%v</b>\n", rank+1, owner.Owner, ownerPlayersResourcesSum(owner)))
		for _, player := range owner.Players {
			buffer.WriteString(p.Sprintf("        %v <b>%v</b>\n", player.Current.Name, player.Data))
		}
	}
	return buffer.String()
}

func (action *Farm) generateMessage(gold, elixir, darkElixir []utils.OwnerProgress[Resource], interval information_manager.Interval) string {
	var buffer bytes.Buffer
	switch interval {
	case information_manager.Year:
		buffer.WriteString("<b>Farm do Ano</b>\n")
	case information_manager.Month:
		buffer.WriteString("<b>Farm do Mês</b>\n")
	case information_manager.Day:
		buffer.WriteString("<b>Farm do Dia</b>\n")
	}
	buffer.WriteString(fmt.Sprintf("%v\n", action.createResourceRankMessage("Gold", gold)))
	buffer.WriteString(fmt.Sprintf("%v\n", action.createResourceRankMessage("Elixir", elixir)))
	buffer.WriteString(fmt.Sprintf("%v\n", action.createResourceRankMessage("Dark Elixir", darkElixir)))
	return buffer.String()
}

func (action *Farm) farmCommand(ctx context.Context, args ...string) error {
	return action.monthTrigger(ctx)
}

func (action *Farm) handleDailyFarm(ctx context.Context, previous, current []clash.Player) error {
	return action.handleFarm(ctx, previous, current, information_manager.Day)
}

func (action *Farm) handleMonthlyFarm(ctx context.Context, previous, current []clash.Player) error {
	return action.handleFarm(ctx, previous, current, information_manager.Month)
}

func (action *Farm) handleAnnualFarm(ctx context.Context, previous, current []clash.Player) error {
	return action.handleFarm(ctx, previous, current, information_manager.Year)
}

func (action *Farm) handleFarm(ctx context.Context, previous, current []clash.Player, interval information_manager.Interval) error {
	gold, elixir, darkElixir := action.resourcesRank(previous, current)
	message := action.generateMessage(gold, elixir, darkElixir, interval)
	_, err := action.bot.SendMessage(ctx, message)
	return err
}
