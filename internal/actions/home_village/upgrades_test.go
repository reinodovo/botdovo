package home_village

import (
	"context"
	"strings"
	"testing"

	"github.com/stretchr/testify/mock"
	"gitlab.com/reinodovo/botdovo/internal/bot"
	"gitlab.com/reinodovo/botdovo/internal/clash"
	"gitlab.com/reinodovo/botdovo/internal/information_manager"
	"gitlab.com/reinodovo/botdovo/internal/mocks"
)

func compareUpgrades(a, b []upgrade) bool {
	if len(a) != len(b) {
		return false
	}
	for idx, upgradeA := range a {
		upgradeB := b[idx]
		if upgradeA != upgradeB {
			return false
		}
	}
	return true
}

func TestUpgradesGeneratePlayerMessage(t *testing.T) {
	// setup
	name := "player1"
	tests := []struct {
		upgrades        []upgrade
		expectedMessage string
	}{
		{
			upgrades: []upgrade{
				{name: "army1", oldLevel: 10, newLevel: 11},
				{name: "army2", oldLevel: 0, newLevel: 1},
			},
			expectedMessage: `<b>player1</b>
army1 10 -> 11
army2 0 -> 1`,
		},
		{
			upgrades:        []upgrade{},
			expectedMessage: "",
		},
	}
	action := Upgrades{}
	for idx, test := range tests {
		// act
		message := action.generatePlayerMessage(clash.Player{Name: name}, test.upgrades)
		// test
		if strings.TrimSpace(message) != strings.TrimSpace(test.expectedMessage) {
			t.Errorf("message and expectedMessage did not match for test %v", idx)
		}
	}
}

func TestUpgradesGenerateMessage(t *testing.T) {
	// setup
	tests := []struct {
		playersUpgrades []playerUpgrades
		interval        information_manager.Interval
		expectedMessage string
	}{
		{
			playersUpgrades: []playerUpgrades{
				{
					player:   clash.Player{Name: "player1"},
					upgrades: []upgrade{{name: "army1", oldLevel: 10, newLevel: 11}},
				},
				{
					player:   clash.Player{Name: "player2"},
					upgrades: []upgrade{{name: "army1", oldLevel: 0, newLevel: 1}},
				},
			},
			interval: information_manager.Day,
			expectedMessage: `<b>Upgrades do Dia</b>
<b>player1</b>
army1 10 -> 11

<b>player2</b>
army1 0 -> 1`,
		},
		{
			playersUpgrades: []playerUpgrades{
				{
					player:   clash.Player{Name: "player1"},
					upgrades: []upgrade{{name: "army1", oldLevel: 10, newLevel: 11}},
				},
			},
			interval: information_manager.Month,
			expectedMessage: `<b>Upgrades do Mês</b>
<b>player1</b>
army1 10 -> 11`,
		},
		{
			playersUpgrades: []playerUpgrades{},
			interval:        information_manager.Month,
			expectedMessage: `<b>Upgrades do Mês</b>
Nada uppado`,
		},
	}
	action := Upgrades{}
	for idx, test := range tests {
		// act
		message := action.generateMessage(test.playersUpgrades, test.interval)
		// test
		if strings.TrimSpace(message) != strings.TrimSpace(test.expectedMessage) {
			t.Errorf("message and expectedMessage did not match for test %v", idx)
		}
	}
}

func TestArmyUpgrades(t *testing.T) {
	// setup
	previous := []clash.Army{
		{Name: "Army1", Level: 5, Village: "home"},
		{Name: "Army2", Level: 5, Village: "home"},
		{Name: "Army3", Level: 5, Village: "home"},
		{Name: "Army3", Level: 5, Village: "builder"},
		{Name: "Army4", Level: 5, Village: "home"},
	}
	current := []clash.Army{
		{Name: "Army1", Level: 6, Village: "home"},
		{Name: "Army2", Level: 5, Village: "home"},
		{Name: "Army3", Level: 7, Village: "home"},
		{Name: "Army3", Level: 6, Village: "builder"},
		{Name: "Army5", Level: 5, Village: "home"},
	}
	expectedUpgrades := []upgrade{
		{name: "Army1", oldLevel: 5, newLevel: 6},
		{name: "Army3", oldLevel: 5, newLevel: 7},
		{name: "Army5", oldLevel: 0, newLevel: 5},
	}
	action := Upgrades{}
	// act
	upgrades := action.armyUpgrades(previous, current)
	// test
	if !compareUpgrades(upgrades, expectedUpgrades) {
		t.Errorf("expected %v, got %v", expectedUpgrades, upgrades)
	}
}

func TestPlayerUpgrades(t *testing.T) {
	// setup
	previous := clash.Player{
		TownHallLevel:       13,
		TownHallWeaponLevel: 4,
		Heroes:              []clash.Army{{Name: "Hero", Level: 5, Village: "home"}},
		Troops:              []clash.Army{{Name: "Troop", Level: 5, Village: "home"}},
		Spells:              []clash.Army{{Name: "Spell", Level: 5, Village: "home"}},
	}
	current := clash.Player{
		TownHallLevel:       14,
		TownHallWeaponLevel: 5,
		Heroes:              []clash.Army{{Name: "Hero", Level: 6, Village: "home"}},
		Troops:              []clash.Army{{Name: "Troop", Level: 6, Village: "home"}},
		Spells:              []clash.Army{{Name: "Spell", Level: 6, Village: "home"}},
	}
	expectedUpgrades := []upgrade{
		{name: "Town Hall", oldLevel: 13, newLevel: 14},
		{name: "Town Hall Weapon", oldLevel: 4, newLevel: 5},
		{name: "Hero", oldLevel: 5, newLevel: 6},
		{name: "Troop", oldLevel: 5, newLevel: 6},
		{name: "Spell", oldLevel: 5, newLevel: 6},
	}
	action := Upgrades{}
	// act
	upgrades := action.getPlayerUpgrades(previous, current)
	// test
	if !compareUpgrades(upgrades, expectedUpgrades) {
		t.Errorf("expected %v, got %v", expectedUpgrades, upgrades)
	}
}

func TestAllPlayerUpgrades(t *testing.T) {
	// setup
	previous := []clash.Player{
		{Tag: "tag1", TownHallLevel: 13},
		{Tag: "tag2"},
		{Tag: "tag3", TownHallWeaponLevel: 4},
	}
	current := []clash.Player{
		{Tag: "tag1", TownHallLevel: 14},
		{Tag: "tag2"},
		{Tag: "tag3", TownHallWeaponLevel: 5},
		{Tag: "tag4", TownHallWeaponLevel: 5},
	}
	expectedPlayerUpgrades := []playerUpgrades{
		{
			player: current[0],
			upgrades: []upgrade{
				{name: "Town Hall", oldLevel: 13, newLevel: 14},
			},
		},
		{
			player: current[2],
			upgrades: []upgrade{
				{name: "Town Hall Weapon", oldLevel: 4, newLevel: 5},
			},
		},
		{
			player: current[3],
			upgrades: []upgrade{
				{name: "Town Hall Weapon", oldLevel: 0, newLevel: 5},
			},
		},
	}
	action := Upgrades{}
	// act
	allPlayersUpgrades := action.getAllPlayersUpgrades(previous, current)
	// test
	if len(allPlayersUpgrades) != len(expectedPlayerUpgrades) {
		t.Fatalf("expected %v player upgrades, got %v", len(expectedPlayerUpgrades), len(allPlayersUpgrades))
	}
	for idx, playerUpgrades := range allPlayersUpgrades {
		expectedPlayerUpgrade := expectedPlayerUpgrades[idx]
		if expectedPlayerUpgrade.player.Tag != playerUpgrades.player.Tag {
			t.Errorf("player tags at position %v differ: expected %v, got %v", idx, expectedPlayerUpgrade.player.Tag, playerUpgrades.player.Tag)
		}
		if !compareUpgrades(playerUpgrades.upgrades, expectedPlayerUpgrade.upgrades) {
			t.Errorf("expected %v, got %v", expectedPlayerUpgrade.upgrades, playerUpgrades.upgrades)
		}
	}
}

func TestUpgradesSortPlayersUpgrades(t *testing.T) {
	// setup
	playersUpgrades := []playerUpgrades{
		{
			player:   clash.Player{Tag: "tag1", TownHallLevel: 10, TownHallWeaponLevel: 0, ExpLevel: 1000},
			upgrades: []upgrade{},
		},
		{
			player:   clash.Player{Tag: "tag2", TownHallLevel: 12, TownHallWeaponLevel: 5, ExpLevel: 100},
			upgrades: []upgrade{},
		},
		{
			player:   clash.Player{Tag: "tag3", TownHallLevel: 12, TownHallWeaponLevel: 2, ExpLevel: 300},
			upgrades: []upgrade{},
		},
		{
			player:   clash.Player{Tag: "tag4", TownHallLevel: 12, TownHallWeaponLevel: 4, ExpLevel: 300},
			upgrades: []upgrade{},
		},
		{
			player:   clash.Player{Tag: "tag5", TownHallLevel: 12, TownHallWeaponLevel: 5, ExpLevel: 1000},
			upgrades: []upgrade{},
		},
	}
	expectedPlayerOrder := []clash.Player{
		{Tag: "tag5"},
		{Tag: "tag2"},
		{Tag: "tag4"},
		{Tag: "tag3"},
		{Tag: "tag1"},
	}
	action := &Upgrades{}
	// act
	resultPlayersUpgrades := action.sortPlayersUpgrades(playersUpgrades)
	// test
	if len(expectedPlayerOrder) != len(resultPlayersUpgrades) {
		t.Errorf("expected %v entries, got %v", len(expectedPlayerOrder), len(resultPlayersUpgrades))
	}
	for idx, playerUpgrade := range resultPlayersUpgrades {
		expectedPlayer := expectedPlayerOrder[idx]
		if playerUpgrade.player.Tag != expectedPlayer.Tag {
			t.Errorf("expected tag %v at position %v, got %v", expectedPlayer.Tag, idx, playerUpgrade.player.Tag)
		}
	}
}

func TestUpgradesCommand(t *testing.T) {
	// setup
	previous := []clash.Player{{
		Tag:                 "tag1",
		Name:                "player1",
		TownHallLevel:       13,
		TownHallWeaponLevel: 4,
		Heroes:              []clash.Army{{Name: "Hero", Level: 5, Village: "home"}},
		Troops:              []clash.Army{{Name: "Troop", Level: 5, Village: "home"}},
		Spells:              []clash.Army{{Name: "Spell", Level: 5, Village: "home"}},
	}}
	current := []clash.Player{{
		Tag:                 "tag1",
		Name:                "player1",
		TownHallLevel:       14,
		TownHallWeaponLevel: 5,
		Heroes:              []clash.Army{{Name: "Hero", Level: 6, Village: "home"}},
		Troops:              []clash.Army{{Name: "Troop", Level: 6, Village: "home"}},
		Spells:              []clash.Army{{Name: "Spell", Level: 6, Village: "home"}},
	}}

	var (
		dayCallback   func(ctx context.Context, a, b []clash.Player) error = nil
		monthCallback func(ctx context.Context, a, b []clash.Player) error = nil
	)
	mockInformationManager := &mocks.InformationManager{}
	mockInformationManager.On("Subscribe", information_manager.Player, information_manager.Day, mock.MatchedBy(func(callback func(ctx context.Context, a, b []clash.Player) error) bool {
		if dayCallback == nil {
			dayCallback = callback
			return true
		}
		return false
	})).Return(func(ctx context.Context) error {
		return dayCallback(ctx, previous, current)
	}, nil)
	mockInformationManager.On("Subscribe", information_manager.Player, information_manager.Month, mock.MatchedBy(func(callback func(ctx context.Context, a, b []clash.Player) error) bool {
		if monthCallback == nil {
			monthCallback = callback
			return true
		}
		return false
	})).Return(func(ctx context.Context) error {
		return monthCallback(ctx, previous, current)
	}, nil)

	var upgradesCommand bot.Command
	mockBot := &mocks.Bot{}
	mockBot.On("SetCommand", mock.Anything, "upgrades", mock.MatchedBy(func(command bot.Command) bool {
		upgradesCommand = command
		return true
	}), mock.Anything).Return(nil)

	tests := []struct {
		args            []string
		expectedMessage string
	}{
		{
			args: []string{},
			expectedMessage: `<b>Upgrades do Dia</b>
<b>player1</b>
Town Hall 13 -> 14
Town Hall Weapon 4 -> 5
Hero 5 -> 6
Troop 5 -> 6
Spell 5 -> 6`,
		},
		{
			args: []string{"m"},
			expectedMessage: `<b>Upgrades do Mês</b>
<b>player1</b>
Town Hall 13 -> 14
Town Hall Weapon 4 -> 5
Hero 5 -> 6
Troop 5 -> 6
Spell 5 -> 6`,
		},
		{
			args: []string{"month"},
			expectedMessage: `<b>Upgrades do Mês</b>
<b>player1</b>
Town Hall 13 -> 14
Town Hall Weapon 4 -> 5
Hero 5 -> 6
Troop 5 -> 6
Spell 5 -> 6`,
		},
		{
			args:            []string{"t"},
			expectedMessage: "fala direito fdp",
		},
	}

	setupUpgrades(mockBot, mockInformationManager)

	for idx, test := range tests {
		mockBot.On("SendMessage", mock.Anything, mock.MatchedBy(func(message string) bool {
			// test
			if strings.TrimSpace(message) != strings.TrimSpace(test.expectedMessage) {
				t.Errorf("expected %v, got %v for test %v", test.expectedMessage, message, idx)
			}
			return true
		})).Return(0, nil)

		// act
		upgradesCommand(context.Background(), test.args...)
	}
}
