package builder_base

import (
	"bytes"
	"context"
	"fmt"

	"gitlab.com/reinodovo/botdovo/internal/bot"
	"gitlab.com/reinodovo/botdovo/internal/clash"
	"gitlab.com/reinodovo/botdovo/internal/information_manager"
	"gitlab.com/reinodovo/botdovo/internal/utils"
)

const versusBattlesRankSize int = 5

type versusRank struct {
	bot      bot.Bot
	clashAPI clash.ClashAPI
}

func setupVersusRank(bot bot.Bot, clashAPI clash.ClashAPI, mgr information_manager.InformationManager) error {
	versusRank := &versusRank{bot: bot, clashAPI: clashAPI}
	if _, err := mgr.Subscribe(information_manager.Player, information_manager.Month, versusRank.handleMonthlyVersusBattleResults); err != nil {
		return err
	}
	vilinhaHelp := "rank do total de vitórias na vilinha (all - mostra todos os membros)"
	return bot.SetCommand(context.Background(), "vilinha", versusRank.rankCommand, vilinhaHelp)
}

func getListSize(wins []versusBattlePlayer, args ...string) (int, bool, error) {
	listSize := utils.Min(versusBattlesRankSize, len(wins))
	if len(args) == 0 {
		return listSize, listSize == len(wins), nil
	}
	if len(args) == 1 && args[0] == "all" {
		return len(wins), true, nil
	}
	return 0, false, utils.InvalidArgumentsError
}

func generateRankMessage(wins []versusBattlePlayer, showAllPlayers bool, interval information_manager.Interval) string {
	var buffer bytes.Buffer
	if interval == information_manager.Month {
		buffer.WriteString("<b>Doentes do Mês</b>\n")
	} else {
		if showAllPlayers {
			buffer.WriteString("<b>Todos os doentes</b>\n")
		} else {
			buffer.WriteString(fmt.Sprintf("<b>Top %v doentes</b>\n", len(wins)))
		}
	}
	if len(wins) == 0 {
		buffer.WriteString("Nenhuma vitória na vilinha, cadê os doente desse grupo?")
	}
	for idx, player := range wins {
		victoryString := "vitórias"
		if player.wins == 1 {
			victoryString = "vitória"
		}
		playerMessage := fmt.Sprintf("<b>%v.</b> %v com <b>%v</b> %v\n", idx+1, player.current.Name, player.wins, victoryString)
		buffer.WriteString(playerMessage)
	}
	return buffer.String()
}

func (action *versusRank) rankCommand(ctx context.Context, args ...string) error {
	players, err := action.clashAPI.MainClanMembers(ctx)
	if err != nil {
		return err
	}

	wins, _ := versusBattlePlayerResults([]clash.Player{}, players)

	sortByVersusBattleWins(wins)

	listSize, allPlayers, err := getListSize(wins, args...)
	if err != nil {
		_, err = action.bot.SendMessage(ctx, "não entendi")
		return err
	}

	message := generateRankMessage(wins[:listSize], allPlayers, "")
	_, err = action.bot.SendMessage(ctx, message)
	return err
}

func (action *versusRank) handleMonthlyVersusBattleResults(ctx context.Context, previous, current []clash.Player) error {
	wins, _ := versusBattlePlayerResults(previous, current)
	wins = sortByVersusBattleWins(wins)
	_, err := action.bot.SendMessage(ctx, generateRankMessage(wins, true, information_manager.Month))
	return err
}
