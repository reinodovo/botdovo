package builder_base

import (
	"gitlab.com/reinodovo/botdovo/internal/bot"
	"gitlab.com/reinodovo/botdovo/internal/information_manager"
)

func setupHonoraryMembersVersusBattles(bot bot.Bot, mgr information_manager.InformationManager) error {
	versusBattles := &versusBattles{bot: bot}
	if _, err := mgr.Subscribe(information_manager.HonoraryMembers, information_manager.Minute, versusBattles.handleVersusBattleResults); err != nil {
		return err
	}
	return nil
}
