package builder_base

import (
	"context"
	"fmt"

	"gitlab.com/reinodovo/botdovo/internal/bot"
	"gitlab.com/reinodovo/botdovo/internal/clash"
	"gitlab.com/reinodovo/botdovo/internal/information_manager"
)

type versusBattles struct {
	bot bot.Bot
}

func setupVersusBattles(bot bot.Bot, mgr information_manager.InformationManager) error {
	versusBattles := &versusBattles{bot: bot}
	if _, err := mgr.Subscribe(information_manager.Player, information_manager.Minute, versusBattles.handleVersusBattleResults); err != nil {
		return err
	}
	return nil
}

func generateBattlesMessage(player versusBattlePlayer, won bool) string {
	if won {
		if player.current.VersusBattleWins >= 1000 && player.previous.VersusBattleWins < 1000 {
			return fmt.Sprintf("Parabéns por mil vitórias na vilinha, %s", player.current.Name)
		}
		return fmt.Sprintf("Parabéns pela vitória na vilinha, %v", player.current.Name)
	}
	return fmt.Sprintf("Meus pêsames pela derrota na vilinha, %v", player.current.Name)
}

func (action *versusBattles) handleVersusBattleResults(ctx context.Context, previous, current []clash.Player) error {
	wins, losses := versusBattlePlayerResults(previous, current)
	for _, playerInfo := range wins {
		action.bot.SendMessage(ctx, generateBattlesMessage(playerInfo, true))
	}
	for _, playerInfo := range losses {
		action.bot.SendMessage(ctx, generateBattlesMessage(playerInfo, false))
	}
	return nil
}
