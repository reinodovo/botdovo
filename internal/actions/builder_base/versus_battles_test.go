package builder_base

import (
	"context"
	"reflect"
	"sort"
	"testing"

	"github.com/stretchr/testify/mock"
	"gitlab.com/reinodovo/botdovo/internal/clash"
	"gitlab.com/reinodovo/botdovo/internal/information_manager"
	"gitlab.com/reinodovo/botdovo/internal/mocks"
)

func TestVersusBattleGenerateMessage(t *testing.T) {
	// setup
	name := "player1"
	var tests = []struct {
		previousWins    int64
		currentWins     int64
		won             bool
		expectedMessage string
	}{
		{
			previousWins:    999,
			currentWins:     1000,
			won:             true,
			expectedMessage: "Parabéns por mil vitórias na vilinha, player1",
		},
		{
			previousWins:    1000,
			currentWins:     1001,
			won:             true,
			expectedMessage: "Parabéns pela vitória na vilinha, player1",
		},
		{
			previousWins:    900,
			currentWins:     2000,
			won:             true,
			expectedMessage: "Parabéns por mil vitórias na vilinha, player1",
		},
		{
			previousWins:    0,
			currentWins:     1,
			won:             true,
			expectedMessage: "Parabéns pela vitória na vilinha, player1",
		},
		{
			previousWins:    0,
			currentWins:     1,
			won:             false,
			expectedMessage: "Meus pêsames pela derrota na vilinha, player1",
		},
	}
	for _, test := range tests {
		previous := clash.Player{Name: name, VersusBattleWins: test.previousWins}
		current := clash.Player{Name: name, VersusBattleWins: test.currentWins}
		// act
		message := generateBattlesMessage(versusBattlePlayer{previous, current, 0, 0}, test.won)
		// test
		if message != test.expectedMessage {
			t.Errorf("expected message to be %v, got %v", test.expectedMessage, message)
		}
	}
}

func TestVersusBattleSubscription(t *testing.T) {
	// setup
	var trigger func(ctx context.Context, previous, current []clash.Player) error

	expectedMessages := []string{
		"Parabéns pela vitória na vilinha, player1",
		"Meus pêsames pela derrota na vilinha, player2",
		"Parabéns pela vitória na vilinha, player4",
		"Meus pêsames pela derrota na vilinha, player4",
	}
	var messages []string
	mockBot := &mocks.Bot{}
	mockBot.On("SetCommand", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
	mockBot.On("SendMessage", mock.Anything, mock.MatchedBy(func(message string) bool {
		messages = append(messages, message)
		return true
	})).Return(0, nil)

	mockInformationManager := &mocks.InformationManager{}
	mockInformationManager.On("Subscribe", information_manager.Player, information_manager.Minute, mock.MatchedBy(func(callback func(ctx context.Context, a, b []clash.Player) error) bool {
		trigger = callback
		return true
	})).Return(nil, nil)

	setupVersusBattles(mockBot, mockInformationManager)

	previous := []clash.Player{
		{Name: "player1", Tag: "tag1", VersusBattleWins: 100, VersusTrophies: 50},
		{Name: "player2", Tag: "tag2", VersusBattleWins: 50, VersusTrophies: 100},
		{Name: "player3", Tag: "tag3", VersusBattleWins: 1000, VersusTrophies: 50},
		{Name: "player4", Tag: "tag4", VersusBattleWins: 60, VersusTrophies: 500},
	}
	current := []clash.Player{
		{Name: "player1", Tag: "tag1", VersusBattleWins: 110, VersusTrophies: 60},
		{Name: "player2", Tag: "tag2", VersusBattleWins: 50, VersusTrophies: 60},
		{Name: "player3", Tag: "tag3", VersusBattleWins: 1000, VersusTrophies: 50},
		{Name: "player4", Tag: "tag4", VersusBattleWins: 61, VersusTrophies: 400},
	}

	// act
	trigger(context.Background(), previous, current)

	// test
	sort.Strings(messages)
	sort.Strings(expectedMessages)
	if !reflect.DeepEqual(messages, expectedMessages) {
		t.Errorf("expected %v, got %v", expectedMessages, messages)
	}
}
