package builder_base

import (
	"context"
	"strings"
	"testing"

	"github.com/stretchr/testify/mock"
	"gitlab.com/reinodovo/botdovo/internal/bot"
	"gitlab.com/reinodovo/botdovo/internal/clash"
	"gitlab.com/reinodovo/botdovo/internal/information_manager"
	"gitlab.com/reinodovo/botdovo/internal/mocks"
	"gitlab.com/reinodovo/botdovo/internal/utils"
)

func TestVersusRankGenerateRankMessage(t *testing.T) {
	// setup
	tests := []struct {
		wins            []versusBattlePlayer
		allPlayers      bool
		expectedMessage string
	}{
		{
			wins: []versusBattlePlayer{
				{current: clash.Player{Name: "player1", VersusBattleWins: 200}, wins: 200},
				{current: clash.Player{Name: "player2", VersusBattleWins: 100}, wins: 100},
			},
			allPlayers: true,
			expectedMessage: `<b>Todos os doentes</b>
<b>1.</b> player1 com <b>200</b> vitórias
<b>2.</b> player2 com <b>100</b> vitórias`,
		},
		{
			wins: []versusBattlePlayer{
				{current: clash.Player{Name: "player1", VersusBattleWins: 200}, wins: 200},
				{current: clash.Player{Name: "player2", VersusBattleWins: 100}, wins: 100},
				{current: clash.Player{Name: "player3", VersusBattleWins: 1}, wins: 1},
			},
			allPlayers: false,
			expectedMessage: `<b>Top 3 doentes</b>
<b>1.</b> player1 com <b>200</b> vitórias
<b>2.</b> player2 com <b>100</b> vitórias
<b>3.</b> player3 com <b>1</b> vitória`,
		},
	}
	for idx, test := range tests {
		// act
		message := generateRankMessage(test.wins, test.allPlayers, "")
		// test
		if strings.TrimSpace(message) != strings.TrimSpace(test.expectedMessage) {
			t.Errorf("expected %v, got %v for test %v", test.expectedMessage, message, idx)
		}
	}
}

func TestVersusBattleGenerateMonthlyMessage(t *testing.T) {
	// setup
	var tests = []struct {
		players         []versusBattlePlayer
		expectedMessage string
	}{
		{
			players: []versusBattlePlayer{},
			expectedMessage: `<b>Doentes do Mês</b>
Nenhuma vitória na vilinha, cadê os doente desse grupo?`,
		},
		{
			players: []versusBattlePlayer{
				{
					previous: clash.Player{Name: "player1", VersusBattleWins: 68},
					current:  clash.Player{Name: "player1", VersusBattleWins: 69},
					wins:     1,
					trophies: 0,
				},
				{
					previous: clash.Player{Name: "player2", VersusBattleWins: 200},
					current:  clash.Player{Name: "player2", VersusBattleWins: 300},
					wins:     100,
					trophies: 0,
				},
			},
			expectedMessage: `<b>Doentes do Mês</b>
<b>1.</b> player1 com <b>1</b> vitória
<b>2.</b> player2 com <b>100</b> vitórias`,
		},
	}
	for _, test := range tests {
		// act
		message := generateRankMessage(test.players, true, information_manager.Month)
		// test
		if strings.TrimSpace(message) != strings.TrimSpace(test.expectedMessage) {
			t.Errorf("expected message to be %v, got %v", test.expectedMessage, message)
		}
	}
}

func TestGetListSize(t *testing.T) {
	// setup
	var tests = []struct {
		players    []versusBattlePlayer
		args       []string
		listSize   int
		allPlayers bool
		err        error
	}{
		{
			players:    make([]versusBattlePlayer, 3),
			args:       []string{},
			listSize:   3,
			allPlayers: true,
			err:        nil,
		},
		{
			players:    make([]versusBattlePlayer, 10),
			args:       []string{},
			listSize:   versusBattlesRankSize,
			allPlayers: false,
			err:        nil,
		},
		{
			players:    make([]versusBattlePlayer, 10),
			args:       []string{"all"},
			listSize:   10,
			allPlayers: true,
			err:        nil,
		},
		{
			players:    make([]versusBattlePlayer, 10),
			args:       []string{"alle"},
			listSize:   0,
			allPlayers: false,
			err:        utils.InvalidArgumentsError,
		},
	}
	for _, test := range tests {
		// act
		listSize, allPlayers, err := getListSize(test.players, test.args...)
		// test
		if listSize != test.listSize {
			t.Errorf("expected listSize to be %v, got %v", test.listSize, listSize)
		}
		if allPlayers != test.allPlayers {
			t.Errorf("expected allPlayers to be %v, got %v", test.allPlayers, allPlayers)
		}
		if err != test.err {
			t.Errorf("expected err to be %v, got %v", test.err, err)
		}
	}
}

func TestVersusRankCommand(t *testing.T) {
	// setup
	var rankCommand bot.Command

	mockBot := &mocks.Bot{}
	mockBot.On("SetCommand", mock.Anything, "vilinha", mock.MatchedBy(func(command bot.Command) bool {
		rankCommand = command
		return true
	}), mock.Anything).Return(nil)

	mockClashAPI := &mocks.ClashAPI{}
	mockClashAPI.On("MainClanMembers", mock.Anything).Return([]clash.Player{
		{Name: "player1", VersusBattleWins: 100},
		{Name: "player2", VersusBattleWins: 50},
		{Name: "player3", VersusBattleWins: 1000},
		{Name: "player4", VersusBattleWins: 60},
		{Name: "player5", VersusBattleWins: 1},
		{Name: "player6", VersusBattleWins: 105},
		{Name: "player7", VersusBattleWins: 90},
	}, nil)

	mockInformationManager := &mocks.InformationManager{}
	mockInformationManager.On("Subscribe", mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)

	setupVersusRank(mockBot, mockClashAPI, mockInformationManager)

	var tests = []struct {
		args            []string
		expectedMessage string
	}{
		{
			args: []string{},
			expectedMessage: `<b>Top 5 doentes</b>
<b>1.</b> player3 com <b>1000</b> vitórias
<b>2.</b> player6 com <b>105</b> vitórias
<b>3.</b> player1 com <b>100</b> vitórias
<b>4.</b> player7 com <b>90</b> vitórias
<b>5.</b> player4 com <b>60</b> vitórias`,
		},
		{
			args: []string{"all"},
			expectedMessage: `<b>Todos os doentes</b>
<b>1.</b> player3 com <b>1000</b> vitórias
<b>2.</b> player6 com <b>105</b> vitórias
<b>3.</b> player1 com <b>100</b> vitórias
<b>4.</b> player7 com <b>90</b> vitórias
<b>5.</b> player4 com <b>60</b> vitórias
<b>6.</b> player2 com <b>50</b> vitórias
<b>7.</b> player5 com <b>1</b> vitória`,
		},
		{
			args:            []string{"test"},
			expectedMessage: "não entendi",
		},
	}

	for idx, test := range tests {
		mockBot.On("SendMessage", mock.Anything, mock.MatchedBy(func(message string) bool {
			// test
			if strings.TrimSpace(message) != strings.TrimSpace(test.expectedMessage) {
				t.Errorf("expected %v, got %v on test %v", test.expectedMessage, message, idx)
			}
			return true
		})).Return(0, nil)

		// act
		rankCommand(context.Background(), test.args...)
	}
}

func TestVersusRankMonthlySubscription(t *testing.T) {
	// setup
	var trigger func(ctx context.Context, previous, current []clash.Player) error

	expectedMessage := `<b>Doentes do Mês</b>
<b>1.</b> player1 com <b>10</b> vitórias
<b>2.</b> player2 com <b>5</b> vitórias
<b>3.</b> player4 com <b>1</b> vitória`
	mockBot := &mocks.Bot{}
	mockBot.On("SetCommand", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)
	mockBot.On("SendMessage", mock.Anything, mock.MatchedBy(func(message string) bool {
		// test
		if strings.TrimSpace(message) != strings.TrimSpace(expectedMessage) {
			t.Errorf("expected %v, got %v", expectedMessage, message)
		}
		return true
	})).Return(0, nil)

	mockClashAPI := &mocks.ClashAPI{}
	mockClashAPI.On("MainClanMembers", mock.Anything).Return(nil, nil)

	mockInformationManager := &mocks.InformationManager{}
	mockInformationManager.On("Subscribe", information_manager.Player, information_manager.Month, mock.MatchedBy(func(callback func(ctx context.Context, a, b []clash.Player) error) bool {
		trigger = callback
		return true
	})).Return(nil, nil)

	setupVersusRank(mockBot, mockClashAPI, mockInformationManager)

	previous := []clash.Player{
		{Name: "player1", Tag: "tag1", VersusBattleWins: 100},
		{Name: "player2", Tag: "tag2", VersusBattleWins: 50},
		{Name: "player3", Tag: "tag3", VersusBattleWins: 1000},
		{Name: "player4", Tag: "tag4", VersusBattleWins: 60},
	}
	current := []clash.Player{
		{Name: "player1", Tag: "tag1", VersusBattleWins: 110},
		{Name: "player2", Tag: "tag2", VersusBattleWins: 55},
		{Name: "player3", Tag: "tag3", VersusBattleWins: 1000},
		{Name: "player4", Tag: "tag4", VersusBattleWins: 61},
	}

	// act
	trigger(context.Background(), previous, current)
}
