package builder_base

import (
	"reflect"
	"testing"

	"gitlab.com/reinodovo/botdovo/internal/clash"
)

func TestVersusBattlePlayerResults(t *testing.T) {
	// setup
	previous := []clash.Player{
		{
			Tag:              "tag1",
			VersusBattleWins: 10,
			VersusTrophies:   30,
		},
		{
			Tag:              "tag2",
			VersusBattleWins: 10,
			VersusTrophies:   30,
		},
		{
			Tag:              "tag3",
			VersusBattleWins: 10,
			VersusTrophies:   30,
		},
		{
			Tag:              "tag4",
			VersusBattleWins: 10,
			VersusTrophies:   30,
		},
		{
			Tag:              "tag5",
			VersusBattleWins: 10,
			VersusTrophies:   30,
		},
		{
			Tag:              "tag6",
			VersusBattleWins: 10,
			VersusTrophies:   30,
		},
		{
			Tag:              "tag7",
			VersusBattleWins: 10,
			VersusTrophies:   30,
		},
	}
	current := []clash.Player{
		{
			Tag:              "tag1",
			VersusBattleWins: 11,
			VersusTrophies:   30,
		},
		{
			Tag:              "tag2",
			VersusBattleWins: 11,
			VersusTrophies:   10,
		},
		{
			Tag:              "tag3",
			VersusBattleWins: 10,
			VersusTrophies:   10,
		},
		{
			Tag:              "tag4",
			VersusBattleWins: 10,
			VersusTrophies:   30,
		},
		{
			Tag:              "tag5",
			VersusBattleWins: 10,
			VersusTrophies:   50,
		},
		{
			Tag:              "tag6",
			VersusBattleWins: 9,
			VersusTrophies:   30,
		},
		{
			Tag:              "tag8",
			VersusBattleWins: 11,
			VersusTrophies:   10,
		},
	}
	expectedWins := []versusBattlePlayer{
		{previous: previous[0], current: current[0], wins: 1, trophies: 0},
		{previous: previous[1], current: current[1], wins: 1, trophies: -20},
		{previous: clash.Player{}, current: current[6], wins: 11, trophies: 10},
	}
	expectedLosses := []versusBattlePlayer{
		{previous: previous[1], current: current[1], wins: 1, trophies: -20},
		{previous: previous[2], current: current[2], wins: 0, trophies: -20},
	}

	// act
	wins, losses := versusBattlePlayerResults(previous, current)
	// test
	if len(expectedWins) != len(wins) {
		t.Fatalf("expected %v wins, got %v", len(expectedWins), len(wins))
	}
	for idx, win := range wins {
		expectedWin := expectedWins[idx]
		if !reflect.DeepEqual(win, expectedWin) {
			t.Errorf("winner at position %v differs from expected", idx)
		}
	}
	if len(expectedLosses) != len(losses) {
		t.Fatalf("expected %v losses, got %v", len(expectedLosses), len(losses))
	}
	for idx, loss := range losses {
		expectedLoss := expectedLosses[idx]
		if !reflect.DeepEqual(loss, expectedLoss) {
			t.Errorf("loser at position %v differs from expected", idx)
		}
	}
}

func TestVersusBattlesSortByVersusBattleWins(t *testing.T) {
	// setup
	wins := []versusBattlePlayer{
		{
			previous: clash.Player{Tag: "tag1"},
			current:  clash.Player{Tag: "tag1"},
			wins:     2,
			trophies: 0,
		},
		{
			previous: clash.Player{Tag: "tag2"},
			current:  clash.Player{Tag: "tag2"},
			wins:     20,
			trophies: 0,
		},
	}
	expectedWins := []versusBattlePlayer{
		{
			previous: clash.Player{Tag: "tag2"},
			current:  clash.Player{Tag: "tag2"},
			wins:     20,
			trophies: 0,
		},
		{
			previous: clash.Player{Tag: "tag1"},
			current:  clash.Player{Tag: "tag1"},
			wins:     2,
			trophies: 0,
		},
	}
	// act
	resultWins := sortByVersusBattleWins(wins)
	// test
	if !reflect.DeepEqual(resultWins, expectedWins) {
		t.Errorf("expected %v, got %v", expectedWins, resultWins)
	}
}
