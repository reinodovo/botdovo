package builder_base

import (
	"sort"

	"gitlab.com/reinodovo/botdovo/internal/bot"
	"gitlab.com/reinodovo/botdovo/internal/clash"
	"gitlab.com/reinodovo/botdovo/internal/information_manager"
	"gitlab.com/reinodovo/botdovo/internal/utils"
)

func SetupBuilderBase(bot bot.Bot, clashAPI clash.ClashAPI, mgr information_manager.InformationManager) error {
	if err := setupVersusRank(bot, clashAPI, mgr); err != nil {
		return err
	}
	if err := setupVersusBattles(bot, mgr); err != nil {
		return err
	}
	if err := setupHonoraryMembersVersusBattles(bot, mgr); err != nil {
		return err
	}
	return nil
}

type versusBattlePlayer struct {
	previous clash.Player
	current  clash.Player
	wins     int64
	trophies int64
}

func versusBattlePlayerResults(previous, current []clash.Player) (wins, losses []versusBattlePlayer) {
	previousPlayersByTag := utils.PlayersByTag(previous)
	for _, player := range current {
		playerInfo := versusBattlePlayer{
			current:  player,
			previous: clash.Player{},
			wins:     player.VersusBattleWins,
			trophies: player.VersusTrophies,
		}
		if previousPlayer, ok := previousPlayersByTag[player.Tag]; ok {
			playerInfo.previous = previousPlayer
			playerInfo.wins = player.VersusBattleWins - previousPlayer.VersusBattleWins
			playerInfo.trophies = player.VersusTrophies - previousPlayer.VersusTrophies
		}
		if playerInfo.wins > 0 {
			wins = append(wins, playerInfo)
		}
		if playerInfo.trophies < 0 {
			losses = append(losses, playerInfo)
		}
	}
	return
}

func sortByVersusBattleWins(players []versusBattlePlayer) []versusBattlePlayer {
	sort.Slice(players[:], func(i, j int) bool {
		return players[i].wins > players[j].wins
	})
	return players
}
