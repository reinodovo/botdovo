package clan_capital

import (
	"bytes"
	"context"
	"fmt"
	"sort"

	"gitlab.com/reinodovo/botdovo/internal/bot"
	"gitlab.com/reinodovo/botdovo/internal/clash"
	"gitlab.com/reinodovo/botdovo/internal/configuration_manager"
	"gitlab.com/reinodovo/botdovo/internal/information_manager"
	"gitlab.com/reinodovo/botdovo/internal/utils"
)

var (
	detailKey = "detail"
	totalKey  = "total"
)

type clanCapitalContributionRank struct {
	bot     bot.Bot
	config  configuration_manager.ConfigurationManager
	trigger func(ctx context.Context) error
}

func setupClanCapitalContributionRank(bot bot.Bot, config configuration_manager.ConfigurationManager, mgr information_manager.InformationManager) error {
	clanCapital := &clanCapitalContributionRank{config: config, bot: bot}
	clanCapitalExplanation := "rank de contribuição semanal de Capital Gold (detail, d - mostra contribuição por conta; total, t - contribuição total)"
	if err := bot.SetCommand(context.Background(), "clan_capital", clanCapital.rankCommand, clanCapitalExplanation); err != nil {
		return err
	}
	var err error = nil
	clanCapital.trigger, err = mgr.Subscribe(information_manager.Player, information_manager.Monday, clanCapital.handleClanCapitalContributionRank)
	return err
}

func generateRankMessage(owners []utils.OwnerProgress[Contribution], showPlayers bool, showTotal bool) string {
	if len(owners) == 0 {
		return "Não enche meu saco"
	}

	var total Contribution = 0
	for _, owner := range owners {
		total += totalOwnerClanCapitalContribution(owner)
	}

	var buffer bytes.Buffer
	if showTotal {
		buffer.WriteString(fmt.Sprintf("<b>Contribuição Total para a Clan Capital</b>: <b>%v</b>\n", total))
	} else {
		buffer.WriteString(fmt.Sprintf("<b>Contribuição da Semana para a Clan Capital</b>: <b>%v</b>\n", total))
	}

	for rank, owner := range owners {
		ownerContribution := totalOwnerClanCapitalContribution(owner)
		if ownerContribution == 0 {
			continue
		}
		buffer.WriteString(fmt.Sprintf("<b>%v.</b> %v - <b>%v</b>\n", rank+1, owner.Owner, ownerContribution))
		if !showPlayers {
			continue
		}
		sortPlayersByClanCapitalContribution(owner.Players)
		for _, player := range owner.Players {
			if player.Data == 0 {
				continue
			}
			buffer.WriteString(fmt.Sprintf("        %v - <b>%v</b>\n", player.Current.Name, player.Data))
		}
	}

	return buffer.String()
}

func totalOwnerClanCapitalContribution(ownerProgress utils.OwnerProgress[Contribution]) Contribution {
	var total Contribution = 0
	for _, player := range ownerProgress.Players {
		total += player.Data
	}
	return total
}

func sortPlayersByClanCapitalContribution(players []utils.PlayerProgress[Contribution]) {
	sort.Slice(players[:], func(i, j int) bool {
		return players[i].Data > players[j].Data
	})
}

func sortOwnersByClanCapitalContribution(owners []utils.OwnerProgress[Contribution]) {
	sort.Slice(owners[:], func(i, j int) bool {
		return totalOwnerClanCapitalContribution(owners[i]) > totalOwnerClanCapitalContribution(owners[j])
	})
}

func (action *clanCapitalContributionRank) handleClanCapitalContributionRank(ctx context.Context, previous, current []clash.Player) error {
	showTotal := utils.BoolFromContext(ctx, totalKey)
	if showTotal {
		previous = []clash.Player{}
	}
	ownersProgress := utils.PlayersProgressByOwner(previous, current, playerClanCapitalContributionProgress, action.config)

	sortOwnersByClanCapitalContribution(ownersProgress)

	showPlayers := ctx.Value(detailKey) == nil || utils.BoolFromContext(ctx, detailKey)
	message := generateRankMessage(ownersProgress, showPlayers, showTotal)
	_, err := action.bot.SendMessage(ctx, message)
	return err
}

func parseArguments(args ...string) (showPlayers bool, showTotal bool, err error) {
	showPlayers = false
	showTotal = false
	err = nil
	for _, arg := range args {
		switch arg {
		case "detail":
			showPlayers = true
		case "d":
			showPlayers = true
		case "total":
			showTotal = true
		case "t":
			showTotal = true
		default:
			err = utils.InvalidArgumentsError
		}
	}
	return
}

func (action *clanCapitalContributionRank) rankCommand(ctx context.Context, args ...string) error {
	showPlayers, showTotal, err := parseArguments(args...)
	if err != nil {
		_, err := action.bot.SendMessage(ctx, "fala direito fdp")
		return err
	}
	ctx = context.WithValue(ctx, detailKey, showPlayers)
	ctx = context.WithValue(ctx, totalKey, showTotal)
	return action.trigger(ctx)
}
