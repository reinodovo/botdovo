package clan_capital

import (
	"context"
	"strings"
	"testing"

	"github.com/stretchr/testify/mock"
	"gitlab.com/reinodovo/botdovo/internal/bot"
	"gitlab.com/reinodovo/botdovo/internal/clash"
	"gitlab.com/reinodovo/botdovo/internal/information_manager"
	"gitlab.com/reinodovo/botdovo/internal/mocks"
	"gitlab.com/reinodovo/botdovo/internal/utils"
)

func TestGenerateRankMessage(t *testing.T) {
	// setup
	tests := []struct {
		owners             []utils.OwnerProgress[Contribution]
		shouldShowAccounts bool
		expectedMessage    string
	}{
		{
			owners: []utils.OwnerProgress[Contribution]{
				{
					Owner: "owner1",
					Players: []utils.PlayerProgress[Contribution]{
						{Current: clash.Player{Name: "player1"}, Data: 1000},
					},
				},
				{
					Owner: "owner2",
					Players: []utils.PlayerProgress[Contribution]{
						{Current: clash.Player{Name: "player2"}, Data: 150},
						{Current: clash.Player{Name: "player3"}, Data: 350},
					},
				},
			},
			shouldShowAccounts: false,
			expectedMessage: `<b>Contribuição da Semana para a Clan Capital</b>: <b>1500</b>
<b>1.</b> owner1 - <b>1000</b>
<b>2.</b> owner2 - <b>500</b>`,
		},
		{
			owners: []utils.OwnerProgress[Contribution]{
				{
					Owner: "owner1",
					Players: []utils.PlayerProgress[Contribution]{
						{Current: clash.Player{Name: "player1"}, Data: 1000},
					},
				},
				{
					Owner: "owner2",
					Players: []utils.PlayerProgress[Contribution]{
						{Current: clash.Player{Name: "player2"}, Data: 150},
						{Current: clash.Player{Name: "player3"}, Data: 350},
						{Current: clash.Player{Name: "player4"}, Data: 0},
					},
				},
				{
					Owner:   "owner3",
					Players: []utils.PlayerProgress[Contribution]{},
				},
				{
					Owner: "owner4",
					Players: []utils.PlayerProgress[Contribution]{
						{Current: clash.Player{Name: "player5"}, Data: 0},
					},
				},
			},
			shouldShowAccounts: true,
			expectedMessage: `<b>Contribuição da Semana para a Clan Capital</b>: <b>1500</b>
<b>1.</b> owner1 - <b>1000</b>
        player1 - <b>1000</b>
<b>2.</b> owner2 - <b>500</b>
        player3 - <b>350</b>
        player2 - <b>150</b>`,
		},
		{
			owners:             []utils.OwnerProgress[Contribution]{},
			shouldShowAccounts: false,
			expectedMessage:    "Não enche meu saco",
		},
		{
			owners:             []utils.OwnerProgress[Contribution]{},
			shouldShowAccounts: true,
			expectedMessage:    "Não enche meu saco",
		},
	}
	for idx, test := range tests {
		// act
		message := generateRankMessage(test.owners, test.shouldShowAccounts, false)
		// test
		if strings.TrimSpace(message) != strings.TrimSpace(test.expectedMessage) {
			t.Errorf("expected %v, got %v on test %v", test.expectedMessage, message, idx)
		}
	}
}

func TestRankCommand(t *testing.T) {
	// setup
	mockConfigurationManager := &mocks.ConfigurationManager{}
	mockConfigurationManager.On("AccountOwner", mock.MatchedBy(func(tag string) bool {
		return tag == "tag1"
	})).Return("player1", nil)
	mockConfigurationManager.On("AccountOwner", mock.MatchedBy(func(tag string) bool {
		return tag == "tag2" || tag == "tag3"
	})).Return("player2", nil)

	previous := []clash.Player{
		{Name: "account1", Tag: "tag1", Achievements: []clash.Achievement{{Name: clanCapitalContributionAchievement, Value: 50000}}},
		{Name: "account2", Tag: "tag2", Achievements: []clash.Achievement{{Name: clanCapitalContributionAchievement, Value: 5000}}},
		{Name: "account3", Tag: "tag3", Achievements: []clash.Achievement{{Name: clanCapitalContributionAchievement, Value: 45000}}},
	}
	current := []clash.Player{
		{Name: "account1", Tag: "tag1", Achievements: []clash.Achievement{{Name: clanCapitalContributionAchievement, Value: 51000}}},
		{Name: "account2", Tag: "tag2", Achievements: []clash.Achievement{{Name: clanCapitalContributionAchievement, Value: 5150}}},
		{Name: "account3", Tag: "tag3", Achievements: []clash.Achievement{{Name: clanCapitalContributionAchievement, Value: 45350}}},
	}

	mockInformationManager := &mocks.InformationManager{}
	mockInformationManager.On("Subscribe", mock.Anything, mock.Anything, mock.Anything).Return(
		func(data information_manager.Data, interval information_manager.Interval, callback interface{}) func(ctx context.Context) error {
			if callback, ok := callback.(func(ctx context.Context, a, b []clash.Player) error); ok {
				return func(ctx context.Context) error {
					callback(ctx, previous, current)
					return nil
				}
			} else {
				return func(ctx context.Context) error {
					return nil
				}
			}
		},
		nil,
	)

	tests := []struct {
		args            []string
		expectedMessage string
	}{
		{
			args: []string{},
			expectedMessage: `<b>Contribuição da Semana para a Clan Capital</b>: <b>1500</b>
<b>1.</b> player1 - <b>1000</b>
<b>2.</b> player2 - <b>500</b>`,
		},
		{
			args: []string{"d"},
			expectedMessage: `<b>Contribuição da Semana para a Clan Capital</b>: <b>1500</b>
<b>1.</b> player1 - <b>1000</b>
        account1 - <b>1000</b>
<b>2.</b> player2 - <b>500</b>
        account3 - <b>350</b>
        account2 - <b>150</b>`,
		},
		{
			args: []string{"detail"},
			expectedMessage: `<b>Contribuição da Semana para a Clan Capital</b>: <b>1500</b>
<b>1.</b> player1 - <b>1000</b>
        account1 - <b>1000</b>
<b>2.</b> player2 - <b>500</b>
        account3 - <b>350</b>
        account2 - <b>150</b>`,
		},
		{
			args: []string{"d", "t"},
			expectedMessage: `<b>Contribuição Total para a Clan Capital</b>: <b>101500</b>
<b>1.</b> player1 - <b>51000</b>
        account1 - <b>51000</b>
<b>2.</b> player2 - <b>50500</b>
        account3 - <b>45350</b>
        account2 - <b>5150</b>`,
		},
		{
			args: []string{"t"},
			expectedMessage: `<b>Contribuição Total para a Clan Capital</b>: <b>101500</b>
<b>1.</b> player1 - <b>51000</b>
<b>2.</b> player2 - <b>50500</b>`,
		},
		{
			args:            []string{"d", "x"},
			expectedMessage: "fala direito fdp",
		},
		{
			args:            []string{"x"},
			expectedMessage: "fala direito fdp",
		},
	}
	for idx, test := range tests {
		var rankCommand bot.Command

		mockBot := &mocks.Bot{}
		mockBot.On("SetCommand", mock.Anything, "clan_capital", mock.MatchedBy(func(command bot.Command) bool {
			rankCommand = command
			return true
		}), mock.Anything).Return(nil)
		mockBot.On("SendMessage", mock.Anything, mock.MatchedBy(func(message string) bool {
			// test
			if strings.TrimSpace(message) != strings.TrimSpace(test.expectedMessage) {
				t.Errorf("expected %v, got %v for test %v", test.expectedMessage, message, idx)
			}
			return true
		})).Return(0, nil)

		setupClanCapitalContributionRank(mockBot, mockConfigurationManager, mockInformationManager)

		// act
		rankCommand(context.Background(), test.args...)
	}
}
