package clan_capital

import (
	"context"
	"fmt"

	"gitlab.com/reinodovo/botdovo/internal/bot"
	"gitlab.com/reinodovo/botdovo/internal/clash"
	"gitlab.com/reinodovo/botdovo/internal/information_manager"
	"gitlab.com/reinodovo/botdovo/internal/utils"
	"go.opentelemetry.io/otel/trace"
)

const ongoingState = "ongoing"

type clanCapitalBonus struct {
	bot bot.Bot
}

func setupClanCapitalBonus(bot bot.Bot, mgr information_manager.InformationManager) error {
	clanCapital := &clanCapitalBonus{bot: bot}
	_, err := mgr.Subscribe(information_manager.CapitalRaids, information_manager.Minute, clanCapital.handleClanCapitalBonus)
	return err
}

func generateBonusMessage(player string) string {
	return fmt.Sprintf("%v conseguiu o ataque bônus no Pico, parabéns", player)
}

type memberBonus struct {
	name  string
	bonus int
}

func ongoingRaidMembersBonus(raids []clash.CapitalRaid) map[string]memberBonus {
	membersBonus := make(map[string]memberBonus)
	for _, raid := range raids {
		if raid.State == ongoingState {
			for _, member := range raid.Members {
				membersBonus[member.Tag] = memberBonus{
					name:  member.Name,
					bonus: member.BonusAttackLimit,
				}

			}
		}
	}
	return membersBonus
}

func membersWithNewBonus(previous, current []clash.CapitalRaid) []string {
	members := make([]string, 0)

	previousMembersBonus := ongoingRaidMembersBonus(previous)
	currentMembersBonus := ongoingRaidMembersBonus(current)

	for tag, currentMemberBonus := range currentMembersBonus {
		if previousMemberBonus, ok := previousMembersBonus[tag]; ok && currentMemberBonus.bonus > previousMemberBonus.bonus {
			members = append(members, currentMemberBonus.name)
		}
	}
	return members
}

func (action *clanCapitalBonus) handleClanCapitalBonus(ctx context.Context, previous, current []clash.CapitalRaid) error {
	span := trace.SpanFromContext(ctx)

	members := membersWithNewBonus(previous, current)
	for _, member := range members {
		message := generateBonusMessage(member)
		_, err := action.bot.SendMessage(ctx, message)
		utils.RecordErrorIfNotNil(span, err)
	}
	return nil
}
