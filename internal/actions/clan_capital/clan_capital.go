package clan_capital

import (
	"gitlab.com/reinodovo/botdovo/internal/bot"
	"gitlab.com/reinodovo/botdovo/internal/clash"
	"gitlab.com/reinodovo/botdovo/internal/configuration_manager"
	"gitlab.com/reinodovo/botdovo/internal/information_manager"
)

const (
	clanCapitalContributionAchievement = "Most Valuable Clanmate"
)

type Contribution = int64

func SetupClanCapital(bot bot.Bot, config configuration_manager.ConfigurationManager, mgr information_manager.InformationManager) error {
	if err := setupClanCapitalContributionUpdates(bot, mgr); err != nil {
		return err
	}
	if err := setupClanCapitalContributionRank(bot, config, mgr); err != nil {
		return err
	}
	if err := setupClanCapitalBonus(bot, mgr); err != nil {
		return err
	}
	return nil
}

func playerClanCapitalContribution(player clash.Player) Contribution {
	achievement, err := player.FindAchieventment(clanCapitalContributionAchievement)
	if err != nil {
		return 0
	}
	return achievement.Value
}

func playerClanCapitalContributionProgress(previous, current clash.Player) Contribution {
	previousContribution := playerClanCapitalContribution(previous)
	currentContribution := playerClanCapitalContribution(current)
	return currentContribution - previousContribution
}
