package clan_capital

import (
	"testing"

	"gitlab.com/reinodovo/botdovo/internal/clash"
)

func TestPlayerClanCapitalContribution(t *testing.T) {
	// setup
	tests := []struct {
		player               clash.Player
		expectedContribution Contribution
	}{
		{
			player: clash.Player{
				Name: "player1",
				Tag:  "tag1",
				Achievements: []clash.Achievement{
					{Name: "Most Valuable Clanmate", Value: 3000},
				},
			},
			expectedContribution: 3000,
		},
		{
			player: clash.Player{
				Name: "player1",
				Tag:  "tag1",
				Achievements: []clash.Achievement{
					{Name: "Something Else", Value: 3000},
				},
			},
			expectedContribution: 0,
		},
		{
			player:               clash.Player{Name: "player1", Tag: "tag1"},
			expectedContribution: 0,
		},
	}
	for idx, test := range tests {
		// act
		contribution := playerClanCapitalContribution(test.player)
		// test
		if contribution != test.expectedContribution {
			t.Errorf("contribution and expectedContribution did not match for test %v", idx)
		}
	}
}

func TestPlayerClanCapitalContributionProgress(t *testing.T) {
	// setup
	tests := []struct {
		previous             clash.Player
		current              clash.Player
		expectedContribution Contribution
	}{
		{
			previous:             clash.Player{Achievements: []clash.Achievement{{Name: clanCapitalContributionAchievement, Value: 3000}}},
			current:              clash.Player{Achievements: []clash.Achievement{{Name: clanCapitalContributionAchievement, Value: 3500}}},
			expectedContribution: 500,
		},
		{
			previous:             clash.Player{Achievements: []clash.Achievement{{Name: clanCapitalContributionAchievement, Value: 3000}}},
			current:              clash.Player{Achievements: []clash.Achievement{{Name: clanCapitalContributionAchievement, Value: 3000}}},
			expectedContribution: 0,
		},
		{
			previous:             clash.Player{Achievements: []clash.Achievement{{Name: clanCapitalContributionAchievement, Value: 3000}}},
			current:              clash.Player{Achievements: []clash.Achievement{{Name: clanCapitalContributionAchievement, Value: 13000}}},
			expectedContribution: 10000,
		},
		{
			previous:             clash.Player{Achievements: []clash.Achievement{{Name: clanCapitalContributionAchievement, Value: 4000}}},
			current:              clash.Player{Achievements: []clash.Achievement{{Name: clanCapitalContributionAchievement, Value: 3000}}},
			expectedContribution: -1000,
		},
	}
	for idx, test := range tests {
		// act
		contribution := playerClanCapitalContributionProgress(test.previous, test.current)
		// test
		if contribution != test.expectedContribution {
			t.Errorf("contribution and expectedContribution do not match for test %v", idx)
		}
	}
}
