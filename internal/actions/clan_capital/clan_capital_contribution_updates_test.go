package clan_capital

import (
	"context"
	"reflect"
	"sort"
	"testing"

	"github.com/stretchr/testify/mock"
	"gitlab.com/reinodovo/botdovo/internal/clash"
	"gitlab.com/reinodovo/botdovo/internal/mocks"
	"gitlab.com/reinodovo/botdovo/internal/utils"
)

func TestGeneratePlayerClanCapitalContributionUpdateMessage(t *testing.T) {
	// setup
	player := utils.PlayerProgress[Contribution]{
		Current: clash.Player{Name: "player1"},
		Data:    150,
	}
	expectedMessage := "player1 contribuiu <b>150</b> pra capital."
	// act
	message := generatePlayerClanCapitalContributionUpdateMessage(player)
	// test
	if message != expectedMessage {
		t.Error("message and expectedMessage did not match")
	}
}

func TestHandlePlayerClanCapitalContributionUpdate(t *testing.T) {
	// setup
	mockConfigurationManager := &mocks.ConfigurationManager{}
	mockConfigurationManager.On("AccountOwner", mock.MatchedBy(func(tag string) bool {
		return tag == "tag1"
	})).Return("player1", nil)
	mockConfigurationManager.On("AccountOwner", mock.MatchedBy(func(tag string) bool {
		return tag == "tag2" || tag == "tag3"
	})).Return("player2", nil)

	previous := []clash.Player{
		{Name: "account1", Tag: "tag1", Achievements: []clash.Achievement{{Name: clanCapitalContributionAchievement, Value: 50000}}},
		{Name: "account2", Tag: "tag2", Achievements: []clash.Achievement{{Name: clanCapitalContributionAchievement, Value: 5000}}},
		{Name: "account3", Tag: "tag3", Achievements: []clash.Achievement{{Name: clanCapitalContributionAchievement, Value: 45000}}},
	}
	current := []clash.Player{
		{Name: "account1", Tag: "tag1", Achievements: []clash.Achievement{{Name: clanCapitalContributionAchievement, Value: 51000}}},
		{Name: "account2", Tag: "tag2", Achievements: []clash.Achievement{{Name: clanCapitalContributionAchievement, Value: 5000}}},
		{Name: "account3", Tag: "tag3", Achievements: []clash.Achievement{{Name: clanCapitalContributionAchievement, Value: 45350}}},
	}
	expectedMessages := []string{
		"account1 contribuiu <b>1000</b> pra capital.",
		"account3 contribuiu <b>350</b> pra capital.",
	}

	var messages []string
	mockBot := &mocks.Bot{}
	mockBot.On("SendMessage", mock.Anything, mock.MatchedBy(func(message string) bool {
		messages = append(messages, message)
		return true
	})).Return(0, nil)

	clanCapitalContributionUpdates := clanCapitalContributionUpdates{bot: mockBot}

	// act
	clanCapitalContributionUpdates.handlePlayerClanCapitalContributionUpdate(context.Background(), previous, current)

	// test
	sort.Strings(messages)
	sort.Strings(expectedMessages)
	if !reflect.DeepEqual(messages, expectedMessages) {
		t.Errorf("expected %v, got %v", expectedMessages, messages)
	}
}
