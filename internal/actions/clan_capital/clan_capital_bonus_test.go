package clan_capital

import (
	"reflect"
	"testing"

	"gitlab.com/reinodovo/botdovo/internal/clash"
)

func TestMembersWithNewBonus(t *testing.T) {
	// setup
	previous := []clash.CapitalRaid{
		{
			State: ongoingState,
			Members: []clash.CapitalRaidMember{
				{
					Name:             "name1",
					Tag:              "tag1",
					BonusAttackLimit: 0,
				},
				{
					Name:             "name2",
					Tag:              "tag2",
					BonusAttackLimit: 0,
				},
				{
					Name:             "name3",
					Tag:              "tag3",
					BonusAttackLimit: 1,
				},
			},
		},
	}
	current := []clash.CapitalRaid{
		{
			State: ongoingState,
			Members: []clash.CapitalRaidMember{
				{
					Name:             "name1",
					Tag:              "tag1",
					BonusAttackLimit: 0,
				},
				{
					Name:             "name2",
					Tag:              "tag2",
					BonusAttackLimit: 1,
				},
				{
					Name:             "name3",
					Tag:              "tag3",
					BonusAttackLimit: 1,
				},
			},
		},
	}
	expectedMembers := []string{"name2"}
	// act
	members := membersWithNewBonus(previous, current)
	// test
	if !reflect.DeepEqual(members, expectedMembers) {
		t.Errorf("expected %v, got %v", expectedMembers, members)
	}
}
