package clan_capital

import (
	"context"
	"fmt"

	"gitlab.com/reinodovo/botdovo/internal/bot"
	"gitlab.com/reinodovo/botdovo/internal/clash"
	"gitlab.com/reinodovo/botdovo/internal/information_manager"
	"gitlab.com/reinodovo/botdovo/internal/utils"
	"go.opentelemetry.io/otel/trace"
)

type clanCapitalContributionUpdates struct {
	bot bot.Bot
}

func setupClanCapitalContributionUpdates(bot bot.Bot, mgr information_manager.InformationManager) error {
	clanCapitalContributionUpdates := clanCapitalContributionUpdates{bot: bot}
	_, err := mgr.Subscribe(information_manager.Player, information_manager.Minute, clanCapitalContributionUpdates.handlePlayerClanCapitalContributionUpdate)
	return err
}

func generatePlayerClanCapitalContributionUpdateMessage(player utils.PlayerProgress[Contribution]) string {
	return fmt.Sprintf("%v contribuiu <b>%v</b> pra capital.", player.Current.Name, player.Data)
}

func (action *clanCapitalContributionUpdates) handlePlayerClanCapitalContributionUpdate(ctx context.Context, previous, current []clash.Player) error {
	players := utils.PlayersProgress(previous, current, playerClanCapitalContributionProgress)
	for _, player := range players {
		if player.Data == 0 {
			continue
		}
		message := generatePlayerClanCapitalContributionUpdateMessage(player)
		_, err := action.bot.SendMessage(ctx, message)

		span := trace.SpanFromContext(ctx)
		utils.RecordErrorIfNotNil(span, err)
	}
	return nil
}
