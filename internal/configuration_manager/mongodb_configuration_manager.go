package configuration_manager

import (
	"context"
	"log"
	"time"

	"github.com/patrickmn/go-cache"
	"gitlab.com/reinodovo/botdovo/internal/database"
)

const (
	defaultAccount = "Bilu"
)

const (
	collectionName = "config"
	key            = "config"
	cacheKey       = "config"
)

type DBConfigurationManager struct {
	db    database.Database
	cache *cache.Cache
}

func NewDBConfigurationManager(db database.Database, cache *cache.Cache, logger *log.Logger) *DBConfigurationManager {
	return &DBConfigurationManager{db: db, cache: cache}
}

func (mgr *DBConfigurationManager) config() (Config, error) {
	var config Config
	if cachedConfig, found := mgr.cache.Get(cacheKey); found {
		config = cachedConfig.(Config)
	} else {
		err := mgr.db.Get(context.Background(), "config", "config", &config)
		if err != nil {
			return config, err
		}
		mgr.cache.Set(cacheKey, config, 1*time.Minute)
	}
	return config, nil
}

func (mgr *DBConfigurationManager) saveConfig(config Config) error {
	err := mgr.db.SaveObject(context.Background(), collectionName, key, config)
	mgr.cache.Delete(cacheKey)
	return err
}

func (mgr *DBConfigurationManager) OverviewSheetsId() (string, error) {
	config, err := mgr.config()
	if err != nil {
		return "", err
	}
	return config.CWL.OverviewSheetsId, nil
}

func (mgr *DBConfigurationManager) SheetsId() (string, error) {
	config, err := mgr.config()
	if err != nil {
		return "", err
	}
	return config.CWL.SheetsId, nil
}

func (mgr *DBConfigurationManager) SetSheetsId(id string) error {
	config, err := mgr.config()
	if err != nil {
		return err
	}
	config.CWL.SheetsId = id
	return mgr.saveConfig(config)
}

func (mgr *DBConfigurationManager) PlanningSheetsId() (string, error) {
	config, err := mgr.config()
	if err != nil {
		return "", err
	}
	return config.CWL.PlanningSheetsId, nil
}

func (mgr *DBConfigurationManager) SetPlanningSheetsId(id string) error {
	config, err := mgr.config()
	if err != nil {
		return err
	}
	config.CWL.PlanningSheetsId = id
	return mgr.saveConfig(config)
}

func (mgr *DBConfigurationManager) CWLBonus() (CWLBonusConfig, error) {
	config, err := mgr.config()
	if err != nil {
		return CWLBonusConfig{}, err
	}
	return config.CWL.Bonus, nil
}

func (mgr *DBConfigurationManager) SetCWLBonus(bonus CWLBonusConfig) error {
	config, err := mgr.config()
	if err != nil {
		return err
	}
	config.CWL.Bonus = bonus
	return mgr.saveConfig(config)
}

func (mgr *DBConfigurationManager) AccountOwner(tag string) (string, error) {
	config, err := mgr.config()
	if err != nil {
		return "", err
	}
	if accountInfo, ok := config.Clan.Accounts[tag]; ok {
		return accountInfo.Owner, nil
	} else {
		return defaultAccount, nil
	}
}

func (mgr *DBConfigurationManager) MemberTags() ([]string, error) {
	config, err := mgr.config()
	if err != nil {
		return nil, err
	}
	var tags []string
	for tag := range config.Clan.Accounts {
		tags = append(tags, tag)
	}
	return tags, nil
}

func (mgr *DBConfigurationManager) HonoraryMembers() ([]string, error) {
	config, err := mgr.config()
	return config.Clan.HonoraryMembers, err
}
