package configuration_manager

type CWLSingleBonusConfig struct {
	Person  string
	Account string
}

type CWLBonusConfig struct {
	Season      string
	MessageId   int
	TotalAmount int
	Bonuses     []CWLSingleBonusConfig
	State       string
}

type CWLConfig struct {
	SheetsId         string
	PlanningSheetsId string
	OverviewSheetsId string
	Bonus            CWLBonusConfig
}

type AccountInfo struct {
	Owner string
}

type ClanConfig struct {
	Accounts        map[string]AccountInfo
	HonoraryMembers []string
}

type Config struct {
	CWL  CWLConfig
	Clan ClanConfig
}

type ConfigurationManager interface {
	OverviewSheetsId() (string, error)
	SheetsId() (string, error)
	SetSheetsId(string) error
	PlanningSheetsId() (string, error)
	SetPlanningSheetsId(string) error
	CWLBonus() (CWLBonusConfig, error)
	SetCWLBonus(CWLBonusConfig) error
	AccountOwner(string) (string, error)
	MemberTags() ([]string, error)
	HonoraryMembers() ([]string, error)
}
