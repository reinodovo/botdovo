fetch:
	git fetch
	git checkout origin/master

build: FORCE
	docker build --tag reinodovo/botdovo . -f build/Dockerfile --platform linux/amd64

push: build
	docker push reinodovo/botdovo

deploy: FORCE
	-kubectl delete secret botdovo-secret
	kubectl create secret generic botdovo-secret --from-env-file=.env
	-kubectl delete -f ./deploy
	kubectl apply -f ./deploy

format:
	go fmt ./...

test: format
	go test ./... -v -coverprofile=coverage.cov -coverpkg=./...

coverage: test
	go tool cover -html=coverage.cov

run: test
	docker run --env-file .env --network host reinodovo/botdovo

FORCE: ;