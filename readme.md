# Bot d'Ovo

`@reinodovo_bot`

## Environment Variables

This bot needs some environment variables to run correcly

- `BOT_TOKEN:` This can be found by talking to `@BotFather`.
- `BOT_USER_ID:` Number prefix of `BOT_TOKEN`
- `CHAT_ID:` The bot only accepts messages for this chat id.

- `API_TOKEN:` Clash API token for the IP which is running the bot.
- `CLAN_TAG:` The main clan tag which is used in almost all clash API calls.

- `GOOGLE_CREDENTIALS:` Google credentials json content to allow the bot to change google sheets (Check [google auth section](#google-auth)).

- `RABBITMQ_HOST`
- `RABBITMQ_PORT`
- `RABBITMQ_USER`
- `RABBITMQ_PASS`
- `RABBITMQ_TELEGRAM_MESSAGES_STREAM_NAME`

- `MONGODB_URI`

- `HONEYCOMB_API_KEY:` Honeycomb API Key, can be found on the [Honeycomb team page](https://ui.honeycomb.io/teams/reino-d-ovo/)
- `HONEYCOMB_DATASET:` Honeycomb dataset, set it to `test` when running locally.

## Testing

Unless you have a systemd service setup for testing the bot, you need to export all the environment variables to run the bot locally.

Also, if the token for the real botdovo is currently being used, you need to change it to some other test bot.

The clash API token also needs to change to reflect your IP.

### Mocks

If you need to mock one of the interfaces for making tests, run `mockery --name={InterfaceName} --recursive`

## Architecture

![Bot d'Ovo Architecture](docs/images/architecture.png)
[diagram source](https://app.diagrams.net/#G1fHoWlT-MEF8IXuJg7YWo9nOOdAQOW1jN)

## Google Auth

Bot d'Ovo is a project on the Google Cloud Platform. It's credentials can be found under the `API & Services / Credentials` section, [here](https://console.cloud.google.com/apis/credentials?project=bot-dovo).

Since Bot d'Ovo is a test application, it's token expires in 7 days, making it necessary to regenerate the token. To do this use the command `update_token`.
